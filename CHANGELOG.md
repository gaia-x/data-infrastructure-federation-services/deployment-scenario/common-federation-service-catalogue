# [1.5.0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/compare/1.4.0...1.5.0) (2024-06-27)


### Features

* update query to return Aster-X compliance ([8ababda](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/commit/8ababda07d2ea433fbb1b25a195b6b41716fd46b))

# [1.4.0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/compare/1.3.1...1.4.0) (2024-04-26)


### Features

* enable CES synchronisation catalogue asynchronously ([90e0448](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/commit/90e044821655bde3d7dc6e41f41410f41033468c))
* enable HTTP synchronisation catalogue asynchronously ([d572db0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/commit/d572db0c0562bf2f1841eaff49641396a9719f3b))

## [1.3.1](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/compare/1.3.0...1.3.1) (2024-04-19)


### Bug Fixes

* exception when synchronization is done through redis ([a8f8cc7](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/commit/a8f8cc78dc92859d1743df85cacee21ae28127f3))
* wrong context ([33024d5](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/commit/33024d53ae9687f6984601d70dc05a1666865f3f))

# [1.3.0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/compare/1.2.0...1.3.0) (2024-03-14)


### Features

* enable filter on data-product SPARQL query ([cadb483](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/commit/cadb483c8993b70e5765b1c3bf1ef095a037bf6b))

# [1.2.0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/compare/v1.1.0...1.2.0) (2024-2-29)


### Features

* add documentation and enable increase version to release. ([062e541](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/commit/062e541a7de869478a9fed63f1941085ea5677c1))
