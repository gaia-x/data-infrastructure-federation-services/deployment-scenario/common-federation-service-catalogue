ARG REGISTRY=public.ecr.aws/docker/library

FROM $REGISTRY/python:3.11-slim as build

ENV PIP_DEFAULT_TIMEOUT=100 \
    # Allow statements and log messages to immediately appear
    PYTHONUNBUFFERED=1 \
    # disable a pip version check to reduce run-time & log-spam
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    # cache is useless in docker image, so disable to reduce image size
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.3.2

WORKDIR /app

COPY pyproject.toml poetry.lock ./

RUN pip install --no-cache-dir "poetry==$POETRY_VERSION" \
    && poetry install --no-root --no-ansi --no-interaction \
    && poetry export -f requirements.txt --output requirements.txt --without-hashes


### Final stage
FROM $REGISTRY/python:3.11.6-slim-bullseye as final

ENV PORT=5001
WORKDIR /app

COPY --from=build /app/requirements.txt .

RUN set -ex \
    # Create a non-root user
    && addgroup --system --gid 1001 appgroup \
    && adduser --system --uid 1001 --gid 1001 --no-create-home appuser \
    # Upgrade the package index and install security upgrades
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get upgrade -y libdb5.3 \
    # Install dependencies
    && pip install --no-cache-dir --upgrade -r /app/requirements.txt \
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

COPY ./federated_catalogue federated_catalogue

EXPOSE ${PORT}

CMD [ "python","-u", "-m", "federated_catalogue" ]

# Set the user to run the application
USER appuser
