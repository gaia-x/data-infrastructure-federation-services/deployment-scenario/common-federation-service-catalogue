NAME := catalogue
INSTALL_STAMP := .install.stamp
POETRY := $(shell command -v poetry 2> /dev/null)

.DEFAULT_GOAL := help

.PHONY: help
help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo ""
	@echo "  install     	install packages and prepare environment"
	@echo "  clean       	remove all temporary files"
	@echo "  lint        	run the code linters"
	@echo "  format      	reformat code"
	@echo "  test        	run all the tests"
	@echo "  image       	build a docker image for provider catalog"
	@echo "  test-ci     	launch integration tests with docker-compose"
	@echo "  pre-commit  	run pre-commit checks"
	@echo "  launch-python	launch locally provider catalog"
	@echo ""
	@echo "Check the Makefile to know exactly what each target is doing."

install: $(INSTALL_STAMP)
$(INSTALL_STAMP): pyproject.toml poetry.lock
	@if [ -z $(POETRY) ]; then echo "Poetry could not be found. See https://python-poetry.org/docs/"; exit 2; fi
	$(POETRY) install
	pre-commit install --install-hooks
	touch $(INSTALL_STAMP)

.PHONY: clean
clean:
	find . -type d -name "__pycache__" | xargs rm -rf {};
	rm -rf $(INSTALL_STAMP) .coverage .mypy_cache .pytest_cache .ruff_cache

.PHONY: lint
lint: $(INSTALL_STAMP)
	find . -type d -name "__pycache__" | xargs rm -rf {};
	$(POETRY) run isort --settings-path=pyproject.toml --check-only $(NAME)
	$(POETRY) run black --check ./tests/ $(NAME) --config=pyproject.toml --diff
	$(POETRY) run pylint --rcfile=pyproject.toml $(NAME)
	$(POETRY) run ruff --config=pyproject.toml $(NAME)

.PHONY: format
format: $(INSTALL_STAMP)
	$(POETRY) run isort --settings-path=pyproject.toml ./tests/ $(NAME)
	$(POETRY) run black ./tests/ $(NAME) --config=pyproject.toml

.PHONY: test
test: $(INSTALL_STAMP)
	docker-compose down -v
	docker-compose up --force-recreate --renew-anon-volumes redis graphdb -d
	$(POETRY) run pytest ./federated_catalogue/tests/

.PHONY: image
image: $(INSTALL_STAMP)
	docker build --tag provider-catalogue .

.PHONY: test-ci
test-ci: $(INSTALL_STAMP)
	$(POETRY) export -f requirements.txt --output requirements.txt --without-hashes --with dev
	docker-compose down -v
	docker-compose -f docker-compose-ci.yml up --force-recreate --build --renew-anon-volumes --exit-code-from pytest --

.PHONY: pre-commit
pre-commit: $(INSTALL_STAMP)
	pre-commit run

.PHONY: launch-python
launch-python: $(INSTALL_STAMP)
	docker-compose down -v
	docker-compose up --force-recreate --renew-anon-volumes redis -d
	$(POETRY) run python3 -m federated_catalogue
