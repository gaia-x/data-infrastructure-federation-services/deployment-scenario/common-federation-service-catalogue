# Catalogue HTTP Synchronisation

## Big view explanation

Federated Catalogue can store Catalogue URLs that it will regularly fetch to get Provider Catalogue to synchronise
inside its database.

To manage the HTTP synchronisation, you have a dedicated section inside REST API.

![REST API to manage catalogue synchronisation](./images/swagger_http_synchronisation.png)

## Instruction sequence for catalogue registration

1. Provider register his catalogue URL which expose a catalogue object

For example, provider ovhcloud exposes its catalogue on the following URL : [https://catalogue-api.ovhcloud.demo23.gxfs.fr/catalogue](https://catalogue-api.ovhcloud.demo23.gxfs.fr/catalogue)

Thus, you obtain the following catalogue:
```json
{
  "@context": [
    "https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
  ],
  "@type": "gx:Catalogue",
  "gx:providedBy": "https://ovhcloud.provider.dev.gxdch.ovh/legal-participant-json/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json",
  "gx:getVerifiableCredentialsIDs": [
    "https://ovhcloud.provider.dev.gxdch.ovh/service-offering/0725e902f94bca3ce89bfe47791d5b606efdf30b2a4182a2ada42179b13b73cc/data.json",
    "https://ovhcloud.provider.dev.gxdch.ovh/service-offering/0d7c4a71c19fd84072e8f063aa2550678984bc7e4bca8232d59bb2a737fd4548/data.json"
  ]
}
```

In that case, you can register the catalogue URL [https://catalogue-api.ovhcloud.provider.demo23.gxfs.fr/catalogue](https://catalogue-api.ovhcloud.provider.demo23.gxfs.fr/catalogue) to the Federated Catalogue.

```bash
curl -X 'POST' \
  'https://federated-catalogue-api.aster-x.demo23.gxfs.fr/provider-catalogue-url?provider_catalogue_url=https%3A%2F%2Fcatalogue-api.ovhcloud.provider.demo23.gxfs.fr%2Fcatalogue' \
  -H 'accept: application/json' \
  -H 'x-api-key: <FEDERATED CATALOGUE API KEY>' \
  -d ''
```

2. Federated catalogue regularly launch the HTTP synchronisation

However, you can test if the catalogue is well registered by calling the following endpoint:

```bash
curl -X 'POST' \
  'https://federated-catalogue-api.aster-x.demo23.gxfs.fr/launch-http-synchro' \
  -H 'accept: application/json' \
  -H 'x-api-key: <FEDERATED CATALOGUE API KEY>' \
  -d ''
```

You can also check the status of the synchronisation by calling the following endpoint:

```bash
curl -X 'GET' \
  'https://federated-catalogue-api.aster-x.demo23.gxfs.fr/http-synchro-status' \
  -H 'accept: application/json'
```

3. To update his catalogue, Provider need to change his Catalogue object and to wait next synchronisation.


## HTTP synchronisation explanation

For each Catalogue URL in his registry, Federated Catalogue will :
- Retrieve the whole catalogue VC Ids
- Compute the new id to add to the database
- Compute the id to remove to the database
- For each new id to add, will retrieve the VC content and will index it.

> ❗A VC can not be updated (= not with the same id) so we don't manage update of a VC.

> ❗When you remove a Catalogue URL, the whole associated catalogue is removed of the Federated Catalogue.

## HTTP synchronisation troubleshooting

If you added a wrong Catalogue URL, you can remove it by following the next commands.

> ❗The following commands need to be executed by an administrator of the Federated Catalogue.
> This administrator must have access to the kubernetes cluster where the Federated Catalogue is deployed
> and must have the right to execute kubectl commands.


```bash
export POD_NAME=$(kubectl -n aster-x get pods -o custom-columns=name:".metadata.name" --no-headers | grep redis)
k exec -n aster-x $POD_NAME -it -- redis-cli
```

Then, you can remove the Catalogue URL by executing the following command on redis:

```bash
smembers REGISTERED_CATALOGUE
srem REGISTERED_CATALOGUE <wrong url>
del HTTP_SYNCHRO_STATUS
```
