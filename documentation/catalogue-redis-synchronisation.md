# Catalogue Redis Synchronisation


## Context
The federated catalogue is an aggregation of the data provided by the providers participating in the federation.
Providers with their own catalogue can manage the published data. They can then add or delete this data within the
federated catalogue.

To do this, a pub/sub broker is set up and allows the update messages from the providers' catalogues to pass through.
The federated catalogue then listens to the various update messages from the providers' catalogues.

![Pub/Sub schema to synchronize Provider and Federated catalogue](./images/catalogue_pub_sub.png)
**Figure 1** - Pub/Sub schema to synchronize Provider and Federated catalogue

---
**Note**<br/>
Currently, the process of on-boarding a participant to the federation is not yet defined.
All providers can modify the data in the federated catalogue even if they do not participate in the federation
<br/>To demonstrate the pub/sub mechanism, we first use a redis server that is deployed within the federation namespace
---

Federated Catalog is feeding with two ways :
- API expose an endpoint to push data inside the federated-catalog (Push mode)
- Federated Catalog pull the participant-catalog to synchronize its graph federatedCatalogueDatabase (Pull mode)

---
**Note**: Federated Catalog API expose an endpoint to add json-ld object to the catalog. The graph will store the
object and create association if IRI of the node are identical to existing node.
---

## Global view architecture

```mermaid
C4Container
    title Federated Catalogue - Global view architecture

    Person(provider, Provider, "A legal person that provides <br/> a set of services or data products")
    Person(consumer, Consumer, "A legal person that consumes <br/>  a set of services or data products proposed by providers")

    Container_Boundary(c1, "Provider") {
        Container_Boundary(provider_wallet, "Provider Wallet") {
            Container(user_agent, "User Agent", "Python / Flask", "Bootstrap, manage and store provider's VC")
            Container(vc_issuer, "VC Issuer", "Python / Flask", "Sign provider's VC")
        }
        Container_Boundary(provider_catalogue, "Provider Catalogue") {
            Container(provider_catalogue_api, "Provider Catalogue API", "Python / FastAPI", "Manage self-description objects into provider's catalogue<br/> to be available or not in Federated Catalogue")
            ContainerDb(provider_catalogue_cache, "Catalogue Cache", "Redis", "Stores only self-described objects <br/> that are available in the catalogue")

        }
    }

    Container_Boundary(c2, "Federation") {
        Container_Boundary(federation_catalogue, "Federation Catalogue") {
            Container(federation_catalogue_spa, "Federation Catalogue Web App", "Typescript / NodeJS", "Portal to Search, Query and manage <br/> federated catalogue thanks to a Web Browser.")
            Container(federation_catalogue_api, "Federation Catalogue API", "Python / FastAPI", "Manage self-description objects into provider's catalogue<br/> to be available or not in Federated Catalogue")
            Container(federation_catalogue_rule_checker, "Federation Catalogue Rule Checker", "OPA / Rego", "Check federated catalogue rules thanks to OPA Policies")
            ContainerDb(federation_catalogue_db, "Federation Catalogue Database", "GraphDB", "Stores the object graphs from the catalogues<br/> of the providers belonging to the federation")
            ContainerDb(federation_catalogue_cache, "Catalogue Cache", "Redis", "Stores self-described objects <br/> that are available in the catalogue to be efficient.")

        }

        System_Ext(pub_sub_broker, "Messaging - Pub / Sub Broker", "Redis")

        Container_Boundary(federation_wallet, "Federation Wallet") {
            Container(federation_user_agent, "User Agent", "Python / Flask", "Bootstrap, manage and store federation's VC")
            Container(federation_vc_issuer, "VC Issuer", "Python / Flask", "Sign federation's VC")
        }
    }

    Rel_Back(provider_catalogue_cache, provider_catalogue_api, "Reads from and writes to", "sync, JSON/HTTPS")
    Rel(user_agent, vc_issuer, "Uses", "JSON/HTTPS")

    Rel(provider_catalogue_api, pub_sub_broker, "Send VP message into broker", "JSON/HTTPS")
    Rel(pub_sub_broker, federation_catalogue_api, "Deliver VP messages to subscribers", "JSON/HTTPS")

    Rel(consumer, federation_catalogue_spa, "Uses", "HTTPS")
    Rel(federation_catalogue_spa, federation_catalogue_api, "Uses", "JSON/HTTPS")
    Rel(federation_catalogue_api, federation_catalogue_rule_checker, "Uses", "JSON/HTTP")
    Rel_Back(federation_catalogue_db, federation_catalogue_api, "Reads from and writes to", "sync, JSON/HTTP")
    Rel_Back(federation_catalogue_cache, federation_catalogue_api, "Reads from and writes to", "sync, JSON/HTTP")

    Rel(federation_user_agent, federation_vc_issuer, "Uses", "JSON/HTTPS")

```

---
**Note**: An API Key protects Federated Catalogue API for demo purpose. When provider catalogue pushes updated into
federated catalogue, it must authenticate with this API Key.
When identification and authentication service will be implemented, it will replace this mechanism by a proper method.
---
## Terminology

| Term                              | Definition                                                                                                                                 | Gitlab repository                                                                                                                                                          |
|-----------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Wallet                            | An entity used by the Holder to receive, store, present, and manage Verifiable Credentials and key material.                               |
| User Agent                        | User Agent is an implementation of Wallet for demo purpose                                                                                 | [User Agent repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent)                                                  |
| VC Issuer                         | VC Issuer is an internal service that generates VC for wallet                                                                              | [VC Issuer repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer)                                                    |
| Provider catalogue API            | API to share provider service catalog with a federation                                                                                    | [Provider Service Catalogue repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue)                |
| Federated service catalogue       | Service offering catalogue that belongs to a Federation. It is an aggregation of catalogues offered by providers belonging to a federation | [Common Federated Service Catalogue repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/common-federation-service-catalogue) |
| Federated catalogue Web App       | Front-end to search into federated catalogue                                                                                               | [Federated catalogue Web App repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2)                             |
| Federated service catalogue API   | API to search in Federated service offering catalogue                                                                                      | [Common Federated Service Catalogue repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue)       |
| Federated Catalogye Rule CheckerI | Rule engine to check if objects submitted to federated catalogue are compliant or not with federation policies                             | [Common Federated Service Catalogue repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/abc-checker)                         |

## Use case and flow

### Provider catalogue management
#### Create a new service-offering

```mermaid
sequenceDiagram
    actor Bob as [Provider]<br>Bob
    participant Wallet as [Wallet]<br>Provider Wallet
    participant VC Issuer
    participant Provider Catalog
    participant Compliance
    participant Labelling

    Bob->>Bob: Create service offering SD
    Bob->>+Wallet: Store service offering SD
    Wallet-->>-Bob: OK

    Bob->>+VC Issuer: Ask VC Service-Offering generation with Provider as Issuer
    VC Issuer-->>-Bob: VC Service-Offering generated

    Bob->>+Wallet: Store VC Service-Offering
    Wallet-->>-Bob: OK

    Bob->>+Compliance: Ask Compliance for Service-Offering
    Compliance-->>-Bob: VC Compliance

    Bob->>+Labelling: Ask Labelling for Service-Offering
    Labelling-->>-Bob: VC Labelling

    Bob->>+Wallet: Store VC Labelling and VC Compliance
    Wallet-->>-Bob: OK
```
#### Add a new service-offering into Provider catalogue

```mermaid
sequenceDiagram
    actor Bob as [Provider]<br>Bob
    participant Wallet as [Wallet]<br>Provider Wallet
    participant VC Issuer
    participant Provider Catalog
    participant Pub/Sub broker

    Bob->>+Provider Catalog: Index VCs to make them available
    Provider Catalog-->>-Bob: OK

    Provider Catalog->>+Wallet: Ask VP for Provider Catalogue
    Wallet->>+VC Issuer: Ask sign VP
    VC Issuer-->>-Wallet: VP Catalogue generated
    Wallet-->>-Provider Catalog: VP generated
    Provider Catalog->>+Pub/Sub broker: Send VP for Provider Catalogue
```
#### Remove a new service-offering from Provider catalogue

```mermaid
sequenceDiagram
    actor Bob as [Provider]<br>Bob
    participant Wallet as [Wallet]<br>Provider Wallet
    participant VC Issuer
    participant Provider Catalog
    participant Pub/Sub broker

    Bob->>+Provider Catalog: Delete VCs to make them unavailable
    Provider Catalog-->>-Bob: OK

    Provider Catalog->>+Wallet: Ask VP for Provider Catalogue
    Wallet->>+VC Issuer: Ask sign VP
    VC Issuer-->>-Wallet: VP Catalogue generated
    Wallet-->>-Provider Catalog: VP generated
    Provider Catalog->>+Pub/Sub broker: Send VP for Provider Catalogue
```

### Federated catalogue management
```mermaid
sequenceDiagram
    participant Pub/Sub broker
    participant Federated Catalog
    participant Rule checker
    participant Federated Catalog Cache Node
    participant Federated Catalog Database
    participant Provider User Agent

    Pub/Sub broker->>+Federated Catalog: Send message with Provider Catalogue VP
    Federated Catalog->>+Rule checker: Check Provider Catalogue VP
    Rule checker->>+Rule checker: Verifying whether Participant Certificate has already registred in the Registry
    Rule checker->>+Rule checker: Verifying whether Participant is a Provider
    Rule checker->>+Rule checker: Verifying the signatures of all VCs/VP in the request
    Rule checker->>+Rule checker: Verifying whether all VCs belong to the same Participant
    Rule checker->>+Rule checker: Verifying whether Compliance and FC belong to the same Federation
    Rule checker->>+Rule checker: Verifying Object Integrity of VCs
    alt All rules are checked
    Rule checker->>Federated Catalog: OK
    loop For each new VC
        Federated Catalog->>Provider User Agent: Get provider's VC from its did
        Provider User Agent-->>Federated Catalog: VC Object
        Federated Catalog->>Federated Catalog Cache Node: Stores VC DID and VC Object
        Federated Catalog->>Federated Catalog Database: Update provider graph objects
    end
    loop For each deleted VC
        Federated Catalog->>Federated Catalog Cache Node: remove VC DID and VC Object
        Federated Catalog->>Federated Catalog Database: Update provider graph objects
    end
    else At least one rule is not checked
        Rule checker->>Federated Catalog: Not valid
    end
```
