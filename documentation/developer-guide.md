# Developer Guide

## Technologies

- Flask API to expose endpoints
- Swagger to explain endpoints and format
- GraphDB to store data and request with SPARQL
- Redis to have a cache between provider catalog and federated catalog

We choose do not expose SPARQL but to expose search methods with filters.

## Util commands

See the content of the [Makefile](./Makefile) to view all the util commands.

### Local dev setup

- Install python 3.10
- Install pre-commit
- Install Docker and Docker Compose

Execute:

```sh
pre-commit install --install-hooks
python -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```

### Local dev setup (Nix users)

For nix user, you can simply create a .envrc file for direnv:

```sh
use_nix
pre-commit install --install-hooks
```

### Pre-commit and Git

On every commit, a set of pre-commit hooks **must** be runned to ensure a minimum of quality checks. pre-commit hooks are also executed on the Gitlab-ci pipelines.

Every commit will trigger pre-commit execution.

You can manually launch pre-commit:

```sh
# pre-commit only on modified files
pre-commit run
# pre-commit on all files
pre-commit run -a
```

### Launch image in local environment thanks to minikube

Start and configure minikube

```bash
minikube start

# Enable local env with local registry (otherwise local K8s couldn't load images)
eval $(minikube -p minikube docker-env)

# Build again docker image to load it into local registry
make image

# Use config file (after change image link to use local registry)
kubectl apply -f deployment/generated_manifests/file.yaml

# Create a port-fowarding to call catalog API
kubectl port-forward service/federated-catalogue-api 5000:http -n abc-federation

# Check connection
curl -X GET http://localhost:5000/healthcheck
```
