# Guide to deploy Federated Catalogue to kubernetes clusters

## Context
Currently, the deployment of applications on the demo cluster differs from the method used on the demo cluster.

Two strategies have been adopted:
- Gitops approach with the gitlab agent on the demo cluster
- CICD workflow approach on the dev cluster.

Each method has its advantages and disadvantages
. Nevertheless, it is necessary to standardize the deployments so that it is understandable by all project stakeholders.

The first approach does not produce any logs and it is quite difficult to know if new deployments have been made.
The second approach seems to be the most appropriate because we see what happens during the deployment.

## Description
As described in [Using Gitlab CI/CD with a Kubernetes Cluster](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html), we deployed a gitlab agent in each kubernetes cluster and added gitlab agent configuration in
[k8s-deployment](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment/-/tree/main/.gitlab/agents) repository.

| Kubernetes Cluster | Agent name |
|--------------------|------------|
| DEMO               | default    |
| DEV                | dev        |

So gitlab agent `dev` is configured to deploy to `DEV` cluster, and agent `default` to `DEMO` cluster for all projects in group `deployment-scenario`.

You can have more information on each [Gitlab agents](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment/-/clusters).




## Usage

The following instructions have been tested on the [Common federation service catalogue](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/common-federation-service-catalogue) repository.

### Add a deploy stage in the gitlab-ci.yaml file

Ensure you pipeline has a **deploy** phase, just after the image is built.

```yaml
stages:
  - test
  - build
  - deploy
```

Add a job definition for the **deploy:dev** step :
```yaml
deploy:dev:
  stage: "deploy"
  needs:
    - job: "build:api"
  image:
    name: nixos/nix:2.11.1
    entrypoint: [""]
  tags:
    - k8s-local
  variables:
    PACKAGE_PATH: deployment/packages/overlays/dev-participants
  script:
    - nix-env -iA nixpkgs.kustomize nixpkgs.kubectl
    - kubectl config get-contexts
    - kubectl config use-context gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment:dev
    - kubectl get pods -n gitlab-agent-dev
    # Update the kustomize overlay with the image reference.
    - cd ${PACKAGE_PATH}
    - kustomize edit set image api-image-name=${IMAGE_API_REF}
    # Generate the definitive kubernetes manifests using the kustomize overlay
    - kubectl apply -k . --timeout=300s --wait=true
    - kubectl delete pods --selector app=catalogue-api --all-namespaces

  rules:
      - if: $CI_COMMIT_BRANCH == 'develop'
```

In the above snippet, please note:

* `kubectl config use-context gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment:dev` which indicate the job will deploy on the gitlab-agent named `dev`, and so on the **dev** kubernetes cluster.
* `PACKAGE_PATH: deployment/packages/overlays/dev-participants` to indicate the **dev** manifests are inside a different folder from the **demo** ones

and a job definition for the **deploy:demo** step :

```yaml
deploy:dev:
  stage: "deploy"
  needs:
    - job: "build:api"
  image:
    name: nixos/nix:2.11.1
    entrypoint: [""]
  tags:
    - k8s-local
  variables:
    PACKAGE_PATH: deployment/packages/overlays/demo-participants
  script:
    - nix-env -iA nixpkgs.kustomize nixpkgs.kubectl
    - kubectl config get-contexts
    - kubectl config use-context gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment:default
    - kubectl get pods -n gitlab-agent
    # Update the kustomize overlay with the image reference.
    - cd ${PACKAGE_PATH}
    - kustomize edit set image api-image-name=${IMAGE_API_REF}
    # Generate the definitive kubernetes manifests using the kustomize overlay
    - kubectl apply -k . --timeout=300s --wait=true
    - kubectl delete pods --selector app=catalogue-api --all-namespaces

  rules:
      - if: $CI_COMMIT_BRANCH == 'main'
```

In the above snippet, please note:

* `kubectl config use-context gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment:default` which indicate the job will deploy on the gitlab-agent named `default`, and so on the **demo** kubernetes cluster.
* `PACKAGE_PATH: deployment/packages/overlays/demo-participants` to indicate the **demo** manifests

**You do not have to have manifest:update and manifest:push anymore**



## References
For more information, you can read this [Deployment guide to dev cluster](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment/-/blob/main/docs/deploy-guide-dev.md)
