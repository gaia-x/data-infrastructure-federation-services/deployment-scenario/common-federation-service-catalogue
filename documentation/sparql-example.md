## Sub request example to have n locations associated to 1 service

### SPARQL

```SQL
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

select distinct ?provider ?service ?service_type ?provider_service_id ?title (concat("[", group_concat(distinct ?m_location ;separator=", "), "]") as ?m_location)
where {
        ?service service:providedBy ?provider .
        ?service service:serviceType ?service_type .
        ?service service:providerServiceId ?provider_service_id .
        ?service service:serviceTitle ?title .
        ?service service:isAvailableOn ?location .
        {
            select distinct ?location2 (concat("{",
                        "'location_id': '",str(?location2),"',",
                        "'country_name': '",str(?country_name),"',",
                        "'state': '",str(?state),"',",
                        "'urban_area': '",str(?urban_area),"',",
                        "'provider_designation': '",str(?provider_designation),"'",
                        "}") as ?m_location)
            where {
                ?location2 participant:country ?country_name .
                ?location2 participant:state ?state .
                ?location2 participant:urbanArea ?urban_area .
                ?location2 participant:providerDesignation ?provider_designation .
            }
        }
        FILTER(?location = ?location2) .
}
group by ?provider ?service ?service_type ?provider_service_id ?title
limit 100
```

### Mapper to resul

```python
results = []

for rs in result_services:
    if len(rs) != 0:
        results.append({
            "provider_did": rs['provider']['value'],
            "service_did": rs['service']['value'],
            "service_type": rs['service_type']['value'],
            "provider_service_id": rs['provider_service_id']['value'],
            "service_title": rs['title']['value'],
            "locations": json.loads((rs['m_location']['value']).replace("'", "\""))
        })
```

## How to search on service ?

### V1

```SQL
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

#select ?service ?provider ?description ?keyword ?provider_service_id ?service_title ?location ?country_name ?state ?urban_area ?provider_designation ?service_type
select distinct *
where {
    ?service service:providedBy ?provider .
	?service service:providerServiceId ?provider_service_id .
    ?service service:serviceTitle ?service_title .
    ?service service:isAvailableOn ?location .
    ?location participant:country ?country_name .
    ?location participant:state ?state .
    ?location participant:urbanArea ?urban_area .
    ?location participant:providerDesignation ?provider_designation .
    ?service dct:description ?description .
    {
        select ?service (concat("['", (group_concat(distinct ?service_type ; separator="', '")), "']") as ?service_type)
            where {
            	OPTIONAL {?service service:serviceType ?service_type} .
            }
        group by ?service
    }
    {
        select ?service (concat("['", (group_concat(distinct ?layer ; separator="', '")), "']") as ?layer)
            where {
	            OPTIONAL {?service service:layer ?layer} .
            }
        group by ?service
    }
    {
        select ?service (concat("['", (group_concat(distinct ?keyword ; separator="', '")), "']") as ?keyword)
            where {
	            OPTIONAL {?service dcat:keyword ?keyword} .
            }
        group by ?service
    }
    FILTER(?provider = <did:web:gigas.provider.gaia-x.community:participant:10f73b0bf81e10616bd3ac78c0f77c34c9a6022353ee0077b6e09e87d33ffb9e/data.json>)
}
limit 100
```

### V2

```SQL
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

select distinct ?service ?provider ?provider_service_id ?service_title ?location ?country_name ?state ?urban_area ?provider_designation ?description (concat("['", (group_concat(distinct ?service_type ; separator="', '")), "']") as ?service_type) (concat("['", (group_concat(distinct ?layer ; separator="', '")), "']") as ?layer) (concat("['", (group_concat(distinct ?keyword ; separator="', '")), "']") as ?keyword)
where {
    ?service service:providedBy ?provider .
	?service service:providerServiceId ?provider_service_id .
    ?service service:serviceTitle ?service_title .
    ?service service:isAvailableOn ?location .
    ?location participant:country ?country_name .
    ?location participant:state ?state .
    ?location participant:urbanArea ?urban_area .
    ?location participant:providerDesignation ?provider_designation .
    ?service dct:description ?description .
	?service service:serviceType ?service_type .
    ?service service:layer ?layer .
    ?service dcat:keyword ?keyword .
}
group by ?service ?provider ?provider_service_id ?service_title ?location ?country_name ?state ?urban_area ?provider_designation ?description
limit 100
```

We keep V2 because code to add filters is easier to understand and to code.

## How to update data

### DELETE PROVIDER

#### DELETE PROVIDER NODE

```SQL
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

delete {
    ?provider ?p ?o
}
where {
    ?provider ?p ?o
    FILTER( ?provider = <did:web:example.org/data.json>)
}
```

#### DELETE PROVIDER SERVICE

```SQL
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

delete {
    ?service ?p ?o
}
where {
    ?service rdf:type service:ServiceOffering .
    ?service service:providedBy ?provider .
    ?service ?p ?o .
    FILTER( ?provider = <did:web:example.org/data.json>)
}
```

## How to have metrics on object type

```SQL
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX compliance: <https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

select *
where {
    {
        select ?type (count(?provider) as ?count)
        {
            ?provider rdf:type ?type .
            ?provider rdf:type participant:Provider .
        }
        group by ?type
    }
    UNION
    {
        select ?type (count(?service_offering) as ?count)
        {
            ?service_offering rdf:type ?type .
            ?service_offering rdf:type service:ServiceOffering .
        }
        group by ?type
    }
    UNION
    {
        select ?type (count(?service_offering) as ?count)
        {
            ?service_offering rdf:type ?type .
            ?service_offering rdf:type service:LocatedServiceOffering .
        }
        group by ?type
    }
    UNION
    {
        select ?type (count(?item) as ?count)
        {
            ?item rdf:type ?type .
            ?item rdf:type compliance:ComplianceReference .
        }
        group by ?type
    }
}
limit 100
```

#### DELETE PROVIDER LOCATION and its properties

```SQL
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
delete { ?location ?p ?o }
where {
    {
        select ?location (count(?location) as ?c)
        where {
            ?location participant:hasProvider ?provider .
            ?location rdf:type participant:Location .
            filter (?provider = <did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json>)
            filter (?location = <did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/80ba30db8dd2622a93be652e8db5b3f3085baabc50d387d1dc3311f76455e22d/data.json>)
        }
        group by ?location
	}
    filter (?c = 1)
    ?location ?p ?o
}
```
#### DELETE PROVIDER SERVICE OFFERING and its properties

```SQL
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>

delete {?service ?p ?o}
where {
    {
        select ?service (count(?service) as ?c)
        where {
            ?service service:providedBy ?provider .
            ?service rdf:type service:ServiceOffering .
            filter (?provider = <did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json>)
            filter (?service = <did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/service-offering/e1d5bfdac8d12397f31c89747352313d8ca890494b0aff4407bb3dad8be13c04/data.json>)
        }
        group by ?service
	}
    filter (?c = 1)
    ?service ?p ?o
}
```

#### DELETE PROVIDER LOCATED SERVICE OFFERING and its properties

```SQL
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>

delete (?service ?p ?o)
where {
    {
        select ?service (count(?service) as ?c)
        where {
            ?service rdf:type service:LocatedServiceOffering .
            ?service service:isImplementationOf ?serviceOffering .
            ?serviceOffering service:providedBy ?provider .
            filter (?service = <did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/117ffbeabd2902fbae7bc8a9a7b810e91ab7e3c61a8206f1189ca1d7bf65c967/data.json>)
            filter (?provider = <did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json>)
        }
        group by ?service
	}
    filter (?c = 1)
    ?service ?p ?o
}```
