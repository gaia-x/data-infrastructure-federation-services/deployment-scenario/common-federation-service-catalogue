# How to guide

## How to launch federated-catalogue locally

1. Run a Local REDIS
```shell
docker run --rm -p 6379:6379 -v mon-volume-redis:/data --name redis -t redis:7.0.11-alpine --save "" --appendonly no
```

2. Run a local GRAPH_DB
```shell
docker run --rm -p 127.0.0.1:7200:7200 --name graphdb-instance-name -t khaller/graphdb-free:10.1.0
```

3. Launch ```__main__.py``` with following parameters
```shell
GRAPHDB_URL=http://localhost:7200
REDIS_HOST=redis://127.0.0.1:6379/0
REDIS_PUB_SUB_HOST=redis://127.0.0.1:6379/1

API_KEY_AUTHORIZED=<API_KEY>
API_PORT_EXPOSED=5002
CES_URL=https://ces-main.lab.gaia-x.eu
PARENT_DOMAIN=dev.gaiax.ovh
PARTICIPANT_NAME=abc-federation
```

## How to filter

You can add filter inside the body of the Query endpoint. Each filter is equivalent to an **AND** with the other filters.

### By Provider(s)

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{
    "provider_ids": ["did:web:aruba.provider.dev.gaiax.ovh:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/vc/legal-participant/legal-participant.json"]
}'
```

Notes :
- /GET /api/filters-data to get data use to filter
- provider_ids contains the IRI of the Provider Verifiable Credentials.
- provider_ids is an array and can be multi-values (eq to an OR)

### By Location(s)

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{
    "location_ids": ["did:web:aruba.provider.dev.gaiax.ovh:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/vc/229cfb6b85ff44205eb22e33bc01647405743ec976269fab413b012debeec223/data.json"]
}'
```

Notes :
- /GET /api/locations to get data use to filter
- location_ids is the IRI of the Location Verifiable Credentials.
- location_ids is an array and can be multi-values (eq to an OR)

### By layer(s)

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{
    "layers": ["IAAS"]
}'
```

Notes :
- /GET /api/filters-data to get data use to filter
- This filter can have multi-values, this values will be computed with an **OR** operator.


### By Service Type(s) or other keyword(s)

Service offering has a gx:keyword field. We can filter on value inside this array.

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{

    "keywords": ["Compute", "Container"]

}'
```

Notes :
- /GET /api/filters-data to get data use to filter
- This filter can have multi-values, this values will be computed with an **OR** operator.

### By Compliance Reference(s) (ex: ISO 27001)

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{
    "compliance_ref_ids": ["did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-reference/e0423237acdcb2844f0db3c688353be38e04dc1078ce200a06d94f74f40e1641/data.json"]
}'
```

Notes :
- /GET /api/filters-data to get data use to filter
- compliance_ref_ids is an array and can be multi-values (eq to an OR)


### By Level of Aster-X Conformity

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{
    "aster_label_ids": ["https://aster-x.demo23.gxfs.fr/participant/d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-label/42b03421cde76b84ff90b7eb6cb1a2821ad6388bdb15b3d67cb73fd5c508da0b/data.json"]
}'
```

Notes :
- /GET /api/filters-data to get data use to filter
- aster_label_ids is an array and can be multi-values (eq to an OR)

### By Countries

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{
    "countries": ["France"]
}'
```

Notes :
- /GET /api/filters-data or /GET /locations to get data use to filter

### By urban area(s)

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{
    "urban_areas": ["Paris"]
}'
```

Notes :
- /GET /api/filters-data or /GET /api/country_urban_area to get data use to filter

### Search with service name

```shell
curl --location '{FEDERATED_CATALOGUE_URL}/query' \
--header 'Content-Type: application/json' \
--data '{
    "search_service_name": "s3"
}'
```

Notes :
- Search all services which contains 's3' in the name of the service
