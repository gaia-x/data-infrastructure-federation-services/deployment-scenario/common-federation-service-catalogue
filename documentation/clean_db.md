## Clean service-offering

```sql
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

delete {
  ?service ?p ?o
}
where {
    ?service rdf:type service:ServiceOffering .
    ?service service:providedBy ?provider .
    ?service ?p ?o .
    FILTER( ?provider = <did:web:docaposte-certinomis.provider.gaia-x.community:participant:8ab4e339575fcbc5fb07c40258ec9ebceab38e7d1bba701589434f605fbf11e0/data.json>)
}
```

## Clean located-service-offering

```sql
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

delete {
  ?located_service ?p ?o
}
where {
    ?location rdf:type participant:Location .
    ?location participant:hasProvider ?provider .
    ?located_service service:isHostedOn ?location .
    ?located_service ?p ?o .
    FILTER( ?provider = <did:web:docaposte-certinomis.provider.gaia-x.community:participant:8ab4e339575fcbc5fb07c40258ec9ebceab38e7d1bba701589434f605fbf11e0/data.json>)
}
```

## Clean location

```sql
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

delete {
  ?location ?p ?o
}
where {
    ?location rdf:type participant:Location .
    ?location participant:hasProvider ?provider .
    ?location ?p ?o .
    FILTER( ?provider = <did:web:docaposte-certinomis.provider.gaia-x.community:participant:8ab4e339575fcbc5fb07c40258ec9ebceab38e7d1bba701589434f605fbf11e0/data.json>)
}
```


## Clean provider

```sql
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

delete {
    ?provider ?p ?o
}
where {
    ?provider ?p ?o
    FILTER( ?provider = <did:web:docaposte-certinomis.provider.gaia-x.community:participant:8ab4e339575fcbc5fb07c40258ec9ebceab38e7d1bba701589434f605fbf11e0/data.json>)
}
```
