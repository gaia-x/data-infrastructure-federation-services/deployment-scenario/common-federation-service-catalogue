# Federated Catalog Documentation

## Table of Contents

- [Glossary](#glossary)
- [Description](#description)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [Environment Variables](#environment-variables)
- [Deployment Instructions on Kubernetes](#deployment-instructions-on-kubernetes)
- [Catalogue synchronisation](#catalogue-synchronisation)
- [Appendices](#appendices)
- [Contributing](#contributing)
- [License](#license)

## Glossary

| Term                | Description                                                                                                                    |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------|
| Participant-Catalog | Each participant can expose through this component the list of Verifiable Credentials he wants to share with the federation.   |
| Federated-Catalog   | Concatenation of the participant catalogs inside a graph database with feature of search and filtering Verifiable Credentials. |

## Description

This project is a Federated Catalogue system. Each participant in a federation can expose a list of Verifiable
Credentials they want to share with the federation through their Participant Catalog. The Federated Catalog is a
concatenation of all the Participant Catalogs, stored inside a graph database with search and filtering features for
Verifiable Credentials.
The system also includes a rule checker to verify the compliance of objects submitted to the federated catalogue with
federation policies.

Federated catalogue synchronisation is done [by different methods](#catalogue-synchronisation).

This project is developed in Python 3.12, Redis and GraphDB.

## Prerequisites

This project has several prerequisites that you need to meet before you can install and run it on your machine.
Here are the prerequisites:

1. **Python**: The project is primarily developed in Python. Make sure you have Python installed on your machine.
   The project requires Python version 3.12 or higher.

2. **Poetry**: This project uses Poetry for dependency management. If you don't have Poetry installed,
   you can install it following the instructions on
   the [official Poetry website](https://python-poetry.org/docs/#installation).
   The project requires Poetry version 1.8 or higher.

3. **Docker**: Docker is used for creating and managing the project's containers. Make sure Docker is installed and
   running on your machine. You can download Docker from the
   [official Docker website](https://www.docker.com/products/docker-desktop).

4. **Docker Compose**: Docker Compose is used to define and run multi-container Docker applications.

5. **Environment Variables**: You will need to set up several environment variables for the project to run correctly.
   You can set these variables in a `.env` file in the root of your project.

Please ensure that all these prerequisites are met before you proceed with the installation and configuration of the
project.

## Installation

Before you can run the Provider Catalogue, you need to install the necessary dependencies. This project uses Python,
so you will need to have it installed on your machine. Here are the steps to install the project:

1. Clone the repository to your local machine:

```bash
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue.git
```

2. Navigate to the project directory:

```bash
cd federated-catalogue
```

3. Install the project dependencies. This project uses Poetry for dependency management:

```bash
poetry install
```

## Usage

> **Important Notice**: To be able to get data into the federated catalogue, you can configure the federated catalogue
> to pull data from the provider catalogue or to pull data from the Gaia-X Centralized Event Service (CES).

You can run Federated Catalogue in a Docker container or locally on your machine. Here are the instructions for both
methods.

### Setting environment variables

The easiest way to set environment variables is to create a `.env` file in the root of your project and add the
environment variables there:

```bash
touch .env
```

Then open the `.env` file and add your environment variables:

```env
LOG_LEVEL="DEBUG"
ENVIRONMENT="DEV"
API_KEY_AUTHORIZED="your_api_key"
API_PORT_EXPOSED="5001"
REDIS_HOST="redis://127.0.0.1:6379/0"
REDIS_PUB_SUB_HOST="redis://127.0.0.1:6379/1"
RULE_CHECKER_URL="http://localhost:3003"
CES_URL="https://ces-v1.lab.gaia-x.eu"
PARTICIPANT_NAME="your_participant_name"
PARENT_DOMAIN="your_domain"
```

Replace `your_api_key`, `your_participant_name`, `your_domain` with your actual values.

> Note: See [Environment Variables](#environment-variables) to have a description of each environment variable.

### Running the application locally

First of all, you need to run a redis server and graphdb on your machine.

You can do this by running the following command:

```bash
docker run --rm -p 127.0.0.1:6379:6379 --name some-redis -d redis:7.2-rc-alpine
```

```bash
docker run --rm -p 7200:7200 --name graphdb-instance-name -t khaller/graphdb-free:10.1.0
```

> Note: Make sure you have docker installed on your machine.

Then, you can run the application locally by running the following command:

Run the application:

```bash
poetry run python -m federated_catalogue
```

This will start the application. It will be accessible on `http://localhost:5001`.

To stop the application, press `Ctrl + C`.

### Running the application in a Docker container

The docker-compose file is already provided in the project. You can run the application in a Docker container by running
the following command:

```bash
docker-compose up
```

This will start the application. It will be accessible on `http://localhost:5001`.

To stop the application, press `Ctrl + C`.

## Environment Variables

Here is the list of environment variables used in your project, along with their explanations and default values:

| Environment Variable           | Explanation                                                                                                                                          | Default Values                          |
|--------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------|
| `LOG_LEVEL`                    | Sets the application's log level. Possible values include `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `CRITICAL`.                                       | `DEBUG`                                 |
| `ENVIRONMENT`                  | Sets the application's environment. Possible values could be `DEV`, `TEST`, `STAGING`, or `PROD`.                                                    | `DEV`                                   |
| `API_KEY_AUTHORIZED`           | This is the API Key for authorization. The value would be a string that you generate and keep secret.                                                | None (required)                         |
| `API_PORT_EXPOSED`             | This is the exposed port for the API. The value would be an integer representing the port number, such as `5001`.                                    | `5001`                                  |
| `REDIS_HOST`                   | This is the Redis Host DSN. The value would be a string in the format `redis://hostname:port/db_number`, such as `redis://127.0.0.1:6379/0`.         | `redis://127.0.0.1:6379/0`              |
| `REDIS_PUB_SUB_HOST`           | This is the Redis Pub/Sub Host DSN. The value would be a string in the format `redis://hostname:port/db_number`, such as `redis://127.0.0.1:6379/1`  | `redis://127.0.0.1:6379/1`              |
| `GRAPHDB_URL`                  | This variable sets the URL for the GraphDB database.The value would be a string representing the URL, such as `http://127.0.0.1:7200`.               | `http://127.0.0.1:7200`                 |
| `CES_URL`                      | This is the URL for the centralized event system. The value would be a string representing the URL, such as `https://ces-development.lab.gaia-x.eu`. | `https://ces-development.lab.gaia-x.eu` |
| `RULE_CHECKER_URL`             | This is the URL for the rule checker. The value would be a string representing the URL, such as `http://localhost:3003`.                             | `http://localhost:3003`                 |
| `PARTICIPANT_NAME`             | This is the name of the participant. The value would be a string representing the participant's name, such as `ovhcloud`.                            | None (required)                         |
| `PARENT_DOMAIN`                | This is the application domain name. The value would be a string representing the domain name, such as `provider.dev.gaiax.ovh`.                     | None (required)                         |

## Deployment Instructions on Kubernetes

This project includes a Kubernetes deployment files that you can use to deploy the application on a Kubernetes cluster.

> Note: You can find the base deployment files in the `deployment/packages/base` directory. This project uses Kustomize
> for managing Kubernetes resources and kubectl for deploying them.

To deploy the application on a Kubernetes cluster, follow these steps:

1. **Create a Namespace**: You need to create a namespace where you will deploy your application, you can do this using
   the `kubectl` command:

```bash
kubectl create namespace abcd
```

2. **Create a Secret for API_KEY_AUTHORIZED **: You need to create a Kubernetes Secret to store API_KEY_AUTHORIZED
   environment variables. You can do this using the `kubectl` command:

```bash
kubectl create secret generic catalogue-api-key --from-literal=api_key_authorized=a_secret -n abcd
```

This command creates a Secret named `catalogue-api-key` in the `abcd` namespace.

3. **Create a ConfigMap for Environment Variables**: You need to create a ConfigMap to store the environment variables
   for the application. You can do this using the `kubectl` command:

```bash
kubectl create configmap catalog-configuration --from-env-file=.env -n abcd
```

4. **Deploy the Application**: You can deploy the application using the `kubectl` command:

```bash
kubectl apply -k deployment/packages/base/ -n abcd
```

This command deploys the application to the `abcd` namespace using the configuration files in
the `deployment/packages/base/` directory.

4. **Verify the Deployment**: You can verify that the application has been deployed successfully using the `kubectl`
   command:

```bash
kubectl get pods -n abcd
```

This command lists all the pods in the `ovhcloud` namespace. You should see a pod for your application in the list.

5. **Access the Application**: If your application exposes a web interface, you can access it using port forwarding. You
   can do this using the `kubectl` command:

```bash
kubectl port-forward svc/catalogue-api 5001:80 -n abcd
```

This command forwards traffic from port 5001 on your local machine to port 80 on the `catalogue-api` service in
the `abcd` namespace. You can access the application by navigating to `http://localhost:5001` in your web browser.

Please note that these instructions are for a basic deployment. Depending on your specific requirements, you may need to
modify these instructions. For example, you may need to use a different namespace, or you may need to configure
additional resources such as Ingresses or Persistent Volumes.

## Catalogue synchronisation

Federated catalogue aggregates catalogue from different provider. You can add your catalogue by several ways :

- By pushing your catalogue inside Redis consumed by the federated catalogue. You need to respect the catalogue
  format [https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/service/#catalogue]. To have a deep dive
  explanation you can read [this document](./catalogue-redis-synchronisation.md).
- By pushing your catalogue inside the Credential Event Service [https://gaia-x.eu/news-press/gaia-x-and-catalogues/].
  Aster-X Federated Catalogue regularly consumes the CES and integrates compatible VC inside his database.
- By register your catalogue inside the Federated Catalogue. The registered URL need to provide a catalogue with
  compatible format [https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/service/#catalogue] and
  Federated Catalogue will regularly consume this catalogue to integrate it in his
  database [more explanation](./catalogue-http-synchronisation.md).

## Appendices

- [Clean Database](./clean_db.md): This document explains how to clean the graph database.

## Contributing

To contribute to Provider Catalogue, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

For more information on how to contribute to this project, please refer to the [CONTRIBUTING.md](../CONTRIBUTING.md)
file.

## License

This project uses the following license: [Apache v2.0 License](../LICENSE).
