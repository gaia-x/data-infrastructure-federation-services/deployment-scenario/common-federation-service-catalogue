# -*- coding: utf-8 -*-
import hashlib
import os
from enum import Enum
from functools import lru_cache
from typing import Final

from dotenv import load_dotenv
from pydantic import BaseSettings, validator
from pydantic.networks import AnyHttpUrl, RedisDsn
from pydantic.types import SecretStr

load_dotenv()

ROOT_DIR: Final = os.path.realpath(os.path.dirname(__file__))
CONTEXT_CATALOG: Final = {
    "@context": ["https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"],
}

TYPE_CATALOG: Final = {
    "@type": "gx:Catalogue",
}

CATALOG_PREFIX_KEY: Final = "catalogue"
FEDERATION_REPOSITORY: Final = "abc-federation"

SERVICE_TYPES: Final = [
    "Authentication",
    "Backup",
    "Bare Metal",
    "Collaboration",
    "Compute",
    "Container",
    "Data Analytics",
    "Data Base",
    "Digital Signature",
    "ID management",
    "IoT",
    "Logs",
    "Machine Learning",
    "Networking",
    "Storage",
    "Translation",
    "Virtual Machine",
    "VPN",
    "VPS",
    "Webhosting",
    "Data Product",
]

LAYERS: Final = ["IAAS", "PAAS", "SAAS"]


class LogLevelEnum(str, Enum):
    critical = "CRITICAL"
    fatal = "FATAL"
    error = "ERROR"
    warning = "WARNING"
    info = "INFO"
    debug = "DEBUG"


class EnvironmentEnum(str, Enum):
    dev = "DEV"
    prod = "PROD"
    test = "TEST"


class Settings(BaseSettings):
    log_level: LogLevelEnum = LogLevelEnum.debug
    environment: EnvironmentEnum = EnvironmentEnum.dev
    api_key_authorized: SecretStr
    api_port_exposed: int = 5001
    redis_host: RedisDsn = "redis://127.0.0.1:6379/0"
    graphdb_url: AnyHttpUrl = "http://127.0.0.1:7200"
    redis_pub_sub_host: RedisDsn = "redis://127.0.0.1:6379/1"
    rule_checker_url: AnyHttpUrl = "http://127.0.0.1:3003"
    ces_url: AnyHttpUrl = "http://127.0.0.1"
    participant_name: str
    parent_domain: str

    @validator("api_key_authorized", pre=True)
    @classmethod
    def apply_root(cls, value: str):
        """
        The apply_root function is a class method that takes in a string value and returns the same string with
        whitespace stripped from both ends of the string. If the input value is None, then it will return None.

        Args:
            cls: Pass the class of the object that is being created
            value: str: Define the type of value that will be passed to the function

        Returns:
            The value of the field if it is none

        """
        return value if value is None else value.strip().replace("\n", "")

    @property
    def participant_hash(self) -> str:
        """
        The participant_hash function is used to generate a unique hash for each participant.
        This hash is used as the key in the dictionary of participants, and it's also stored in
        the database so that we can look up a participant by their name.

        Args:
            self: Represent the instance of the class

        Returns:
            The sha-256 hash of the participant's name

        """
        return hashlib.sha256(bytes(self.participant_name, "utf-8")).hexdigest()

    @property
    def did_url(self) -> str:
        """
        The did_url function returns a string that is the DID URL for this participant.
        The format of the DID URL is:
            did:web:[participant_name].[parent_domain]:participant:[participant_hash]/data.json

        Args:
            self: Bind the method to an object

        Returns:
            The participant's did url
        """
        return (
            f"https://{self.participant_name}.{self.parent_domain}/"
            f"legal-participant-json/"
            f"{self.participant_hash}/"
            f"data.json"
        )


@lru_cache
def get_settings() -> Settings:
    """
    The get_settings function is a factory function that returns an instance of the Settings class.
    The Settings class contains all the settings for this project, and it's used by other modules to access
    those settings.

    Args:

    Returns:
        A settings object
    """
    return Settings()
