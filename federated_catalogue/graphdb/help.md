API disponible avec graphdb : https://rdf4j.org/documentation/reference/rest-api/

RDFLib python documentation : https://rdflib.readthedocs.io/en/stable/

Image documentation : https://hub.docker.com/r/khaller/graphdb-free


# Initialisation commands

Launch our image of graphDB
```bash
docker run --rm -p 127.0.0.1:7200:7200 --name graphdb-instance-name -t my-graphdb:latest
```

Launch image with config
```bash
docker run --rm -p 127.0.0.1:7200:7200 --name graphdb-instance-name -v /Users/geromeegron/src/wescale/gaia-x/demo-22.11/common-federation-service-catalogue/code/service_catalogue/database/config:/repository.init/abc-federation -t khaller/graphdb-free:latest
```

Load repository configuration (repository name : federation-tes)
```bash
curl -X POST --header "Content-Type:multipart/form-data" \
-F "config=@./config.ttl" "http://localhost:7200/rest/repositories"
```

Load JSON-ld data into repository
```bash
curl -X PUT --header 'Content-Type: application/ld+json' \
-T catalog.json http://localhost:7200/repositories/abc-federation/statements
```


kubectl port-forward service/api 5000:http -n federated-catalogue
kubectl port-forward service/persistent-graphdb 5072:http -n abc-federation
