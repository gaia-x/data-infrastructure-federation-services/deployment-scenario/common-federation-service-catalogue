# -*- coding: utf-8 -*-
import asyncio
import logging
from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI
from federated_catalogue import graphdb_client
from federated_catalogue.Config import ROOT_DIR, Settings, get_settings
from federated_catalogue.domain.interfaces.graph_repository import GraphRepository
from federated_catalogue.domain.interfaces.repository import CatalogueRepository
from federated_catalogue.infra.api.rest.api import create_fastapi_application
from federated_catalogue.infra.spi.db.redis import RedisCatalogueRepository
from federated_catalogue.infra.spi.graph_repository.graphdb import GraphDBRepository
from federated_catalogue.infra.spi.rule_checker.checker import ABCRuleCheckerClient
from federated_catalogue.observability.logger import configure_logging
from federated_catalogue.services.catalogue import CatalogueService
from federated_catalogue.services.ces_event_consumer import CesEventConsumer
from federated_catalogue.services.healthcheck import HealthcheckService
from federated_catalogue.services.redis_catalogue_subscriber import (
    RedisCatalogueSubscriber,
)
from prometheus_fastapi_instrumentator import Instrumentator
from redis.asyncio.client import Redis

log = logging.getLogger(__name__)

if __name__ == "__main__":
    settings: Settings = get_settings()
    configure_logging()

    graphdb_client.launch_init_repository("abc-federation", f"{ROOT_DIR}/graphdb/config/config.ttl")

    redis_client = Redis.from_url(url=settings.redis_host, decode_responses=True)
    catalogue_repository: CatalogueRepository = RedisCatalogueRepository(redis_client=redis_client)
    graph_repository: GraphRepository = GraphDBRepository()

    catalogue_service = CatalogueService(catalogue_repository, graph_repository)
    healthcheck_service = HealthcheckService(repository=catalogue_repository)
    ces_event_consumer = CesEventConsumer(catalogue_service=catalogue_service, redis_client=redis_client)

    rule_checker_client = ABCRuleCheckerClient(checker_host=settings.rule_checker_url)

    @asynccontextmanager
    async def lifespan(current_app: FastAPI):
        log.info("Launching Redis Catalogue Subscriber")
        redis_catalogue_subscriber = RedisCatalogueSubscriber(catalogue_service, rule_checker_client)
        await asyncio.create_task(redis_catalogue_subscriber.listen())

        log.info(f"Healthcheck endpoint available on : http://127.0.0.1:{settings.api_port_exposed}/healthcheck")
        log.debug(f"GRAPHDB_URL : {settings.graphdb_url}")

        instrumentator.expose(current_app)
        yield

        log.info("Shutting down")
        await redis_catalogue_subscriber.stop()

    app = create_fastapi_application(
        lifespan=lifespan,
        catalogue_service=catalogue_service,
        healthcheck_service=healthcheck_service,
        ces_event_consumer=ces_event_consumer,
    )

    instrumentator = Instrumentator().instrument(app)
    uvicorn.run(app, host="0.0.0.0", port=settings.api_port_exposed, log_config=None)
