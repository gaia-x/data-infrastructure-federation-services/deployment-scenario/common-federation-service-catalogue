# -*- coding: utf-8 -*-
import http
import logging
import ssl
from typing import Any, Final, Optional

import httpx

log = logging.getLogger(__name__)

DEFAULT_TIMEOUT: Final = 10.0
DEFAULT_UNIVERSAL_DID_RESOLVER_URL: Final = "https://dev.uniresolver.io/1.0/identifiers/"


def transform_did_into_url(did: str):
    if is_did_web(did):
        return did.replace(":", "/").replace("did/web/", "https://")
    else:
        return did


def is_did_web(did: str):
    return did.startswith("did:web")


async def get_json_from_url(url: str, timeout=DEFAULT_TIMEOUT) -> Any:
    client = httpx.AsyncClient()
    default_headers = {
        "Content-Type": "application/json",
    }

    try:
        response = await client.get(
            url=url,
            timeout=timeout,
            headers=default_headers,
        )

        match response.status_code:
            case http.HTTPStatus.OK:
                return response.json()
            case http.HTTPStatus.BAD_REQUEST:
                raise BadRequestException(str(response.content))
            case http.HTTPStatus.FORBIDDEN:
                raise NotAuthorizedException(response.json()["message"])
            case _:
                raise HostNotReachable(f"{url} is not reachable: {response.status_code}")
    except ssl.SSLCertVerificationError as err:
        raise HostNotReachable(f"{url} is not reachable: SSL Certificate issue") from err
    except httpx.ConnectError as err:
        raise HostNotReachable(f"{url} is not reachable: Connection Error") from err
    except Exception as err:
        raise HostNotReachable(f"{url} is not reachable") from err
    finally:
        await client.aclose()


async def get_did_document_from_did(did: str) -> Any | None:
    url = f"{DEFAULT_UNIVERSAL_DID_RESOLVER_URL}{did}"

    try:
        result = await get_json_from_url(url=url)

        return result["didDocument"] if "didDocument" in result else None
    except Exception as err:
        log.warning(f"Unable to retrieve document because {err}")
        return None


def get_id_from_jsonld(jsonld: dict[str, Any]) -> Optional[str]:
    """
    The get_id_from_jsonld function returns the @id or id value from a JSON-LD document.

    Args:
        jsonld: dict[str, Any]: json-ld structure to extract id / @id


    Returns:
        The &quot;@id&quot; or &quot;id&quot; key from a json-ld document
    """
    return None if jsonld is None else jsonld.get("@id", jsonld.get("id"))


def get_type_from_jsonld(jsonld: dict[str, Any]) -> Optional[str | list[str]]:
    """
    The get_type_from_jsonld function returns the @type or type value from a JSON-LD object.

    Args:
        jsonld: dict[str, Any]: json-ld structure to extract type / @type

    Returns:
        The @type or type field of a jsonld object
    """
    return None if jsonld is None else jsonld.get("@type", jsonld.get("type"))


class BadRequestException(Exception):
    """
    The BadRequestException class is a custom exception class that is raised when a HTTP request returns a status code
    of 400 (Bad Request). It inherits from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)


class NotAuthorizedException(Exception):
    """
    The NotAuthorizedException class is a custom exception class that is raised when the API returns a status code of
    403 (FORBIDDEN). It inherits from the base Exception class and overrides the __init__ method to set the error
    message.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)


class HostNotReachable(Exception):
    """
    The HostNotReachable class is a custom exception class that is raised when a host is not reachable. It inherits
    from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)
