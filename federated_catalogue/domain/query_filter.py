from pydantic import BaseModel


class QueryFilter(BaseModel):
    provider_ids: list = None
    location_ids: list = None
    keywords: list = None
    countries: list = None
    urban_areas: list = None
    layers: list = None
    compliance_ref_ids: list = None
    aster_label_ids: list = None
    search_service_name: str = None

    def apply_provider_filter(self, filters):
        return (
            filters
            + """
            FILTER(?vc_provider IN %s) ."""
            % self.transform_into_sparql_id_array(self.provider_ids)
        )

    def apply_location_filter(self, filters):
        return (
            filters
            + """
            FILTER(?vc_location IN %s) ."""
            % self.transform_into_sparql_id_array(self.location_ids)
        )

    def apply_keyword_filter(self, filters: str, select_federation_services: str):
        keyword_string = self.transform_into_sparql_array(self.keywords)

        filters = (
            filters
            + """
            FILTER(?keyword IN %s) ."""
            % keyword_string
        )

        select_federation_services = (
            select_federation_services
            + """
            ?service gx:keyword ?keyword ."""
        )

        return filters, select_federation_services

    def apply_country_filter(self, filters: str, select_federation_services: str):
        countries_string = self.transform_into_sparql_array(self.countries)

        filters = (
            filters
            + """
            FILTER(?country IN %s) ."""
            % countries_string
        )

        select_federation_services = (
            select_federation_services
            + """
            ?location aster-conformity:country ?country ."""
        )

        return filters, select_federation_services

    def apply_urban_area_filter(self, filters: str, select_federation_services: str):
        urban_areas_string = self.transform_into_sparql_array(self.urban_areas)

        filters = (
            filters
            + """
            FILTER(?urban_area IN %s) ."""
            % urban_areas_string
        )

        select_federation_services = (
            select_federation_services
            + """
            ?location aster-conformity:urbanArea ?urban_area ."""
        )

        return filters, select_federation_services

    def apply_layer_filter(self, filters: str, select_federation_services: str):
        layers_string = self.transform_into_sparql_array(self.layers)

        filters = (
            filters
            + """
            FILTER(?layer IN %s) ."""
            % layers_string
        )

        select_federation_services = (
            select_federation_services
            + """
            ?service aster-conformity:layer ?layer ."""
        )

        return filters, select_federation_services

    def apply_compliance_ref_filter(self, filters: str, select_federation_services: str):
        filters = (
            filters
            + """
            FILTER(?compliance_reference IN %s) ."""
            % self.transform_into_sparql_id_array(self.compliance_ref_ids)
        )

        select_federation_services = (
            select_federation_services
            + """
            ?compliance_certificate_claim aster-conformity:hasLocatedServiceOffering ?located_service .
            ?compliance_certificate_claim aster-conformity:hasComplianceCertificationScheme ?compliance_certification_scheme .
            ?compliance_certification_scheme aster-conformity:hasComplianceReference ?compliance_reference .
        """
        )

        return filters, select_federation_services

    def apply_aster_label_filter(self, filters: str, select_federation_services: str):
        filters = (
            filters
            + """
            FILTER(?compliance_label IN %s) ."""
            % self.transform_into_sparql_id_array(self.aster_label_ids)
        )

        select_federation_services = (
            select_federation_services
            + """
            ?granted_label aster-conformity:vc ?vc_located_service ."""
        )

        return filters, select_federation_services

    def apply_search_service_name(self, filters: str):
        from rdflib import Literal

        filters = (
            filters
            + """
            FILTER(regex(?service_name, "%s", "i")) ."""
            % Literal(self.search_service_name)
        )

        return filters

    @staticmethod
    def transform_into_sparql_array(array):
        from rdflib import Literal

        array_litteral = []

        for item in array:
            array_litteral.append(f"{Literal(item)}")

        return f"{array_litteral}".replace("[", "(").replace("]", ")")

    @staticmethod
    def transform_into_sparql_id_array(ids: list):
        from rdflib import URIRef

        array_ids = []

        for id in ids:
            array_ids.append(f"<{URIRef(id)}>")

        array_string = f"{array_ids}".replace("[", "(").replace("]", ")").replace("'", "")

        return array_string
