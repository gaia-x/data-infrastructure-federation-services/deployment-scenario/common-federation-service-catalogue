from dataclasses import dataclass, field
from datetime import datetime, timezone
from enum import Enum
from typing import Optional

from dataclasses_json import config, dataclass_json


@dataclass_json
@dataclass
class Catalogue:
    """
    To have a better comprehension about @dataclass_json --> https://pypi.org/project/dataclasses-json/
    """

    provided_by: str = field(metadata=config(field_name="gx:providedBy"))
    get_verifiable_credentials_ids: Optional[str | list[str]] = field(
        metadata=config(field_name="gx:getVerifiableCredentialsIDs")
    )


class SynchroStatusValue(Enum):
    PENDING = "PENDING"
    DONE = "DONE"


@dataclass_json
@dataclass
class SynchroStatus:
    status: SynchroStatusValue
    start_date: str
    end_date: Optional[str] = None

    @staticmethod
    def create_start_status():
        now_utc = datetime.now(timezone.utc)

        http_synchro_status = SynchroStatus(SynchroStatusValue.PENDING, datetime.isoformat(now_utc))

        return http_synchro_status

    @staticmethod
    def create_end_status(start_status):
        now_utc = datetime.now(timezone.utc)

        http_synchro_status = SynchroStatus(
            SynchroStatusValue.DONE, start_status.start_date, datetime.isoformat(now_utc)
        )

        return http_synchro_status
