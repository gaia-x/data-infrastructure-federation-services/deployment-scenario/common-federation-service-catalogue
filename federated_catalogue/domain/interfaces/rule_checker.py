from abc import ABCMeta, abstractmethod
from typing import Any


class RuleChecker(metaclass=ABCMeta):
    @abstractmethod
    async def check_verifiable_presentation(self, verifiable_presentation: Any) -> bool:
        """
        The check_verifiable_presentation function is used to verify that the verifiable presentation
        is valid. It does this by checking that the proof is valid, and then checking that all of the
        claims in the presentation are present in at least one credential. The check_verifiable_presentation
        method should be implemented by a subclass.

        Args:
            self: Bind the method to an object
            verifiable_presentation: Any: Check the verifiable presentation

        Returns:
            A boolean value

        """
        raise NotImplementedError
