# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

from federated_catalogue.domain.query_filter import QueryFilter


class GraphRepository(metaclass=ABCMeta):
    @abstractmethod
    def delete_did_document(self, did_document):
        raise NotImplementedError

    @abstractmethod
    def delete_provider(self, did: str):
        raise NotImplementedError

    @abstractmethod
    def post_json_ld_content(self, json_ld, did=""):
        raise NotImplementedError

    @abstractmethod
    def count_list_vc_service_offering(self, input_filters: QueryFilter):
        raise NotImplementedError

    @abstractmethod
    def list_vc_service_offering(self, input_filters: QueryFilter, page: int, size: int):
        raise NotImplementedError

    @abstractmethod
    def list_vc_providers(self):
        raise NotImplementedError

    @abstractmethod
    def list_vc_locations(self):
        raise NotImplementedError

    @abstractmethod
    def list_compliance_labels(self, level: int):
        raise NotImplementedError

    @abstractmethod
    def list_criterions(self, can_be_self_assessed: bool):
        raise NotImplementedError

    @abstractmethod
    def list_compliance_references(self):
        raise NotImplementedError

    @abstractmethod
    def delete_service_from_did(self, did: str):
        raise NotImplementedError

    @abstractmethod
    def list_address_light(self):
        pass
