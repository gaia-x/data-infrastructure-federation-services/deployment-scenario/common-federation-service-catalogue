# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class CatalogueRepository(metaclass=ABCMeta):
    @abstractmethod
    async def store(self, did: str, document: str) -> None:
        """
        The store function is used to store a document in the database.

        Args:
            self: Represent the instance of the class
            did: str: Identify the document
            document: str: Store the document

        Returns:
            Nothing
        """
        raise NotImplementedError

    @abstractmethod
    async def get_keys(self, key_pattern: str) -> list[str]:
        raise NotImplementedError

    @abstractmethod
    async def get_array(self, key: str):
        raise NotImplementedError

    @abstractmethod
    async def add(self, key, value):
        raise NotImplementedError

    @abstractmethod
    async def remove(self, key, value):
        raise NotImplementedError

    @abstractmethod
    async def delete(self, did: str) -> str:
        """
        The delete function deletes a document from the database.


        Args:
            self: Represent the instance of the class
            did: str: Specify the id of the document to be deleted

        Returns:
            A string
        """
        raise NotImplementedError

    @abstractmethod
    async def mdelete(self, keys: set[str]):
        raise NotImplementedError

    @abstractmethod
    async def get(self, did: str):
        raise NotImplementedError

    @abstractmethod
    async def mget(self, dids: set[str]):
        raise NotImplementedError

    @abstractmethod
    async def mset(self, key_value: dict):
        raise NotImplementedError

    @abstractmethod
    async def copy(self, source: str, destination: str):
        raise NotImplementedError

    @abstractmethod
    async def diff(self, key_source, key_diff) -> set[str]:
        raise NotImplementedError

    @abstractmethod
    async def ping(self) -> bool:
        """
        The ping function is used to check if the server is up and running.
        It returns a boolean value, True if the server responds with a pong message, False otherwise.

        Args:
            self: Represent the instance of the class

        Returns:
            A boolean value of whether the connection is alive or not
        """
        raise NotImplementedError
