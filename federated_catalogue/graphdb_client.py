# -*- coding: utf-8 -*-
import json
import logging
import urllib.parse

import requests
from federated_catalogue.Config import (
    FEDERATION_REPOSITORY,
    LAYERS,
    SERVICE_TYPES,
    Settings,
    get_settings,
)
from rdflib import Graph, URIRef
from rdflib.term import Literal

__KEY_PATTERN = "#KEY"
__VALUE_PATTERN = "#VALUE"
__SPARQL_RESULTS_JSON_TYPE = "application/sparql-results+json"

log = logging.getLogger(__name__)
settings: Settings = get_settings()

VC_CONTEXT = "https://www.w3.org/2018/credentials/v1"
STABLE_CONTEXT = "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials"


def post_json_ld_content(repository, json_ld):
    headers = {"Content-Type": "application/ld+json; charset=utf-8"}

    g = Graph()
    g.parse(data=json.dumps(json_ld).replace(VC_CONTEXT, STABLE_CONTEXT), format="json-ld")
    # g.parse(open("./graphdb/examples/catalog.json"), format="json-ld")

    log.debug(g.serialize(format="json-ld"))

    r = requests.post(
        f"{settings.graphdb_url}/repositories/{repository}/statements",
        headers=headers,
        data=g.serialize(format="json-ld", encoding="utf-8"),
    )
    log.debug(r.text)

    return r.status_code


def get_third_party_compliance_certification_claim(
    compliance_assessment_body_did, located_service_offering_did, compliance_certification_scheme_did
):
    prefix = """
        PREFIX compliance: <https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX vc: <https://www.w3.org/2018/credentials#>
        """

    select = (
        """
        select distinct ?vc
        where {
            ?tpccc rdf:type compliance:ThirdPartyComplianceCertificateClaim .
            ?tpccc compliance:hasComplianceCertificationScheme <"""
        + compliance_certification_scheme_did
        + """> .
            ?tpccc compliance:hasServiceOffering <"""
        + located_service_offering_did
        + """> .
            ?tpccc compliance:hasComplianceAssessmentBody <"""
        + compliance_assessment_body_did
        + """> .
            ?vc vc:credentialSubject ?tpccc .
        }
        limit 100
        """
    )

    query = prefix + select
    log.debug(query)

    url = f"{settings.graphdb_url}/repositories/{FEDERATION_REPOSITORY}?query={urllib.parse.quote_plus(query)}"
    headers = {"Accept": __SPARQL_RESULTS_JSON_TYPE}
    response = requests.request("GET", url, headers=headers, data={})
    results = []

    try:
        if response.status_code != 200:
            raise Exception(f"Unable to call graphdb database because of {response.text}")

        my_result_json = json.loads(response.text)
        log.debug(my_result_json)

        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend({"vc_did": get_value_if_exist(rs, "vc")} for rs in result_services if len(rs) != 0)
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")
        return f"Error to retrieve data because {e}"

    return results


# <RDF4J_URL>/repositories/<ID>/rdf-graphs/<NAME>
def build_init_bindings(filters):
    results = {}

    if len(filters) > 0:
        for key, value in filters.items():
            results[key] = URIRef(value) if key == "provider_did" else Literal(value)
    log.debug(f"buildInitBindings : {results}")

    return results


def build_filters(input_filters):
    filters = ""
    if len(input_filters) > 0:
        filter_tpl = "FILTER(?#KEY = #VALUE) ."

        for key, value in input_filters.items():
            if key == "provider_did":
                filters = filters + filter_tpl.replace(__KEY_PATTERN, "provider").replace(__VALUE_PATTERN, f"<{value}>")
            elif key == "location_did":
                filters = filters + filter_tpl.replace(__KEY_PATTERN, "location").replace(__VALUE_PATTERN, f"<{value}>")
            elif key == "located_service_did":
                filters = filters + filter_tpl.replace(__KEY_PATTERN, "located_service").replace(
                    __VALUE_PATTERN, f"<{value}>"
                )
            elif key == "service_did":
                filters = filters + filter_tpl.replace(__KEY_PATTERN, "service").replace(__VALUE_PATTERN, f"<{value}>")
            elif key == "compliance_ref_did":
                filters = filters + filter_tpl.replace(__KEY_PATTERN, "compliance_reference").replace(
                    __VALUE_PATTERN, f"<{value}>"
                )
            elif key == "compliance_label_level":
                log.debug("Skip filter compliance_label_level")
            else:
                filters = filters + filter_tpl.replace(__KEY_PATTERN, key).replace(__VALUE_PATTERN, f"'{value}'")

    log.debug(f"Filters : {filters}")

    return filters


def search_service_with_filters(input_filters):
    prefix = """
        PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
        PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
        PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>
        PREFIX dct: <http://purl.org/dc/terms/>
        PREFIX dcat: <http://www.w3.org/ns/dcat#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX vc: <https://www.w3.org/2018/credentials#>
        PREFIX compliance: <https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#>
        """

    select = """
        select distinct ?service ?provider ?provider_designation ?provider_service_id ?service_title ?location ?located_service ?country_name ?state ?urban_area ?location_provider_designation ?description (concat("['", (group_concat(distinct ?service_type ; separator="', '")), "']") as ?service_type) (concat("['", (group_concat(distinct ?layer ; separator="', '")), "']") as ?layer) (concat("['", (group_concat(distinct ?keyword ; separator="', '")), "']") as ?keyword) (concat("['", (group_concat(distinct ?located_service_vc ; separator="', '")), "']") as ?located_service_vc) (concat("['", (group_concat(distinct ?compliance_reference_title ; separator="', '")), "']") as ?compliance_reference_title) (concat("['", (group_concat(distinct ?compliance_reference ; separator="', '")), "']") as ?compliance_reference)
        where {
            {
                ?service service:providedBy ?provider .
                ?provider participant:designation ?provider_designation .
                OPTIONAL {?service service:providerServiceId ?provider_service_id} .
                ?service service:serviceTitle ?service_title .
                ?service service:isAvailableOn ?location .
                ?location participant:country ?country_name .
                ?location participant:state ?state .
                ?location participant:urbanArea ?urban_area .
                ?location participant:providerDesignation ?location_provider_designation .
                ?service dct:description ?description .
                ?service service:serviceType ?service_type .
                ?service service:layer ?layer .
                ?service dcat:keyword ?keyword .
                ?location participant:hasLocatedServiceOffering ?located_service .
                ?located_service service:isImplementationOf ?service .
                OPTIONAL {?located_service_vc vc:credentialSubject ?located_service} .
                ?compliance_certificate_claim compliance:hasServiceOffering ?located_service .
                ?compliance_certificate_claim compliance:hasComplianceCertificationScheme ?compliance_certification_scheme .
                ?compliance_certification_scheme compliance:hasComplianceReference ?compliance_reference .
                ?compliance_reference compliance:hasComplianceReferenceTitle ?compliance_reference_title .
            }
            UNION
            {
                ?service service:providedBy ?provider .
                ?provider participant:designation ?provider_designation .
                ?service service:providerServiceId ?provider_service_id .
                ?service service:serviceTitle ?service_title .
                ?service service:isAvailableOn ?location .
                ?location participant:country ?country_name .
                ?location participant:state ?state .
                ?location participant:urbanArea ?urban_area .
                ?location participant:providerDesignation ?location_provider_designation .
                ?service dct:description ?description .
                ?service service:serviceType ?service_type .
                ?service service:layer ?layer .
                ?service dcat:keyword ?keyword .
                ?location participant:hasLocatedServiceOffering ?located_service .
                ?located_service service:isImplementationOf ?service .
                FILTER NOT EXISTS { ?complianceCertificateClaim compliance:hasServiceOffering ?located_service } .
            }
        """

    filters = build_filters(input_filters)

    group_by = """
        }
        group by ?service ?provider ?provider_designation ?provider_service_id ?service_title ?location ?country_name ?state ?urban_area ?location_provider_designation ?description ?located_service
    """

    query = prefix + select + filters + group_by

    url = f"{settings.graphdb_url}/repositories/{FEDERATION_REPOSITORY}?query={urllib.parse.quote_plus(query)}"
    headers = {"Accept": __SPARQL_RESULTS_JSON_TYPE}
    response = requests.request("GET", url, headers=headers, data={})
    results = []

    try:
        if response.status_code != 200:
            raise Exception(f"Unable to call graphdb database because of {response.text}")

        my_result_json = json.loads(response.text)

        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(
                {
                    "provider_did": get_value_if_exist(rs, "provider"),
                    "provider_designation": get_value_if_exist(rs, "provider_designation"),
                    "service_did": get_value_if_exist(rs, "service"),
                    "service_type": get_value_if_exist(rs, "service_type"),
                    "provider_service_id": get_value_if_exist(rs, "provider_service_id"),
                    "service_title": get_value_if_exist(rs, "service_title"),
                    "location_did": get_value_if_exist(rs, "location"),
                    "located_service_did": get_value_if_exist(rs, "located_service"),
                    "country_name": get_value_if_exist(rs, "country_name"),
                    "state": get_value_if_exist(rs, "state"),
                    "urban_area": get_value_if_exist(rs, "urban_area"),
                    "location_provider_designation": get_value_if_exist(rs, "location_provider_designation"),
                    "layer": get_value_if_exist(rs, "layer"),
                    "keyword": get_value_if_exist(rs, "keyword"),
                    "located_service_vc": get_value_if_exist(rs, "located_service_vc"),
                    "description": get_value_if_exist(rs, "description"),
                    "compliance_reference_did": get_value_if_exist(rs, "compliance_reference"),
                    "compliance_reference_title": get_value_if_exist(rs, "compliance_reference_title"),
                }
                for rs in result_services
                if len(rs) != 0
            )
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")
        return f"Error to retrieve data because {e}"

    return results


def call_graphdb_query(query):
    url = f"{settings.graphdb_url}/repositories/{FEDERATION_REPOSITORY}?query={urllib.parse.quote_plus(query)}"
    headers = {"Accept": __SPARQL_RESULTS_JSON_TYPE}
    response = requests.request("GET", url, headers=headers, data={})

    if response.status_code != 200:
        raise Exception(f"Unable to call graphdb database because of {response.text}")

    return json.loads(response.text)


def get_providers():
    query = """
        PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

        select ?provider ?designation
        where {
            ?provider rdf:type participant:Provider .
            ?provider participant:designation ?designation .
        } limit 100
    """

    my_result_json = call_graphdb_query(query)
    results = []

    try:
        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(
                {
                    "provider_did": get_value_if_exist(rs, "provider"),
                    "designation": get_value_if_exist(rs, "designation"),
                }
                for rs in result_services
                if len(rs) != 0
            )
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")

    return results


def get_locations():
    query = """
        PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

        select ?location ?provider ?country_name ?state ?urban_area ?provider_designation
        where {
            ?location rdf:type participant:Location .
            ?location participant:hasProvider ?provider .
            ?location participant:country ?country_name .
            ?location participant:state ?state .
            ?location participant:urbanArea ?urban_area .
            ?location participant:providerDesignation ?provider_designation .
        }
    """

    my_result_json = call_graphdb_query(query)
    results = []

    try:
        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(
                {
                    "location_did": get_value_if_exist(rs, "location"),
                    "provider_did": get_value_if_exist(rs, "provider"),
                    "country_name": get_value_if_exist(rs, "country_name"),
                    "state": get_value_if_exist(rs, "state"),
                    "urban_area": get_value_if_exist(rs, "urban_area"),
                    "provider_designation": get_value_if_exist(rs, "provider_designation"),
                }
                for rs in result_services
                if len(rs) != 0
            )
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")

    return results


def get_catalog():
    query = """
            PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>

            select ?provider (concat("['", (group_concat(?service ; separator="', '")), "']") as ?service)
            where {
                ?service service:providedBy ?provider .
            }
            group by ?provider
    """

    my_result_json = call_graphdb_query(query)

    results = []

    try:
        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(
                {
                    "provider": get_value_if_exist(rs, "provider"),
                    "services": json.loads((get_value_if_exist(rs, "service")).replace("'", '"')),
                }
                for rs in result_services
                if len(rs) != 0
            )
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")

    return results


def get_services():
    query = """
        PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

        select ?service
        where {
            ?service rdf:type service:ServiceOffering .
        }
    """

    my_result_json = call_graphdb_query(query)

    results = []

    try:
        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(get_value_if_exist(rs, "service") for rs in result_services if len(rs) != 0)
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")

    return results


def get_compliance_references():
    query = """
        PREFIX compliance: <https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

        select *
        where {
            ?compliance_ref rdf:type compliance:ComplianceReference .
            OPTIONAL { ?compliance_ref compliance:cRValidFrom ?valid_from_date } .
            ?compliance_ref compliance:hasComplianceReferenceManager ?compliance_reference_manager .
            ?compliance_ref compliance:hasComplianceReferenceTitle ?compliance_reference_title .
            ?compliance_ref compliance:hasReferenceUrl ?compliance_reference_url .
            # ?compliance_ref compliance:hasSha256 ?compliance_reference_sha256 .
            ?compliance_ref compliance:hasVersion ?compliance_reference_version .
            ?compliance_ref compliance:referenceType ?compliance_reference_type .
        }
    """

    my_result_json = call_graphdb_query(query)

    results = []

    try:
        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(
                {
                    "compliance_ref_did": get_value_if_exist(rs, "compliance_ref"),
                    "valid_from_date": get_value_if_exist(rs, "valid_from_date"),
                    "compliance_reference_manager": get_value_if_exist(rs, "compliance_reference_manager"),
                    "title": get_value_if_exist(rs, "compliance_reference_title"),
                    "reference_url": get_value_if_exist(rs, "compliance_reference_url"),
                    # "reference_sha256": get_value_if_exist(rs, 'compliance_reference_sha256'),
                    "version": get_value_if_exist(rs, "compliance_reference_version"),
                    "type": get_value_if_exist(rs, "compliance_reference_type"),
                }
                for rs in result_services
                if len(rs) != 0
            )
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")

    return results


def get_compliance_labels():
    query = """
        PREFIX compliance: <https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX dct: <http://purl.org/dc/terms/>

        select distinct *
        where {
            ?compliance_label rdf:type compliance:ComplianceLabel .
            ?compliance_label compliance:hasName ?name .
            ?compliance_label dct:hasDescription ?description .
            ?compliance_label compliance:hasLevel ?level .
        }
    """

    my_result_json = call_graphdb_query(query)

    results = []

    try:
        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(
                {
                    "compliance_label_did": get_value_if_exist(rs, "compliance_label"),
                    "name": get_value_if_exist(rs, "name"),
                    "description": get_value_if_exist(rs, "description"),
                    "level": get_value_if_exist(rs, "level"),
                    "has_required_criteria_combinations": get_criteria_by_compliance_label(
                        get_value_if_exist(rs, "compliance_label")
                    ),
                }
                for rs in result_services
                if len(rs) != 0
            )
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")

    return results


def get_criterions():
    query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX compliance: <https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#>

        select distinct ?criterion_did
        {
            ?criterion_did rdf:type compliance:ComplianceCriterion
        }
    """

    my_result_json = call_graphdb_query(query)

    results = []

    try:
        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(
                {"criterion_did": get_value_if_exist(rs, "criterion_did")} for rs in result_services if len(rs) != 0
            )
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")

    return results


def get_criteria_by_compliance_label(compliance_label_did):
    query = """
        PREFIX compliance: <https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX dct: <http://purl.org/dc/terms/>

        select distinct *
        where {
            ?criterion rdf:type compliance:ComplianceCriterion .
            ?criterion compliance:hasName ?name .
            ?criterion dct:hasDescription ?description .
            ?criterion compliance:hasLevel ?level .
            ?criterion compliance:hasCategory ?category .
            ?criterion compliance:canBeSelfAssessed ?can_be_self_assessed .
            ?compliance_label compliance:hasRequiredCriteria ?criterion .
            FILTER (?compliance_label = <#COMPLIANCE_LABEL_DID>) .
        }
    """

    query = query.replace("#COMPLIANCE_LABEL_DID", compliance_label_did)

    my_result_json = call_graphdb_query(query)

    results = []

    try:
        if "results" in my_result_json and "bindings" in my_result_json["results"]:
            result_services = my_result_json["results"]["bindings"]

            results.extend(
                {
                    "criterion_did": get_value_if_exist(rs, "criterion"),
                    "name": get_value_if_exist(rs, "name"),
                    "description": get_value_if_exist(rs, "description"),
                    "level": get_value_if_exist(rs, "level"),
                    "category": get_value_if_exist(rs, "category"),
                    "can_be_self_assessed": get_value_if_exist(rs, "can_be_self_assessed"),
                }
                for rs in result_services
                if len(rs) != 0
            )
    except Exception as e:
        log.debug(f"Error to retrieve data because {e}")

    return results


def clean_tabs(value):
    value = value.replace("'',", "")
    value = value.replace("''", "")

    return value


def get_value_if_exist(result, field):
    if field not in result:
        return ""

    value = result[field]["value"]
    return clean_tabs(value) if field in ["keyword", "layer", "service_type"] else value


def get_all_available_filters():
    """
    List different available filters in one single point (better for front app).

    # TODO : this method must be in BFF (Backend For Frontend) because it is its responsibility
    :return:
    """
    return {
        "available_services": SERVICE_TYPES,
        "available_layers": LAYERS,
        "available_providers": get_providers(),
        "available_locations": get_locations(),
    }


def repository_exist(repository_name):
    url = f"{settings.graphdb_url}/rest/repositories/{urllib.parse.quote_plus(repository_name)}"
    headers = {"Accept": "application/json"}

    response = requests.get(url, headers=headers)

    return response.status_code == 200


def create_repository(config_file):
    url = f"{settings.graphdb_url}/rest/repositories/"
    payload = {}
    files = {"config": open(config_file, "rb")}

    headers = {"Accept": "application/json"}

    response = requests.request("POST", url, headers=headers, data=payload, files=files)

    log.debug(response.text)


def launch_init_repository(repository_name, config_file):
    # check if repository exist
    if not repository_exist(repository_name):
        log.debug(f"Launch creation of repository {repository_name}")

        create_repository(config_file)

        log.debug(f"Repository {repository_name} created.")
    else:
        log.debug(f"Repository {repository_name} already exist")
