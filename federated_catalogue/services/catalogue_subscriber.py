# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

from federated_catalogue.domain.interfaces.rule_checker import RuleChecker
from federated_catalogue.services.catalogue import CatalogueService


class CatalogueSubscriber(metaclass=ABCMeta):
    def __init__(self, catalogue_service: CatalogueService, rule_checker_client: RuleChecker = None) -> None:
        self.catalogue_service = catalogue_service
        self.rule_checker_client = rule_checker_client

    @abstractmethod
    async def listen(self):
        raise NotImplementedError

    @abstractmethod
    async def stop(self):
        raise NotImplementedError
