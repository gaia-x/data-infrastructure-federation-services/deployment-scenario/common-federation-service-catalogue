# -*- coding: utf-8 -*-
from __future__ import annotations

import asyncio
import copy
import json
import logging
from typing import Any

from federated_catalogue.Config import (
    CATALOG_PREFIX_KEY,
    CONTEXT_CATALOG,
    LAYERS,
    SERVICE_TYPES,
    TYPE_CATALOG,
    Settings,
    get_settings,
)
from federated_catalogue.domain.catalogue import Catalogue, SynchroStatus
from federated_catalogue.domain.interfaces.graph_repository import GraphRepository
from federated_catalogue.domain.interfaces.repository import CatalogueRepository
from federated_catalogue.domain.query_filter import QueryFilter
from federated_catalogue.utils import (
    HostNotReachable,
    get_did_document_from_did,
    get_id_from_jsonld,
    get_json_from_url,
    is_did_web,
    transform_did_into_url,
)

log = logging.getLogger(__name__)
settings: Settings = get_settings()


def divide_chunks(m_list, n):
    # looping till length l
    for i in range(0, len(m_list), n):
        yield m_list[i : i + n]


class CatalogueService:
    def __init__(self, repository: CatalogueRepository, graph_repository: GraphRepository) -> None:
        """
        The __init__ function is called when the class is instantiated.
        It sets up the object with a repository to use for data access.

        Args:
            self: Represent the instance of the class
            repository: CatalogueRepository: Pass in the repository object
            graph_repository: GraphRepository: Pass in the repository object

        Returns:
            Nothing
        """

        self.repository = repository
        self.graph_repository = graph_repository
        self.query_cached = None

    @staticmethod
    def catalog_to_json(catalogue: Catalogue) -> str:
        """
        The catalog_to_json function takes a Catalogue object and returns a JSON string representation of it.

        Args:
            catalogue: Catalogue: Specify the type of the parameter catalog

        Returns:
            A string that is a json representation of the catalog
        """
        to_dict = catalogue.to_dict() | CONTEXT_CATALOG | TYPE_CATALOG
        return json.dumps(to_dict)

    async def build_catalog(self) -> Catalogue:
        """
        The build_catalog function is used to build a Catalogue object.

        Args:
            self: Access the repository

        Returns:
            A catalogue object
        """
        vcs = []

        provider_catalogues = await self.repository.get_keys(f"{CATALOG_PREFIX_KEY}/*")
        for catalog_url in provider_catalogues:
            vc_ids = await self.repository.get_array(catalog_url)
            vcs.extend(iter(vc_ids))

        return Catalogue(provided_by=settings.did_url, get_verifiable_credentials_ids=vcs)

    async def index_vc_from_did(self, did: str, use_universal_resolver: bool = False) -> Any:
        """
        The index_vc_from_did function is used to index a Verifiable Credential from a DID.

        Args:
            self: Bind the method to an object
            did: str: Pass in the did of the verifiable credential

        Returns:
            The json_doc
            :param did:
            :param use_universal_resolver:
        """
        try:
            log.info(f"START to retrieve data for {did}")

            json_doc = None

            if is_did_web(did) and use_universal_resolver:
                log.info(f"Try universal resolver for {did}")
                json_doc = await get_did_document_from_did(did)

            if json_doc is None:
                json_doc = await get_json_from_url(transform_did_into_url(did), timeout=0.5)

            log.info(f"END to retrieve data for {did}")

            if json_doc is not None:
                log.info(f"START to store inside Redis with {did}")
                await self.repository.store(did, json.dumps(json_doc))
                log.info(f"END to store inside Redis with {did}")
                log.info(f"START to store inside Graphdb with {did}")
                await self.graph_repository.post_json_ld_content(json_doc, did=did)
                log.info(f"END to store inside Graphdb with {did}")

            return json_doc
        except HostNotReachable:
            log.info(f"Unable to reach data with id {did}")
            return None
        except RuntimeError:
            log.error(f"Unable to retrieve data from id or did {did}")
        except Exception as e:
            log.error(f"Unable to index_vc_from_did because of {e}")

    async def index_vc(self, vc: dict) -> Any:
        """
        The index_vc function is used to index a Verifiable Credential.

        Args:
            self: Bind the method to an object
            vc: dict: Pass the verifiable credential

        Returns:
            The json_doc
        """
        if vc is not None:
            await self.repository.store(get_id_from_jsonld(vc), json.dumps(vc))
            await self.graph_repository.post_json_ld_content(vc)

        return vc

    async def delete_vc_from_did(self, did: str) -> Any:
        """
        The delete_vc_from_did function deletes a VC from the DID repository.

        Args:
            self: Bind the method to an object
            did: str: Specify the did of the vc to be deleted

        Returns:
            A json document
        """
        json_doc = await self.repository.delete(did)
        if json_doc is not None:
            self.graph_repository.delete_did_document(json.loads(json_doc))

        return json_doc

    async def launch_synchronisation(self, catalogue: Catalogue):

        """
        The launch_synchronisation function is responsible for synchronising the local repository with a remote catalogue.
        It does so by first copying the current state of the catalogue to a temporary key, then deleting all Verifiable Credentials
        from that catalogue and replacing them with those from the remote one. It then compares this new state to its previous one,
        and deletes any Verifiable Credentials which are no longer present in it.

        Args:
            self: Access the class attributes and methods
            catalogue: Catalogue: Pass the catalogue to the function

        Returns:
            A list of deleted vcs
        """
        if catalogue is None:
            return

        catalogue_provider_key = f"catalogue/{catalogue.provided_by}"

        vc_ids_to_add, vc_ids_to_delete = await self.__compute_catalogue_diff__(catalogue, catalogue_provider_key)
        await self.__add_vc_to_catalogue__(catalogue_provider_key, list(vc_ids_to_add))
        await self.__delete_vc_from_catalogue__(catalogue_provider_key, list(vc_ids_to_delete))

    async def __delete_vc_from_catalogue__(self, catalogue_provider_key, vc_ids_to_delete: list):
        for did in vc_ids_to_delete:
            did_document = await self.repository.get(did)
            if did_document is not None and self.graph_repository.delete_did_document(json.loads(did_document)) == 204:
                await self.repository.delete(did)
                await self.repository.remove(catalogue_provider_key, did)

    async def __add_vc_to_catalogue__(self, catalogue_provider_key, vc_ids_to_add: list):
        chunks = list(divide_chunks(vc_ids_to_add, 25))

        for vc_ids in chunks:
            id_json_map = {}

            for vc_id in vc_ids:
                try:
                    log.info(f"Consume {vc_id}")
                    json_doc = await get_json_from_url(transform_did_into_url(vc_id), timeout=0.5)

                    if json_doc:
                        id_json_map[vc_id] = json_doc
                        await self.repository.add(catalogue_provider_key, vc_id)

                except Exception as e:
                    log.error(f"Unable to get content of {vc_id} because of {e}")

            await self.repository.mset(id_json_map)

            try:
                log.info(f"START to store Catalogue inside Graphdb")
                graph_json_ld = JsonLDCatalogue.build_catalogue_from(list(id_json_map.values()))
                await self.graph_repository.post_json_ld_content(graph_json_ld, did="DID_CATALOGUE")
                log.info(f"END to store Catalogue inside Graphdb")
            except HostNotReachable:
                log.info(f"Unable to reach repository")
            except Exception as e:
                log.error(f"Unable to index_vc_from_did because of {e}")

    async def __compute_catalogue_diff__(self, new_catalogue, catalogue_provider_key):
        old_catalogue_ids = list(await self.repository.get_array(catalogue_provider_key))
        vc_ids_to_add = set(new_catalogue.get_verifiable_credentials_ids) - set(old_catalogue_ids)
        vc_ids_to_delete = set(old_catalogue_ids) - set(new_catalogue.get_verifiable_credentials_ids)

        return vc_ids_to_add, vc_ids_to_delete

    async def launch_add_only_synchronisation(self, catalogue: Catalogue):
        """
        The launch_add_only_synchronisation function is responsible for adding catalogue inside the local repository
        with a remote catalogue.

        Args:
            self: Access the class attributes and methods
            catalogue: Catalogue: Pass the catalogue to the function

        Returns:
            A list of deleted vcs
        """
        log.info("Launching Catalogue Synchronization")

        catalogue_provider_key = f"catalogue/{catalogue.provided_by}"

        vc_ids_to_add, vc_ids_to_delete = await self.__compute_catalogue_diff__(catalogue, catalogue_provider_key)
        await self.__add_vc_to_catalogue__(catalogue_provider_key, list(vc_ids_to_add))

    async def add_catalogue_gaia_x(self, catalogue):

        """
        The add_catalogue_gaia_x function is responsible for add the local repository with a remote catalogue.

        Args:
            self: Access the class attributes and methods
            catalogue: Catalogue: Pass the Gaia-X catalogue to the function
        """
        catalogue_provider_key = f"catalogue_gaia_{catalogue.provided_by}"
        consumed_credentials: int = 0
        error_credentials: int = 0

        log.info(f"Catalogue to index: {catalogue}")
        for vc_id in catalogue.get_verifiable_credentials_ids:
            if await self.index_vc_from_did(vc_id, True) is not None:
                log.info(f"Add vc_id to Catalogue: {catalogue_provider_key} -> {vc_id}")
                await self.repository.add(catalogue_provider_key, vc_id)
                consumed_credentials = consumed_credentials + 1
            else:
                await self.repository.add("catalogue_gaia_dead_letter_queue", vc_id)
                error_credentials = error_credentials + 1

        return consumed_credentials, error_credentials

    async def query_services_with_total(self, filters: QueryFilter, page: int, size: int):
        graph_result = self.graph_repository.count_list_vc_service_offering(filters)
        total = graph_result[0]["total"]
        results = await self.query_services(filters, page, size)
        return {"total": total, "results": results}

    async def query_services(self, filters: QueryFilter, page: int, size: int):
        results = []
        not_found_service_vc = []
        not_found_provider_vc = []
        not_found_located_service_vc = []
        not_found_location_vc = []
        not_found_compliance_vc = []
        not_found_granted_label = []
        not_found_compliance_ref = []
        not_found_aster_compliance = []

        services_provider_did = self.graph_repository.list_vc_service_offering(
            input_filters=filters, page=page, size=size
        )

        did_vc_map = await self.__build_cache_map(services_provider_did)

        for service_provider in services_provider_did:
            service_vc = self.__retrieve_vc_with_cache(
                did_vc_map, service_provider["service_vc_did"], not_found_service_vc
            )
            provider_vc = self.__retrieve_vc_with_cache(
                did_vc_map, service_provider["provider_vc_did"], not_found_provider_vc
            )

            if service_vc and provider_vc:
                vcs_for_provider = [json.loads(service_vc), json.loads(provider_vc)]

                # LOCATED_SERVICE
                located_service_vc = self.__retrieve_vc_with_cache(
                    did_vc_map, service_provider["located_service_vc_did"], not_found_located_service_vc
                )
                if located_service_vc:
                    vcs_for_provider.append(json.loads(located_service_vc))

                # LOCATION
                location_vc = self.__retrieve_vc_with_cache(
                    did_vc_map, service_provider["location_vc_did"], not_found_location_vc
                )
                if location_vc:
                    vcs_for_provider.append(json.loads(location_vc))

                # COMPLIANCE
                compliance_vc = self.__retrieve_vc_with_cache(
                    did_vc_map, service_provider["compliance_vc_did"], not_found_compliance_vc
                )
                if compliance_vc:
                    vcs_for_provider.append(json.loads(compliance_vc))

                # GRANTED LABEL
                granted_label = self.__retrieve_vc_with_cache(
                    did_vc_map, service_provider["granted_label_vc_did"], not_found_granted_label
                )
                if granted_label:
                    vcs_for_provider.append(json.loads(granted_label))

                # ASTER-X COMPLIANCE
                aster_compliance = self.__retrieve_vc_with_cache(
                    did_vc_map, service_provider["aster_compliance_id"], not_found_aster_compliance
                )
                if aster_compliance:
                    vcs_for_provider.append(json.loads(aster_compliance))

                # COMPLIANCE REFERENCE
                compliance_ref_dids = service_provider["compliance_references"].split(",")

                for did in compliance_ref_dids:
                    did = str(did).removesuffix("'").removeprefix("'")
                    compliance_ref_vc = self.__retrieve_vc_with_cache(did_vc_map, did, not_found_compliance_ref)
                    if compliance_ref_vc:
                        vcs_for_provider.append(json.loads(compliance_ref_vc))

                results.append(vcs_for_provider)

        log.info(f"Unable to find services in REDIS following dids: {not_found_service_vc}")
        log.info(f"Unable to find providers in REDIS following dids: {not_found_provider_vc}")
        log.info(f"Unable to find located_services in REDIS following dids: {not_found_located_service_vc}")
        log.info(f"Unable to find locations in REDIS following dids: {not_found_location_vc}")
        log.info(f"Unable to find compliance in REDIS following dids: {not_found_compliance_vc}")
        log.info(f"Unable to find granted_label in REDIS following dids: {not_found_granted_label}")
        log.info(f"Unable to find compliance_ref in REDIS following dids: {not_found_compliance_ref}")

        return results

    async def __build_cache_map(self, services_provider_did):
        all_dids = self.__extract_all_dids(services_provider_did)

        return await self.__build_cache_map_from_keys(all_dids)

    async def __build_cache_map_from_keys(self, keys):
        all_vcs = await self.repository.mget(keys)

        cache_map = dict()
        index = 0

        for did in keys:
            cache_map[did] = all_vcs[index]
            index = index + 1

        return cache_map

    @staticmethod
    def __extract_all_dids(services_provider_did):
        all_dids: set = set()

        for service_provider in services_provider_did:
            all_dids.add(service_provider["service_vc_did"])
            all_dids.add(service_provider["provider_vc_did"])
            all_dids.add(service_provider["located_service_vc_did"])
            all_dids.add(service_provider["location_vc_did"])
            all_dids.add(service_provider["compliance_vc_did"])
            all_dids.add(service_provider["granted_label_vc_did"])
            all_dids.add(service_provider["aster_compliance_id"])
            for did in service_provider["compliance_references"].split(","):
                did = str(did).removesuffix("'").removeprefix("'")
                all_dids.add(did)

        return all_dids

    async def list_available_filters(self):
        """
        List different available filters in one single point (better for front app).

        :return:
        """
        providers, addresses, compliance_references, aster_labels = await asyncio.gather(
            self.list_providers(),
            self.list_address_light(),
            self.list_compliance_references(),
            self.list_compliance_labels(),
        )

        return {
            "available_services": SERVICE_TYPES,
            "available_layers": LAYERS,
            "available_providers": providers,
            "available_addresses": addresses,
            "available_compliance_references": compliance_references,
            "available_aster_labels": aster_labels,
        }

    async def list_providers(self):
        results = []
        provider_dids = self.graph_repository.list_vc_providers()

        all_dids = set()
        for provider_did in provider_dids:
            all_dids.add(provider_did["provider_vc_did"])

        all_vcs = set(await self.repository.mget(all_dids))
        all_vcs.add(None)
        all_vcs.remove(None)

        for vc in all_vcs:
            results.append(json.loads(vc))

        return results

    async def list_locations(self):
        results = []
        location_dids = self.graph_repository.list_vc_locations()

        all_dids = set()
        for location_did in location_dids:
            all_dids.add(location_did["location_vc_did"])

        all_vcs = set(await self.repository.mget(all_dids))
        all_vcs.add(None)
        all_vcs.remove(None)

        for vc in all_vcs:
            results.append(json.loads(vc))

        return results

    async def list_compliance_labels(self, level: int = None):
        results = []
        compliance_label_dids = self.graph_repository.list_compliance_labels(level)

        all_dids = set()
        for compliance_label_did in compliance_label_dids:
            all_dids.add(compliance_label_did["compliance_label_did"])

        all_vcs = set(await self.repository.mget(all_dids))
        all_vcs.add(None)
        all_vcs.remove(None)

        for vc in all_vcs:
            results.append(json.loads(vc))

        return results

    async def list_criterions(self, can_be_self_assessed: bool):
        results = []
        compliance_criterion_dids = self.graph_repository.list_criterions(can_be_self_assessed)

        all_dids = set()
        for compliance_criterion_did in compliance_criterion_dids:
            all_dids.add(compliance_criterion_did["compliance_criterion_did"])

        all_vcs = set(await self.repository.mget(all_dids))
        all_vcs.add(None)
        all_vcs.remove(None)

        for vc in all_vcs:
            results.append(json.loads(vc))

        return results

    async def list_compliance_references(self):
        results = []
        compliance_reference_dids = self.graph_repository.list_compliance_references()

        all_dids = set()
        for compliance_reference_did in compliance_reference_dids:
            all_dids.add(compliance_reference_did["compliance_reference_did"])

        all_vcs = set(await self.repository.mget(all_dids))
        all_vcs.add(None)
        all_vcs.remove(None)

        for vc in all_vcs:
            results.append(json.loads(vc))

        return results

    async def list_address_light(self):
        return self.graph_repository.list_address_light()

    async def add_service_offering_vc(self, service_offering_vc: dict):
        service_offering = service_offering_vc["credentialSubject"]
        if service_offering["type"] == "gx:ServiceOffering":
            if "id" in service_offering["gx:providedBy"]:
                provider = service_offering["gx:providedBy"]["id"]
            else:
                provider = service_offering["gx:providedBy"]

            if await self.index_vc(service_offering_vc) is not None:
                await self.repository.add(provider, get_id_from_jsonld(service_offering_vc))
        else:
            log.error(f"The VC is not a VC of ServiceOffering")

    @staticmethod
    def __retrieve_vc_with_cache(cache_map: dict, key: str, not_found_vc: list):
        if key is None:
            return None

        if key not in cache_map:
            not_found_vc.append(key)
            return None
        else:
            return cache_map[key]

    def clean_service_with_dids(self, dids: list):
        deleted = 0
        not_deleted = 0

        for did in dids:
            if self.graph_repository.delete_service_from_did(did) == 204:
                deleted = deleted + 1
            else:
                not_deleted = not_deleted + 1

        return {"deleted": deleted, "not_deleted": not_deleted}

    async def delete_provider_with_dids(self, dids: list):
        for did in dids:
            print(did)
            await self.delete_provider_with_did(did)

        return "OK"

    async def delete_provider_with_did(self, did: str):
        catalogue_key = f"catalogue/{did}"

        # Clean inside Redis
        all_key_catalogue = await self.repository.get_array(catalogue_key)
        await self.repository.mdelete(all_key_catalogue)
        await self.repository.delete(catalogue_key)

        # Clean inside GraphDB
        self.graph_repository.delete_provider(did)

        return "OK"

    async def register_provider_catalogue(self, provider_catalogue_url):
        await self.repository.add("REGISTERED_CATALOGUE", provider_catalogue_url)

    async def get_registered_provider_catalogue(self):
        return await self.repository.get_array("REGISTERED_CATALOGUE")

    async def get_http_synchro_status(self) -> SynchroStatus:
        status = None
        status_json = await self.repository.get("HTTP_SYNCHRO_STATUS")

        if status_json:
            status = SynchroStatus.from_json(status_json)

        return status

    async def store_http_synchro_status(self, http_synchro_status):
        await self.repository.store("HTTP_SYNCHRO_STATUS", http_synchro_status.to_json())

    async def unregister_provider_catalogue(self, provider_catalogue_url):
        catalogue_json = await get_json_from_url(provider_catalogue_url, timeout=0.5)
        catalogue = Catalogue.from_json(json.dumps(catalogue_json))

        await self.delete_provider_with_did(catalogue.provided_by)
        await self.repository.remove("REGISTERED_CATALOGUE", provider_catalogue_url)

    async def launch_http_synchronisation(self):
        start_status = SynchroStatus.create_start_status()
        await self.store_http_synchro_status(start_status)

        catalogue_urls: list = await self.get_registered_provider_catalogue()

        for catalogue_url in catalogue_urls:
            log.info(f"Retrieve catalogue from {catalogue_url}")
            catalogue_json = await get_json_from_url(catalogue_url, timeout=0.5)

            log.debug("Convert json to Catalogue object")
            catalogue = Catalogue.from_json(json.dumps(catalogue_json))

            await self.launch_synchronisation(catalogue)

        end_status = SynchroStatus.create_end_status(start_status)
        await self.store_http_synchro_status(end_status)


class JsonLDCatalogue:
    CATALOG_TPL = {
        "@context": [
            "https://www.w3.org/2018/credentials/v1",
            "https://w3id.org/security/suites/jws-2020/v1",
            "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",
            "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity",
            "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data",
        ],
        "@graph": [],
    }

    def __init__(self) -> None:
        self.catalogue = copy.deepcopy(self.CATALOG_TPL)

    def add_jsonld_in_catalogue(self, json_ld):
        self.catalogue["@graph"].append(json_ld)

    def get_catalogue(self):
        return copy.deepcopy(self.catalogue)

    def add_elements(self, elements: list):
        self.catalogue["@graph"].extend(elements)

    @classmethod
    def build_catalogue_from(cls, elements: list):
        json_catalogue = JsonLDCatalogue()
        json_catalogue.add_elements(elements)
        return json_catalogue.get_catalogue()
