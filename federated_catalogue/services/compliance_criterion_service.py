# -*- coding: utf-8 -*-
import json
import logging

log = logging.getLogger(__name__)


class ComplianceCriterionService:

    # TODO : remove mock
    def __init__(self, criterions_mock_path) -> None:
        with open(criterions_mock_path, "r") as file:
            self.criterions_list = json.load(file)

    def list_criterions(self, can_be_self_assessed_filter=None) -> list:
        criterions = self.criterions_list

        if can_be_self_assessed_filter is not None:
            try:
                criterions = list(
                    filter(
                        lambda criterion: criterion["gax-compliance:canBeSelfAssessed"]["@value"]
                        is can_be_self_assessed_filter,
                        criterions,
                    )
                )
            except KeyError:
                log.debug("Unable to find gax-compliance:canBeSelfAssessed @value into json criterion")

        return criterions

        # criterion_dids = graphdb_client.get_criterions()
        #
        # criterions = []
        # for criterion_did in criterion_dids:
        #     criterion_url = utils.transform_did_into_url(criterion_did['criterion_did'])
        #     criterion = utils.get_json_from_url(criterion_url)
        #     if criterion is not None:
        #         criterions.append(criterion)
        #
        # return criterions
