import logging

from federated_catalogue.domain.catalogue import Catalogue, SynchroStatus
from federated_catalogue.services.catalogue import CatalogueService, settings
from federated_catalogue.utils import get_id_from_jsonld, get_json_from_url
from redis.asyncio import Redis

log = logging.getLogger(__name__)


class CesEventConsumer:
    CES_LAST_RECEIVED_ID_KEY = "LAB_CES_LAST_RECEIVED_ID"
    CES_SYNCHRO_STATUS_KEY = "CES_SYNCHRO_STATUS"

    def __init__(self, catalogue_service: CatalogueService, redis_client: Redis) -> None:
        super().__init__()
        self.catalogue_service = catalogue_service
        self.redis_client = redis_client

    async def get_ces_synchro_status(self) -> SynchroStatus:
        status = None
        status_json = await self.redis_client.get(self.CES_SYNCHRO_STATUS_KEY)

        if status_json:
            status = SynchroStatus.from_json(status_json)

        return status

    async def store_ces_synchro_status(self, ces_synchro_status):
        await self.redis_client.set(self.CES_SYNCHRO_STATUS_KEY, ces_synchro_status.to_json())

    async def consume_ces_events(self, ces_events):
        # TODO /!\ : comment garder l'information sur la compliance associée au VC ?????
        log.info(f"START consume_ces_events")
        read_events: int = 0
        consumed_events: int = 0
        error_events: int = 0
        consumed_credentials: int = 0
        error_credentials: int = 0

        for ces_event in ces_events:
            try:
                read_events = read_events + 1
                # extract VC de compliance du lab
                compliance_vc = ces_event["data"]
                provider = "UNKNOWN PROVIDER"
                vc_ids = [get_id_from_jsonld(compliance_vc)]
                # TODO : check VC validity
                for credential_subject in compliance_vc["credentialSubject"]:
                    # TODO : check integrity of credential according to ces event data
                    logging.info(f"Consume credential of type {credential_subject['gx:type']}")

                    if credential_subject["gx:type"] == "gx:LegalParticipant":
                        provider = credential_subject["id"]

                    vc_ids.append(credential_subject["id"])

                catalogue = Catalogue(provided_by=provider, get_verifiable_credentials_ids=vc_ids)
                log.info(f"START consume_ces_events -> ready to call add_catalogue_gaia_x for event {ces_event}")
                tmp_consumed_credentials, tmp_error_credentials = await self.catalogue_service.add_catalogue_gaia_x(
                    catalogue
                )
                consumed_credentials = consumed_credentials + tmp_consumed_credentials
                error_credentials = error_credentials + tmp_error_credentials
                consumed_events = consumed_events + 1
            except Exception as exc:
                logging.error(f"Unable to consume event {ces_event} cause by {exc}")
                error_events = error_events + 1

        return read_events, consumed_events, error_events, consumed_credentials, error_credentials

    async def consume_ces(self):
        logging.info("Start to consume CES")
        start_status = SynchroStatus.create_start_status()
        await self.store_ces_synchro_status(start_status)

        page = 0
        size = 20
        base_credentials_events_url = f"{settings.ces_url}/credentials-events?size={size}"
        last_received_id = await self.retrieve_last_received_id_from_ces()
        final_read_events: int = 0
        final_consumed_events: int = 0
        final_error_events: int = 0
        final_consumed_credentials: int = 0
        final_error_credentials: int = 0

        if last_received_id is not None:
            base_credentials_events_url = f"{base_credentials_events_url}&lastReceivedID={last_received_id}"

        ces_url = f"{base_credentials_events_url}&page={page}"
        ces_event = await get_json_from_url(ces_url)

        while len(ces_event) > 0:
            await self.store_last_received_id_from_ces(ces_event[len(ces_event) - 1]["id"])
            logging.debug(f"CES event received: {ces_event}")
            (
                read_events,
                consumed_events,
                error_events,
                consumed_credentials,
                error_credentials,
            ) = await self.consume_ces_events(ces_event)
            final_read_events = final_read_events + read_events
            final_consumed_events = final_consumed_events + consumed_events
            final_error_events = final_error_events + error_events
            final_consumed_credentials = final_consumed_credentials + consumed_credentials
            final_error_credentials = final_error_credentials + error_credentials

            page = page + 1
            ces_url = f"{base_credentials_events_url}&page={page}"
            ces_event = await get_json_from_url(ces_url, timeout=0.5)

        end_status = SynchroStatus.create_end_status(start_status)
        await self.store_ces_synchro_status(end_status)

        logging.info("Finish to consume CES")
        logging.info(f"read_events: {final_read_events}")
        logging.info(f"consumed_events: {final_consumed_events}")
        logging.info(f"error_events: {final_error_events}")
        logging.info(f"consumed_credentials: {final_consumed_credentials}")
        logging.info(f"error_credentials: {final_error_credentials}")

    async def retrieve_last_received_id_from_ces(self):
        return await self.redis_client.get(self.CES_LAST_RECEIVED_ID_KEY)

    async def store_last_received_id_from_ces(self, last_received_id):
        return await self.redis_client.set(self.CES_LAST_RECEIVED_ID_KEY, last_received_id)
