# -*- coding: utf-8 -*-
import json
import logging
from typing import Any

from federated_catalogue.Config import CATALOG_PREFIX_KEY, Settings, get_settings
from federated_catalogue.domain.catalogue import Catalogue
from federated_catalogue.domain.interfaces.rule_checker import RuleChecker
from federated_catalogue.infra.api.message_queue.redis import Subscriber
from federated_catalogue.services.catalogue import CatalogueService
from federated_catalogue.services.catalogue_subscriber import CatalogueSubscriber
from federated_catalogue.utils import get_type_from_jsonld

log = logging.getLogger(__name__)
settings: Settings = get_settings()


def verifiable_credentials_to_catalogue(verifiable_credential: dict[str, Any]) -> Catalogue:
    subject = verifiable_credential["credentialSubject"]
    vc_type = get_type_from_jsonld(jsonld=subject)
    if "Catalogue" in vc_type:
        provided_by = subject["gx:providedBy"]
        verifiable_credentials_ids = (
            subject["gx:getVerifiableCredentialsIDs"] if "gx:getVerifiableCredentialsIDs" in subject else []
        )

        return Catalogue(provided_by=provided_by, get_verifiable_credentials_ids=verifiable_credentials_ids)


class RedisCatalogueSubscriber(CatalogueSubscriber):
    def __init__(self, catalogue_service: CatalogueService, rule_checker_client: RuleChecker = None):
        super().__init__(catalogue_service, rule_checker_client)

        self.subscriber = Subscriber(
            channel_name_prefix=CATALOG_PREFIX_KEY,
            url=settings.redis_pub_sub_host,
            message_handler=self.process_update_catalog_event,
        )

    async def listen(self):
        await self.subscriber.start()

    async def stop(self):
        await self.subscriber.stop()

    async def process_update_catalog_event(self, message: str):
        log.debug(f"receiving message: {message}")
        if message is not None:
            json_message = json.loads(message)
            vp_type = get_type_from_jsonld(jsonld=json_message)
            if vp_type is not None and "VerifiablePresentation" in vp_type:
                log.debug("receiving vp")
                # if await self.check_verifiable_presentation(verifiable_presentation=json_message):
                #     log.debug("vp is ok. Process it")
                #     await self.process_verifiable_presentation(verifiable_presentation=json_message)

                log.debug("vp is ok. Process it")
                await self.process_verifiable_presentation(verifiable_presentation=json_message)
            else:
                log.debug("message not handled")

    async def check_verifiable_presentation(self, verifiable_presentation: dict[str, Any]) -> bool:
        if self.rule_checker_client is not None:
            return await self.rule_checker_client.check_verifiable_presentation(
                verifiable_presentation=verifiable_presentation
            )
        return True

    async def process_verifiable_presentation(self, verifiable_presentation: dict[str, Any]) -> None:
        if "verifiableCredential" in verifiable_presentation:
            log.debug("VC found in VP")
            verifiable_credentials = verifiable_presentation["verifiableCredential"]
            for verifiable_credential in verifiable_credentials:
                vc_type = get_type_from_jsonld(jsonld=verifiable_credential)
                log.debug(f"VC type: {vc_type}")

                if "credentialSubject" in verifiable_credential and "Catalogue" in get_type_from_jsonld(
                    jsonld=verifiable_credential["credentialSubject"]
                ):
                    catalogue = verifiable_credentials_to_catalogue(verifiable_credential)
                    log.debug("Launch synchronisation")
                    await self.catalogue_service.launch_synchronisation(catalogue)
