# -*- coding: utf-8 -*-
import asyncio
from typing import Dict

from federated_catalogue.domain.interfaces.repository import CatalogueRepository


class HealthcheckService:
    def __init__(self, repository: CatalogueRepository) -> None:
        """
        The __init__ function is called when the class is instantiated.
        It sets up the object with a repository to use for data access.

        Args:
            self: Represent the instance of the class
            repository: CatalogueRepository: Pass in the repository object

        Returns:
            Nothing
        """
        self.repository = repository

    async def ping(self) -> Dict[str, bool]:
        """
        The ping function is used to check if the database is up and running.
            :return: True if the database responds, False otherwise.


        Args:
            self: Represent the instance of a class

        Returns:
            A boolean value
        """
        statuses = await asyncio.gather(*[self.repository.ping()])

        return dict(zip(["catalogue repository (Redis)"], statuses, strict=True))
