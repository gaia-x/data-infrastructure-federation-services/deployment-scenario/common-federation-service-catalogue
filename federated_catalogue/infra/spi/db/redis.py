# -*- coding: utf-8 -*-
import json

from federated_catalogue.domain.interfaces.repository import CatalogueRepository
from redis.asyncio.client import Redis
from redis.exceptions import BusyLoadingError
from redis.exceptions import ConnectionError as RedisConnectionError


class RedisCatalogueRepository(CatalogueRepository):
    def __init__(self, redis_client: Redis) -> None:
        """
        The __init__ function is called when the class is instantiated.
        It sets up the connection to Redis and assigns it to self.client.

        Args:
            self: Represent the instance of the object itself
            redis_client: Redis: Client with a redis database

        Returns:
            Nothing

        Doc Author:
            Trelent
        """
        self.client = redis_client

    async def get_array(self, key: str):
        """
        The get_set function returns a list returns all the members of the set value stored at key.

        Args:
            self: Refer to the instance of the class
            key: str: Specify the key to search

        Returns:
            A list of all values matching the key

        """
        return await self.client.smembers(key)

    async def store(self, did: str, document: str):
        """
        The store function takes a document id and the document itself as input.
        It then stores the document in Redis using the given id as key.

        Args:
            self: Represent the instance of the class
            did: str: Store the document id
            document: str: Store the document in the database

        Returns:
            The document that was stored
        """
        await self.client.set(name=did, value=document)

    async def sadd(self, key: str, values: set):
        """

        :param key:
        :param values:
        :return:
        """
        await self.client.sadd(key, *values)

    async def add(self, key, value):
        """
        The store function takes a document id and the document itself as input.
        It then stores the document in Redis using the given id as key.

        Args:
            self: Represent the instance of the class
            key: str: the key to store
            value: str: add the value in the database

        Returns:
            None
        """
        await self.client.sadd(key, value)

    async def remove(self, key, value):
        """
        Remove the specified value from the set stored at key. Specified value
        that are not a member of this set are ignored. If key does not exist,
        it is treated as an empty set and this command returns 0.

        Args:
            self: Represent the instance of the class
            key: str: the key
            value: str: remove the value in the database

        Returns:
            None
        """
        await self.client.srem(key, value)

    async def delete(self, did: str) -> None | str:
        """
        The delete function deletes a document from the database.


        Args:
            self: Represent the instance of the class
            did: str: Specify the key of the value to be deleted

        Returns:
            The value of the key that was deleted
        """

        current_type = await self.client.type(name=did)
        if current_type is None:
            return None

        if current_type == "str":
            return await self.client.getdel(name=did)
        await self.client.delete(did)
        return None

    async def get(self, did: str) -> str:
        """
        The get function get a document from the database.


        Args:
            self: Represent the instance of the class
            did: str: Specify the key of the value to get

        Returns:
            The value of the key
        """
        return await self.client.get(name=did)

    async def mget(self, dids: set[str]):
        pipeline = self.client.pipeline()

        for did in dids:
            await pipeline.get(did)

        return await pipeline.execute()

        # I'm not using mget of REDIS because this method failed test cause to coroutine error
        # return await self.client.mget(keys=dids)

    async def mdelete(self, keys: set[str]):
        pipeline = self.client.pipeline()

        for key in keys:
            await pipeline.delete(key)

        return await pipeline.execute()

    async def mset(self, key_value: dict):
        pipeline = self.client.pipeline()

        for key in key_value:
            await pipeline.set(key, json.dumps(key_value[key]))

        return await pipeline.execute()

    async def copy(self, source: str, destination: str):
        """
        Copy the value stored in the ``source`` key to the ``destination`` key.

        Args:
            self: Represent the instance of the class
            source: str: Specify the key of the source
            destination: str: Specify the key of the destination
        """

        await self.client.copy(source, destination)

    async def diff(self, key_source, key_diff) -> set[str]:
        """
        Return the difference of sets specified by ``keys``
        """
        return await self.client.sdiff(key_source, key_diff)

    async def ping(self) -> bool:
        """
        The ping function is used to test the connection between the client and server.
        It returns a boolean value of True if it succeeds, or False if it fails.

        Args:
            self: Represent the instance of the class

        Returns:
            A boolean value indicating whether the connection is still alive
        """
        try:
            await self.client.ping()
        except (RedisConnectionError, BusyLoadingError):
            return False
        return True

    async def get_keys(self, key_pattern: str) -> list[str]:
        """
        The get_keys function returns a list of keys that match the key_pattern.

        Args:
            self: Represent the instance of the class
            key_pattern: str: Specify the pattern of keys to be returned

        Returns:
            A list of keys that match the key_pattern
        """
        return await self.client.keys(pattern=key_pattern)
