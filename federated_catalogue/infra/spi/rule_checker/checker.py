import logging
from typing import Any

import httpx
from federated_catalogue.domain.interfaces.rule_checker import RuleChecker

log = logging.getLogger(__name__)


def verifiable_presentation_to_checker_request(verifiable_presentation: dict[str, Any]) -> dict[str, Any]:
    return {
        "input": {
            "vp": verifiable_presentation,
            "rules": [
                "rule_01",
                "rule_02",
                "rule_03",
                "rule_04",
                "rule_05",
                "rule_06",
                "rule_07",
            ],
        }
    }


class NoVerifiableCredentialsInVerifiablePresentationError(ValueError):
    pass


class ABCRuleCheckerClient(RuleChecker):
    def __init__(self, checker_host: str) -> None:
        self.check_rules_url = f"{checker_host}/api/rules/check"

    async def check_verifiable_presentation(self, verifiable_presentation: dict[str, Any]) -> bool:
        """
        The check_verifiable_presentation function checks the verifiable presentation for validity.

        Args:
            self: Represent the instance of the class
            verifiable_presentation: dict[str, Any]: a verifiable presentation to check

        Returns:
            A boolean value
        """

        headers = {"Content-Type": "application/json"}
        request_data = verifiable_presentation_to_checker_request(verifiable_presentation)

        log.debug(f"Calling {self.check_rules_url} with these parameters: {request_data}")

        async with httpx.AsyncClient() as client:
            response = await client.post(
                self.check_rules_url,
                headers=headers,
                json=request_data,
            )

            log.debug(f"Response: {response.status_code} - {response.content}")

            match response.status_code:
                case 200:
                    return True
                case 403:
                    return False
                case 422:
                    raise NoVerifiableCredentialsInVerifiablePresentationError()
                case _:
                    return False
