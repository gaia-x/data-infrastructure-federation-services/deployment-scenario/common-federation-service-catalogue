# -*- coding: utf-8 -*-
from typing import Final

DELETE_VC_QUERY: Final = """
PREFIX cred: <https://www.w3.org/2018/credentials#>

# VC
DELETE {?s ?p ?o}
WHERE {
    {
        <#VC_ID#> cred:credentialSubject ?s .
        ?s ?p ?o
    }
    UNION
    {
        ?s ?p ?o
        FILTER(?s = <#VC_ID#>)
    }
}
"""

DELETE_CREDENTIAL_QUERY: Final = """
DELETE {?s ?p ?o}
WHERE {
    ?s ?p ?o
    FILTER(?s = <#CRED_ID#>)
}
"""

DELETE_SERVICE_QUERY: Final = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/vocab/service#>

DELETE {?service ?p ?o}
WHERE {
    {
        SELECT ?service (COUNT(?service) as ?c)
        where {
            ?service rdf:type service:#SERVICE_TYPE# .
            FILTER (?service = <#SERVICE_ID#>)
        }
        GROUP BY ?service
    }
    FILTER (?c = 1)
    ?service ?p ?o
}
"""

ASTER_DELETE_PROVIDER_QUERY: Final = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX cred: <https://www.w3.org/2018/credentials#>
PREFIX gx: <https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#>
PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>
PREFIX aster-data: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data#>

delete { ?s ?p ?o
}
where {
    {
        ?s ?p ?o .
        FILTER(?s = <#PROVIDER_ID#>) .
    }
    UNION
    {
        ?s ?p ?o .
        ?s rdf:type gx:ServiceOffering .
        ?s gx:providedBy <#PROVIDER_ID#>
    }
}
"""


ASTER_DELETE_SERVICE_QUERY: Final = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX cred: <https://www.w3.org/2018/credentials#>
PREFIX gx: <https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#>
PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>
PREFIX aster-data: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data#>

DELETE {?service ?p ?o}
WHERE {
    {
        SELECT ?service (COUNT(?service) as ?c)
        where {
            ?service rdf:type gx:#SERVICE_TYPE# .
            FILTER (?service = <#SERVICE_ID#>)
        }
        GROUP BY ?service
    }
    FILTER (?c = 1)
    ?service ?p ?o
}
"""

DELETE_LOCATION_QUERY = """
PREFIX participant: <https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

delete { ?location ?p ?o }
where {
    {
        select ?location (count(?location) as ?c)
        where {
            ?location rdf:type participant:Location .
            filter (?location = <#LOCATION_ID#>)
        }
        group by ?location
    }
    filter (?c = 1)
    ?location ?p ?o
}
"""
