# -*- coding: utf-8 -*-
import json
import logging
import urllib.parse
from typing import Any

import httpx
import requests
from federated_catalogue.Config import FEDERATION_REPOSITORY, Settings, get_settings
from federated_catalogue.domain.interfaces.graph_repository import GraphRepository
from federated_catalogue.domain.query_filter import QueryFilter
from federated_catalogue.infra.spi.graph_repository.constants import (
    ASTER_DELETE_PROVIDER_QUERY,
    ASTER_DELETE_SERVICE_QUERY,
    DELETE_CREDENTIAL_QUERY,
    DELETE_LOCATION_QUERY,
    DELETE_SERVICE_QUERY,
    DELETE_VC_QUERY,
)
from federated_catalogue.utils import get_id_from_jsonld, get_type_from_jsonld
from rdflib import Graph

log = logging.getLogger(__name__)
settings: Settings = get_settings()

VC_CONTEXT = "https://www.w3.org/2018/credentials/v1"
STABLE_CONTEXT = "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials"


def get_value_if_exist(result, field):
    return result[field]["value"] if field in result else ""


def build_pagination(page: int, size: int):
    if size is not None and page is not None:
        limit = size
        offset = page * limit
        return f"limit {limit} offset {offset}"

    return None


def __is_a_verifiable_credential__(document_type):
    if isinstance(document_type, list) and "VerifiableCredential" in document_type:
        return True
    elif isinstance(document_type, list) is False and "VerifiableCredential" == document_type:
        return True
    else:
        return False


class GraphDBRepository(GraphRepository):
    def __init__(self) -> None:
        self.graphdb_url = settings.graphdb_url
        self.repository = FEDERATION_REPOSITORY

    async def post_json_ld_content(self, json_ld, did="default_did"):
        log.info(f"START post_json_ld_content for {did}")
        headers = {"Content-Type": "application/ld+json; charset=utf-8"}

        g = Graph()
        g.parse(data=json.dumps(json_ld).replace(VC_CONTEXT, STABLE_CONTEXT), format="json-ld")

        log.debug(f"{json.dumps(g.serialize(format='json-ld'))}")

        client = httpx.AsyncClient()
        try:
            log.info(f"START call REST GRAPHDB for {did}")
            response = await client.post(
                url=f"{self.graphdb_url}/repositories/{self.repository}/statements",
                headers=headers,
                content=g.serialize(format="json-ld", encoding="utf-8"),
            )
            log.info(f"END call REST GRAPHDB for {did}")

            return response.status_code
        except Exception as err:
            logging.error("Unable to post data into graph.")
        finally:
            await client.aclose()
            log.info(f"END post_json_ld_content for {did}")

    def __call_graphdb_get_query(self, query):
        url = f"{self.graphdb_url}/repositories/{self.repository}?query={urllib.parse.quote_plus(query)}"
        headers = {"Accept": "application/sparql-results+json"}
        response = requests.request("GET", url, headers=headers, data={})

        if response.status_code != 200:
            raise Exception(f"Unable to call graphdb database because of {response.text}")

        return json.loads(response.text)

    def __call_graphdb_post_query(self, query):
        headers = {
            "Content-Type": "application/sparql-update",
            "Accept": "application/json",
        }
        url = f"{self.graphdb_url}/repositories/{self.repository}/statements"

        r = requests.post(url, headers=headers, data=query)

        return r.status_code

    def list_vc_service_offering(self, input_filters: QueryFilter, page: int, size: int):
        prefix = """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX cred: <https://www.w3.org/2018/credentials#>
            PREFIX gx: <https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#>
            PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>
            PREFIX aster-data: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data#>
            """

        query = prefix + self.__build_list_services_query(input_filters, page, size)

        my_result_json = self.__call_graphdb_get_query(query)
        results = []

        try:
            if "results" in my_result_json and "bindings" in my_result_json["results"]:
                result_services = my_result_json["results"]["bindings"]

                results.extend(
                    {
                        "service_vc_did": get_value_if_exist(rs, "vc_service"),
                        "provider_vc_did": get_value_if_exist(rs, "vc_provider"),
                        "located_service_vc_did": get_value_if_exist(rs, "vc_located_service"),
                        "location_vc_did": get_value_if_exist(rs, "vc_location"),
                        "compliance_vc_did": get_value_if_exist(rs, "vc_compliance"),
                        "compliance_references": get_value_if_exist(rs, "compliance_reference"),
                        "granted_label_vc_did": get_value_if_exist(rs, "vc_granted_label"),
                        "aster_compliance_id": get_value_if_exist(rs, "vc_aster_compliance"),
                    }
                    for rs in result_services
                    if len(rs) != 0
                )
        except Exception as e:
            log.debug(f"Error to retrieve data because {e}")

        return results

    @staticmethod
    def __build_list_services_query(input_filters, page=None, size=None):
        select_data_product = """
                    ?service rdf:type aster-data:dataProduct .
                    ?service gx:providedBy ?provider .
                    ?service gx:name ?service_name .
                    ?vc_service cred:credentialSubject ?service .
                    ?vc_provider cred:credentialSubject ?provider .
                    OPTIONAL {
                        ?vc_compliance cred:credentialSubject ?vc_service .
                        ?vc_compliance cred:issuer ?vc_compliance_issuer .
                        FILTER(STRSTARTS(STR(?vc_compliance_issuer), "did:web:compliance.lab.gaia-x.eu:")).
                    }
        """
        select_ces_services = """
                    ?service rdf:type gx:ServiceOffering .
                    ?service gx:providedBy ?provider .
                    ?service gx:name ?service_name .
                    ?vc_service cred:credentialSubject ?service .
                    ?vc_provider cred:credentialSubject ?provider .
                    FILTER NOT EXISTS { ?located_service aster-conformity:isImplementationOf ?service . } .
        """
        select_federation_services = """
                    ?service rdf:type gx:ServiceOffering .
                    ?service gx:providedBy ?provider .
                    ?service gx:name ?service_name .
                    ?vc_service cred:credentialSubject ?service .
                    ?vc_provider cred:credentialSubject ?provider .
                    ?located_service aster-conformity:isImplementationOf ?service .
                    ?vc_located_service cred:credentialSubject ?located_service .
                    ?located_service aster-conformity:isHostedOn ?location .
                    ?vc_location cred:credentialSubject ?location .
                    OPTIONAL {
                        ?compliance_certificate_claim aster-conformity:hasLocatedServiceOffering ?located_service .
                        ?compliance_certificate_claim aster-conformity:hasComplianceCertificationScheme ?compliance_certification_scheme .
                        ?compliance_certification_scheme aster-conformity:hasComplianceReference ?compliance_reference .
                    }
                    OPTIONAL {
                        ?compliance_label rdf:type aster-conformity:ComplianceLabel .
                        ?compliance_label aster-conformity:hasLevel ?label_level .
                        ?granted_label aster-conformity:label ?compliance_label .
                        ?granted_label aster-conformity:vc ?vc_located_service .
                        ?vc_granted_label cred:credentialSubject ?granted_label .
                    }
                    OPTIONAL {
                        ?vc_compliance cred:credentialSubject ?vc_service .
                        ?vc_compliance cred:issuer ?vc_compliance_issuer .
                        FILTER(STRSTARTS(STR(?vc_compliance_issuer), "did:web:compliance.lab.gaia-x.eu:")).
                    }
                    OPTIONAL {
                        ?aster_compliance rdf:type aster-conformity:AsterXCompliance .
                        ?aster_compliance aster-conformity:hasGaiaXCompliance ?vc_ser .
                        ?vc_aster_compliance cred:credentialSubject ?aster_compliance .
                        FILTER(?vc_service = IRI(?vc_ser)) .
                    }
        """

        filters, select_federation_services = build_filters(input_filters, select_federation_services)
        # filters are rebuild with select for data-product but no need to keep them, same filter for two select
        same_filters, select_data_product = build_filters(input_filters, select_data_product)

        select = """
            select distinct ?vc_service ?vc_provider ?vc_located_service ?vc_location ?vc_compliance (group_concat(distinct ?compliance_reference ; separator="','") as ?compliance_reference) ?vc_granted_label ?vc_aster_compliance
            where {
                {
                    %s
                }
                UNION
                {
                    %s
                }
                UNION
                {
                    %s
                }
            """ % (
            select_federation_services,
            select_ces_services,
            select_data_product,
        )

        group_by = """
            }
            group by ?vc_service ?vc_provider ?keyword ?vc_located_service ?vc_location ?vc_compliance ?vc_granted_label ?vc_aster_compliance
            ORDER BY DESC(?label_level) ASC(?service_name)
        """

        limit = build_pagination(page=page, size=size)

        query = select + filters + group_by

        if limit:
            query = query + limit

        return query

    def count_list_vc_service_offering(self, input_filters: QueryFilter):
        prefix = """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX cred: <https://www.w3.org/2018/credentials#>
            PREFIX gx: <https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#>
            PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>
            PREFIX aster-data: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data#>
            """

        query = self.__build_list_services_query(input_filters)

        query = (
            prefix
            + """
            SELECT (COUNT(*) as ?total)
            where {
                %s
            }
        """
            % query
        )

        my_result_json = self.__call_graphdb_get_query(query)
        results = []

        try:
            if "results" in my_result_json and "bindings" in my_result_json["results"]:
                result_services = my_result_json["results"]["bindings"]

                results.extend(
                    {
                        "total": get_value_if_exist(rs, "total"),
                    }
                    for rs in result_services
                    if len(rs) != 0
                )
        except Exception as e:
            log.debug(f"Error to retrieve data because {e}")

        return results

    def list_vc_providers(self):
        query = """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX cred: <https://www.w3.org/2018/credentials#>
            PREFIX gx: <https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#>
            PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>
            PREFIX aster-data: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data#>

            select distinct ?vc_provider
            where {
                ?service rdf:type gx:ServiceOffering .
                ?service gx:providedBy ?provider .
                ?vc_provider cred:credentialSubject ?provider .
            }
        """

        my_result_json = self.__call_graphdb_get_query(query)
        results = []

        try:
            if "results" in my_result_json and "bindings" in my_result_json["results"]:
                result_services = my_result_json["results"]["bindings"]

                results.extend(
                    {
                        "provider_vc_did": get_value_if_exist(rs, "vc_provider"),
                    }
                    for rs in result_services
                    if len(rs) != 0
                )
        except Exception as e:
            log.debug(f"Error to retrieve data because {e}")

        return results

    def list_address_light(self):
        query = """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX cred: <https://www.w3.org/2018/credentials#>
            PREFIX gx: <https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#>
            PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>
            PREFIX aster-data: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data#>

            select distinct ?country ?urban_area
            where {
                ?location rdf:type aster-conformity:Location .
                ?location aster-conformity:country ?country .
                ?location aster-conformity:urbanArea ?urban_area .
            }
            order by ?country ?urban_area
        """

        my_result_json = self.__call_graphdb_get_query(query)
        results = []

        try:
            if "results" in my_result_json and "bindings" in my_result_json["results"]:
                result_services = my_result_json["results"]["bindings"]

                results.extend(
                    {
                        "country": get_value_if_exist(rs, "country"),
                        "urban_area": get_value_if_exist(rs, "urban_area"),
                    }
                    for rs in result_services
                    if len(rs) != 0
                )
        except Exception as e:
            log.debug(f"Error to retrieve data because {e}")

        return results

    def list_vc_locations(self):
        query = """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX cred: <https://www.w3.org/2018/credentials#>
            PREFIX gx: <https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#>
            PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>
            PREFIX aster-data: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-data#>

            select distinct ?vc_location
            where {
                ?location rdf:type aster-conformity:Location .
                ?vc_location cred:credentialSubject ?location .
            }
        """

        my_result_json = self.__call_graphdb_get_query(query)
        results = []

        try:
            if "results" in my_result_json and "bindings" in my_result_json["results"]:
                result_services = my_result_json["results"]["bindings"]

                results.extend(
                    {
                        "location_vc_did": get_value_if_exist(rs, "vc_location"),
                    }
                    for rs in result_services
                    if len(rs) != 0
                )
        except Exception as e:
            log.debug(f"Error to retrieve data because {e}")

        return results

    def list_compliance_labels(self, level: int):
        query = """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>

            select distinct ?compliance_label
            where {
                ?compliance_label rdf:type aster-conformity:ComplianceLabel .
                ?compliance_label aster-conformity:hasLevel ?level .
        """

        if level is not None:
            query = query + f"FILTER(?level = 'Level {level}') ."

        query = query + "}"

        my_result_json = self.__call_graphdb_get_query(query)
        results = []

        try:
            if "results" in my_result_json and "bindings" in my_result_json["results"]:
                result_services = my_result_json["results"]["bindings"]

                results.extend(
                    {
                        "compliance_label_did": get_value_if_exist(rs, "compliance_label"),
                    }
                    for rs in result_services
                    if len(rs) != 0
                )
        except Exception as e:
            log.debug(f"Error to retrieve data because {e}")

        return results

    def list_compliance_references(self):
        query = """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>

            select distinct ?compliance_reference
            where {
                ?compliance_reference rdf:type aster-conformity:ComplianceReference .
            }
        """

        my_result_json = self.__call_graphdb_get_query(query)
        results = []

        try:
            if "results" in my_result_json and "bindings" in my_result_json["results"]:
                result_services = my_result_json["results"]["bindings"]

                results.extend(
                    {
                        "compliance_reference_did": get_value_if_exist(rs, "compliance_reference"),
                    }
                    for rs in result_services
                    if len(rs) != 0
                )
        except Exception as e:
            log.debug(f"Error to retrieve data because {e}")

        return results

    def list_criterions(self, can_be_self_assessed: bool):
        query = """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX aster-conformity: <https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity#>

            select distinct ?compliance_criterion ?p ?o
            where {
                ?compliance_criterion rdf:type aster-conformity:ComplianceCriterion .
        """

        if can_be_self_assessed is not None:
            query = query + f"?compliance_criterion aster-conformity:canBeSelfAssessed {can_be_self_assessed}"

        query = query + "}"

        my_result_json = self.__call_graphdb_get_query(query)
        results = []

        try:
            if "results" in my_result_json and "bindings" in my_result_json["results"]:
                result_services = my_result_json["results"]["bindings"]

                results.extend(
                    {
                        "compliance_criterion_did": get_value_if_exist(rs, "compliance_criterion"),
                    }
                    for rs in result_services
                    if len(rs) != 0
                )
        except Exception as e:
            log.debug(f"Error to retrieve data because {e}")

        return results

    def delete_did_document(self, did_document):
        document_type = get_type_from_jsonld(did_document)

        if document_type:
            if __is_a_verifiable_credential__(document_type):
                log.info(f"Il faut supprimer un VC d'id {get_id_from_jsonld(did_document)}")
                return self.__delete_vc(get_id_from_jsonld(did_document))
            else:
                log.info(f"Il faut supprimer un CRED d'id {get_id_from_jsonld(did_document)}")
                return self.__delete_credential(get_id_from_jsonld(did_document))

    def delete_provider(self, did: str):
        log.debug(f"Deleting provider: {did}")
        query = ASTER_DELETE_PROVIDER_QUERY.replace("#PROVIDER_ID#", did)

        return self.__call_graphdb_post_query(query)

    def delete_service_from_did(self, did: str):
        return self.__delete_service_with_did(did)

    def __delete_service(self, document_type: str, did_document: Any):
        if document_type not in ["ServiceOffering", "LocatedServiceOffering", "DataProduct"]:
            return

        did = did_document["@id"]
        log.debug(f"Deleting Service: {did}")
        query = DELETE_SERVICE_QUERY.replace("#SERVICE_ID#", did).replace("#SERVICE_TYPE#", document_type)

        return self.__call_graphdb_post_query(query)

    def __delete_service_with_did(self, did: str):
        log.debug(f"Deleting Service: {did}")
        query = ASTER_DELETE_SERVICE_QUERY.replace("#SERVICE_ID#", did).replace("#SERVICE_TYPE#", "ServiceOffering")

        return self.__call_graphdb_post_query(query)

    def __delete_location(self, document_type: str, did_document: Any):
        if document_type not in ["Location"]:
            return

        did = did_document["@id"]
        log.debug(f"Deleting Location: {did}")
        query = DELETE_LOCATION_QUERY.replace("#LOCATION_ID#", did)

        return self.__call_graphdb_post_query(query)

    def __delete_vc(self, _id: str):
        log.debug(f"Deleting VC with id: {_id}")
        query = DELETE_VC_QUERY.replace("#VC_ID#", _id)

        return self.__call_graphdb_post_query(query)

    def __delete_credential(self, _id: str):
        log.debug(f"Deleting Credential with id: {_id}")
        query = DELETE_CREDENTIAL_QUERY.replace("#CRED_ID#", _id)

        return self.__call_graphdb_post_query(query)


__KEY_PATTERN = "#KEY"
__VALUE_PATTERN = "#VALUE"


# __SPARQL_RESULTS_JSON_TYPE = "application/sparql-results+json"


def build_filters(input_filters: QueryFilter, select_federation_services: str):
    filters = ""

    if input_filters.provider_ids:
        filters = input_filters.apply_provider_filter(filters)

    if input_filters.location_ids:
        filters = input_filters.apply_location_filter(filters)

    if input_filters.keywords:
        filters, select_federation_services = input_filters.apply_keyword_filter(filters, select_federation_services)

    if input_filters.countries:
        filters, select_federation_services = input_filters.apply_country_filter(filters, select_federation_services)

    if input_filters.urban_areas:
        filters, select_federation_services = input_filters.apply_urban_area_filter(filters, select_federation_services)

    if input_filters.layers:
        filters, select_federation_services = input_filters.apply_layer_filter(filters, select_federation_services)

    if input_filters.compliance_ref_ids:
        filters, select_federation_services = input_filters.apply_compliance_ref_filter(
            filters, select_federation_services
        )

    if input_filters.aster_label_ids:
        filters, select_federation_services = input_filters.apply_aster_label_filter(
            filters, select_federation_services
        )

    if input_filters.search_service_name:
        filters = input_filters.apply_search_service_name(filters)

    log.debug(f"Filters: {filters}")

    return filters, select_federation_services


def transform_into_sparql_id_array(ids):
    array_ids = []

    for id in ids:
        array_ids.append(f"<{id}>")

    array_string = f"{array_ids}".replace("[", "(").replace("]", ")").replace("'", "")

    return array_string
