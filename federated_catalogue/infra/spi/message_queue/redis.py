# -*- coding: utf-8 -*-
import logging

from redis.asyncio.client import Redis

log = logging.getLogger(__name__)


class Publisher:
    def __init__(self, channel_name: str, url: str) -> None:
        log.debug(f"Creating publisher to {channel_name}@{url}")
        self.redis_client = Redis.from_url(url)
        self.channel_name = channel_name

    async def publish(self, message):
        log.debug(f"Publishing message {message}")
        await self.redis_client.publish(self.channel_name, message=message)
