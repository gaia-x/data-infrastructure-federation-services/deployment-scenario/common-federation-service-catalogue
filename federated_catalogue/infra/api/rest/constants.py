# -*- coding: utf-8 -*-
from typing import Final

API_TITLE: Final = "Federated Catalogue"
API_DESCRIPTION: Final = """
REST API of the GXFS-FR Federated Catalogue

[Source repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue)
"""

TAGS_METADATA: Final = [
    {"name": "Query", "description": "Send graph queries to catalogue."},
    {"name": "References", "description": "Get references data of the federation."},
    {"name": "Catalogue synchronisation", "description": "API to synchronise catalogue"},
]
