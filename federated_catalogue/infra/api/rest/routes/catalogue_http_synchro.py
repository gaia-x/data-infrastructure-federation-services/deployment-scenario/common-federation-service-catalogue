# -*- coding: utf-8 -*-
import http
import logging

from fastapi import APIRouter, Depends
from federated_catalogue.Config import Settings, get_settings
from federated_catalogue.domain.catalogue import SynchroStatus, SynchroStatusValue
from federated_catalogue.infra.api.rest.utils import api_key_auth
from federated_catalogue.services.catalogue import CatalogueService
from starlette.background import BackgroundTasks
from starlette.responses import JSONResponse

log = logging.getLogger(__name__)
settings: Settings = get_settings()


def create_catalogue_http_synchro_router(catalogue_service: CatalogueService) -> APIRouter:
    """
    The create_catalogue_http_synchro_router function creates a router for the Federated Catalogue API for the catalogue
    synchronisation via http protocol.

    Args:

    Returns:
        A fastapi router, which is a special object that can be used to define
    """
    catalogue_http_synchro_router = APIRouter()

    @catalogue_http_synchro_router.post(
        "/launch-http-synchro", tags=["Catalogue HTTP synchronisation"], dependencies=[Depends(api_key_auth)]
    )
    async def launch_http_synchro_with_provider_catalogues():
        """
            Use to launch synchronisation with registered provider catalogue.

            Will browse the provider catalogue list and will synchronize Federated Catalogue with provider catalogue :
                - add unknown did and content from provider catalogue
                - delete did present inside federated catalogue but not in provider catalogue

            If a VC associated to a did is updated, it's the responsibility of the provider to delete did inside its
            catalogue and to add it to force a refresh inside Federated Catalogue
        :return:
        """
        await catalogue_service.launch_http_synchronisation()

        return "OK"

    @catalogue_http_synchro_router.get("/http-synchro-status", tags=["Catalogue HTTP synchronisation"])
    async def get_http_synchro_status():
        """
            Use to get HTTP synchro status
        :return:
        """
        http_synchro_status: SynchroStatus = await catalogue_service.get_http_synchro_status()

        return http_synchro_status

    @catalogue_http_synchro_router.post(
        "/launch-async-http-synchro", tags=["Catalogue HTTP synchronisation"], dependencies=[Depends(api_key_auth)]
    )
    async def launch_async_http_synchro_with_provider_catalogues(background_tasks: BackgroundTasks):
        """
            Use to launch synchronisation with registered provider catalogue.

            Will browse the provider catalogue list and will synchronize Federated Catalogue with provider catalogue :
                - add unknown did and content from provider catalogue
                - delete did present inside federated catalogue but not in provider catalogue

            If a VC associated to a did is updated, it's the responsibility of the provider to delete did inside its
            catalogue and to add it to force a refresh inside Federated Catalogue
        :return:
        """
        http_synchro_status: SynchroStatus = await catalogue_service.get_http_synchro_status()

        if http_synchro_status is None or http_synchro_status.status == SynchroStatusValue.DONE:
            background_tasks.add_task(catalogue_service.launch_http_synchronisation)

            return "HTTP provider catalogue synchronisation is launched."
        else:
            return "HTTP synchronisation is pending, please wait to finish this one before to launch a new one."

    @catalogue_http_synchro_router.post(
        "/provider-catalogue-url", tags=["Catalogue HTTP synchronisation"], dependencies=[Depends(api_key_auth)]
    )
    async def add_provider_catalogue_url(provider_catalogue_url: str):
        """
            Use to register a new provider-catalogue url
        :return:
        """
        await catalogue_service.register_provider_catalogue(provider_catalogue_url)

        return "OK"

    @catalogue_http_synchro_router.get(
        "/provider-catalogue-url", tags=["Catalogue HTTP synchronisation"], dependencies=[Depends(api_key_auth)]
    )
    async def get_provider_catalogue_url():
        """
            Use to get registered provider-catalogue urls
        :return:
        """
        provider_urls = await catalogue_service.get_registered_provider_catalogue()

        return provider_urls

    @catalogue_http_synchro_router.delete(
        "/provider-catalogue-url", tags=["Catalogue HTTP synchronisation"], dependencies=[Depends(api_key_auth)]
    )
    async def delete_provider_catalogue_url(provider_catalogue_url: str):
        """
            Use to delete registered provider-catalogue urls
        :return:
        """
        try:
            await catalogue_service.unregister_provider_catalogue(provider_catalogue_url)
        except ValueError:
            return JSONResponse(
                content=f"Unable to find {provider_catalogue_url} in our registry.",
                status_code=http.HTTPStatus.NOT_FOUND,
            )

        return "OK"

    return catalogue_http_synchro_router
