# -*- coding: utf-8 -*-
import logging

from fastapi import APIRouter, Body, Depends
from federated_catalogue.Config import Settings, get_settings
from federated_catalogue.domain.catalogue import SynchroStatus, SynchroStatusValue
from federated_catalogue.infra.api.rest.utils import api_key_auth
from federated_catalogue.services.ces_event_consumer import CesEventConsumer
from starlette.background import BackgroundTasks

log = logging.getLogger(__name__)
settings: Settings = get_settings()


def create_catalogue_ces_synchro_router(
    ces_event_consumer: CesEventConsumer,
) -> APIRouter:
    """
    The create_catalogue_ces_synchro_router function creates a router for the Federated Catalogue API for the CES
    synchronisation.

    Args:
        ces_event_consumer: CesEventConsumer: Pass the CesEventConsumer object to the function

    Returns:
        A fastapi router, which is a special object that can be used to define
        :param ces_event_consumer:
    """
    catalogue_ces_synchro_router = APIRouter()

    @catalogue_ces_synchro_router.post(
        "/ces-event", tags=["Catalogue synchronisation"], dependencies=[Depends(api_key_auth)]
    )
    async def push_ces_event_inside_federated_catalogue(ces_events=Body()):
        """
            Use to push VC from CES_EVENT inside Catalogue (Graph database + Redis database)

            Extract from the body request an array of id (did or http) and retrieve them to inject inside catalogue.
            Condition ? Not for the moment.
        :return:
        """
        await ces_event_consumer.consume_ces_events(ces_events)

        return "OK"

    @catalogue_ces_synchro_router.get("/ces-synchro-status", tags=["Catalogue synchronisation"])
    async def get_ces_synchro_status():
        """
            Use to get CES synchro status
        :return:
        """
        ces_synchro_status: SynchroStatus = await ces_event_consumer.get_ces_synchro_status()

        return ces_synchro_status

    @catalogue_ces_synchro_router.post(
        "/consume-ces", tags=["Catalogue synchronisation"], dependencies=[Depends(api_key_auth)]
    )
    async def consume_ces(background_tasks: BackgroundTasks):
        """
            Use to launch synchronisation with CES inside Catalogue

        :return:
        """
        ces_synchro_status: SynchroStatus = await ces_event_consumer.get_ces_synchro_status()

        if ces_synchro_status is None or ces_synchro_status.status == SynchroStatusValue.DONE:
            background_tasks.add_task(ces_event_consumer.consume_ces)

            return "CES synchronisation is launched."
        else:
            return "CES synchronisation is pending, please wait to finish this one before to launch a new one."

    return catalogue_ces_synchro_router
