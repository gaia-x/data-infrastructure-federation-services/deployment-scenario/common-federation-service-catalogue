# -*- coding: utf-8 -*-
import logging
from typing import Annotated

import requests
from fastapi import APIRouter, Body, Depends, Query, Response
from fastapi.responses import JSONResponse

# TODO : remove this import
from federated_catalogue import graphdb_client
from federated_catalogue.Config import (
    FEDERATION_REPOSITORY,
    LAYERS,
    SERVICE_TYPES,
    Settings,
    get_settings,
)
from federated_catalogue.domain.query_filter import QueryFilter
from federated_catalogue.infra.api.rest.utils import api_key_auth
from federated_catalogue.services.catalogue import CatalogueService
from federated_catalogue.utils import get_json_from_url, transform_did_into_url

log = logging.getLogger(__name__)
settings: Settings = get_settings()


def create_catalogue_router(
    catalogue_service: CatalogueService,
) -> APIRouter:
    """
    The create_catalogue_router function creates a router for the Federated Catalogue API.

    Args:
        catalogue_service: CatalogueService: Pass the catalogueservice object to the function
        item_service: ItemService: Create an instance of the itemservice class

    Returns:
        A fastapi router, which is a special object that can be used to define
        :param catalogue_service:
    """
    catalogue_router = APIRouter()

    @catalogue_router.get("/api/filters-data", tags=["References"])
    async def get_filter_data_list():
        available_filters = await catalogue_service.list_available_filters()

        return JSONResponse(available_filters)

    @catalogue_router.get("/api/providers", tags=["References"])
    async def get_providers_list():
        return await catalogue_service.list_providers()

    @catalogue_router.get("/api/locations", tags=["References"])
    async def get_locations_list():
        return await catalogue_service.list_locations()

    @catalogue_router.get("/api/country_urban_area", tags=["References"])
    async def get_country_and_urban_area_list():
        return await catalogue_service.list_address_light()

    @catalogue_router.get("/api/compliance_references", tags=["References"])
    async def get_compliance_reference_list():
        return await catalogue_service.list_compliance_references()

    @catalogue_router.get("/api/compliance_labels", tags=["References"])
    async def get_compliance_labels(level: int = None):
        return await catalogue_service.list_compliance_labels(level)

    @catalogue_router.get("/api/compliance_criterions", tags=["References"])
    async def get_compliance_criterions(
        can_be_self_assessed: bool | None = Query(default=None, alias="canBeSelfAssessed")
    ):
        return await catalogue_service.list_criterions(can_be_self_assessed)

    @catalogue_router.get("/api/service_types", tags=["References"])
    def get_service_type_list():
        return SERVICE_TYPES

    @catalogue_router.get("/api/layers", tags=["References"])
    def get_layer_list():
        return LAYERS

    @catalogue_router.post("/api/search_third_party_compliance_certification_claim", tags=["Query"])
    def get_third_party_compliance_certification_claim(
        compliance_certification_scheme_did: str, located_service_offering_did: str, compliance_assessment_body_did: str
    ):
        # que faire s'il manque un paramètre ?
        # si les 3 sont obligatoires, peut-être faire un get plutôt qu'un post
        # return json.dumps(Config.LAYERS)
        third_party_compliance_certification_claim_dids = graphdb_client.get_third_party_compliance_certification_claim(
            compliance_assessment_body_did, located_service_offering_did, compliance_certification_scheme_did
        )

        tpcc_claim_vcs = []
        for tpcc_claim_did in third_party_compliance_certification_claim_dids:
            tpcc_claim_url = transform_did_into_url(tpcc_claim_did["vc_did"])
            tpcc_claim_vc = get_json_from_url(tpcc_claim_url)
            if tpcc_claim_vc is not None:
                tpcc_claim_vcs.append(tpcc_claim_vc)

        return tpcc_claim_vcs

    @catalogue_router.post("/query_page", tags=["Query"])
    async def query(filters: QueryFilter = Body(), page: int = None, size: int = None):
        """
        Launch a query retrieve a list of VCs from the federated-catalogue.

        The query can be filtered with different values.
        """

        return JSONResponse(
            content=await catalogue_service.query_services_with_total(filters=filters, page=page, size=size)
        )

    @catalogue_router.post("/query", tags=["Query"])
    async def query(filters: dict = Body(), page: int = None, size: int = None):
        query_filter = QueryFilter()

        for key in filters:
            match key:
                case "provider_did":
                    query_filter.provider_ids = [filters[key]]
                case "location_did":
                    query_filter.location_ids = [filters[key]]
                case "compliance_ref_did":
                    query_filter.compliance_ref_ids = [filters[key]]
                case "aster_label_did":
                    query_filter.aster_label_ids = [filters[key]]
                case "keywords":
                    query_filter.keywords = filters[key]
                case "countries":
                    query_filter.countries = filters[key]
                case "urban_areas":
                    query_filter.urban_areas = filters[key]
                case "layers":
                    query_filter.layers = filters[key]
                case "search_service_name":
                    query_filter.search_service_name = filters[key]

        results = await catalogue_service.query_services(filters=query_filter, page=page, size=size)

        return results

    @catalogue_router.get("/federated-catalog", tags=["Catalogue synchronisation"])
    async def get_federated_catalog() -> Response:
        catalog = await catalogue_service.build_catalog()

        return Response(content=CatalogueService.catalog_to_json(catalog), media_type="application/ld+json")

    # TODO : use by test, see if we keep here or no
    @catalogue_router.delete("/demo-data", dependencies=[Depends(api_key_auth)], tags=["Catalogue synchronisation"])
    def clean_demo_repository():
        headers = {"Content-Type": "application/ld+json"}
        requests.delete(f"{settings.graphdb_url}/repositories/{FEDERATION_REPOSITORY}/statements", headers=headers)
        # print(r.text)

        return "DEMO DATA CLEANED"

    @catalogue_router.delete(
        "/service-offering/dids", dependencies=[Depends(api_key_auth)], tags=["Catalogue synchronisation"]
    )
    def clean_service_offering_repository(dids: Annotated[list, Body()]):
        return JSONResponse(catalogue_service.clean_service_with_dids(dids))

    @catalogue_router.delete(
        "/provider-catalogue", dependencies=[Depends(api_key_auth)], tags=["Catalogue synchronisation"]
    )
    async def clean_provider_catalogue(dids: Annotated[list, Body()]):
        """
            Use to delete catalogue of provider indicated by their did.

        :param dids: did of provider (and not did of their VC !)
        :return:
        """
        return await catalogue_service.delete_provider_with_dids(dids)

    @catalogue_router.post(
        "/service-offering-vc", tags=["Catalogue synchronisation"], dependencies=[Depends(api_key_auth)]
    )
    async def add_service_offering_vc(service_offering_vc=Body()):
        """
            Use to add a VC of ServiceOffering inside the catalogue.

            This methode is used mainly to debug
        :return:
        """
        await catalogue_service.add_service_offering_vc(service_offering_vc)

        return "OK"

    return catalogue_router
