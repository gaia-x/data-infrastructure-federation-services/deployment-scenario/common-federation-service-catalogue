# -*- coding: utf-8 -*-
from fastapi import HTTPException, Security
from fastapi.security.api_key import APIKeyHeader
from federated_catalogue.Config import Settings, get_settings
from starlette.status import HTTP_403_FORBIDDEN

settings: Settings = get_settings()
api_key_header = APIKeyHeader(name="x-api-key", auto_error=False)


def api_key_auth(api_key_header: str = Security(api_key_header)):
    if api_key_header == settings.api_key_authorized.get_secret_value():
        return api_key_header
    else:
        raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail="Could not validate API KEY")
