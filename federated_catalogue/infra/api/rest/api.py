# -*- coding: utf-8 -*-

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from federated_catalogue.infra.api.rest.constants import (
    API_DESCRIPTION,
    API_TITLE,
    TAGS_METADATA,
)
from federated_catalogue.infra.api.rest.routes.catalogue import create_catalogue_router
from federated_catalogue.infra.api.rest.routes.catalogue_ces_synchro import (
    create_catalogue_ces_synchro_router,
)
from federated_catalogue.infra.api.rest.routes.catalogue_http_synchro import (
    create_catalogue_http_synchro_router,
)
from federated_catalogue.infra.api.rest.routes.common import create_common_router
from federated_catalogue.services.catalogue import CatalogueService
from federated_catalogue.services.ces_event_consumer import CesEventConsumer
from federated_catalogue.services.healthcheck import HealthcheckService
from starlette.middleware.gzip import GZipMiddleware


def create_fastapi_application(
    lifespan,
    catalogue_service: CatalogueService,
    healthcheck_service: HealthcheckService,
    ces_event_consumer: CesEventConsumer,
) -> FastAPI:
    """
    The create_fastapi_application function creates a FastAPI application object.

    Args:

    Returns:
        A fastapi object
    """

    app = FastAPI(
        lifespan=lifespan,
        docs_url="/api/doc",
        redoc_url=None,
        title=API_TITLE,
        openapi_tags=TAGS_METADATA,
        description=API_DESCRIPTION,
        version="22.10",
        license_info={
            "name": "Apache 2.0",
            "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
        },
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.add_middleware(GZipMiddleware, minimum_size=1000)

    app.include_router(router=create_common_router(healthcheck_service=healthcheck_service), tags=["Common"])
    app.include_router(router=create_catalogue_router(catalogue_service=catalogue_service))
    app.include_router(router=create_catalogue_ces_synchro_router(ces_event_consumer=ces_event_consumer))
    app.include_router(router=create_catalogue_http_synchro_router(catalogue_service=catalogue_service))

    return app
