import asyncio
import contextlib
import logging
from typing import Any, Awaitable, Callable, Dict

import async_timeout
from federated_catalogue.Config import Settings, get_settings
from redis.asyncio.client import PubSub, Redis

log = logging.getLogger(__name__)
settings: Settings = get_settings()


class Subscriber:
    def __init__(
        self,
        channel_name_prefix: str,
        url: str,
        message_handler: Callable[[str], Awaitable[None]] = None,
    ) -> None:

        self.url = url
        log.debug(f"Creating subcriber to {channel_name_prefix}/*@{url}")
        self.redis_client = Redis.from_url(url, decode_responses=True)

        self.pubsub = self.redis_client.pubsub()
        self.message_handler = message_handler
        self.channel_name = f"{channel_name_prefix}/*"
        self.task = None

    async def start(self) -> None:
        await self.pubsub.psubscribe(self.channel_name)
        self.task = asyncio.create_task(self._reader(self.pubsub))

    async def stop(self) -> None:
        self.task.cancel()

    async def _reader(self, channel: PubSub):
        try:
            while True:
                with contextlib.suppress(asyncio.TimeoutError):
                    async with async_timeout.timeout(300):
                        message = await channel.get_message(ignore_subscribe_messages=True)
                        if message is not None:
                            log.debug(f"Message received: {message}")
                            if self.message_handler is not None:
                                payload = message.get("data", None)
                                await self.message_handler(payload)
                        await asyncio.sleep(1)
        except asyncio.CancelledError:
            log.debug("Received request to cancel")
            log.debug("Closing Redis connection")
            await self.pubsub.unsubscribe(self.channel_name)
            await self.pubsub.close()
        except Exception as err:
            log.exception("Unable to read messages", err)
            log.debug("Attempting to reconnect to Redis")
            await asyncio.sleep(5)

            self.redis_client = Redis.from_url(self.url, decode_responses=True)
            self.pubsub = self.redis_client.pubsub()
            await self.pubsub.psubscribe(self.channel_name)

    @staticmethod
    def __default_handler(message: Dict[str, Any]) -> None:
        log.debug(f"Default Handler -- message : {message}")
