# -*- coding: utf-8 -*-
import asyncio
import json
import os
from dataclasses import dataclass, field
from typing import List

import pytest
from dataclasses_json import Undefined, config, dataclass_json
from fastapi.testclient import TestClient
from federated_catalogue import Config, graphdb_client
from federated_catalogue.Config import Settings, get_settings
from federated_catalogue.domain.interfaces.graph_repository import GraphRepository
from federated_catalogue.domain.interfaces.repository import CatalogueRepository
from federated_catalogue.infra.api.rest.api import create_fastapi_application
from federated_catalogue.infra.spi.db.redis import RedisCatalogueRepository
from federated_catalogue.infra.spi.graph_repository.graphdb import GraphDBRepository
from federated_catalogue.services.catalogue import CatalogueService
from federated_catalogue.services.ces_event_consumer import CesEventConsumer
from federated_catalogue.services.healthcheck import HealthcheckService
from federated_catalogue.services.redis_catalogue_subscriber import (
    RedisCatalogueSubscriber,
)
from redis.asyncio import Redis

settings: Settings = get_settings()


@pytest.fixture(scope="session")
def make_catalog_app():
    graphdb_client.launch_init_repository(
        Config.FEDERATION_REPOSITORY, os.path.join(Config.ROOT_DIR, "graphdb/config/config.ttl")
    )

    data_sets = [
        os.path.join(Config.ROOT_DIR, "tests/data/ikoula-dataset.json"),
        os.path.join(Config.ROOT_DIR, "tests/data/gigas-dataset.json"),
        os.path.join(Config.ROOT_DIR, "tests/data/federation-dataset.json"),
    ]
    for ds in data_sets:
        with open(ds, "r", encoding="utf-8") as f:
            graphdb_client.post_json_ld_content(Config.FEDERATION_REPOSITORY, json.loads(f.read()))

    redis_client = Redis.from_url(settings.redis_host)
    catalogue_repository: CatalogueRepository = RedisCatalogueRepository(redis_client=redis_client)
    graph_repository: GraphRepository = GraphDBRepository()

    catalogue_service = CatalogueService(catalogue_repository, graph_repository)
    healthcheck_service = HealthcheckService(repository=catalogue_repository)
    ces_event_consumer = CesEventConsumer(catalogue_service=catalogue_service, redis_client=redis_client)

    redis_catalogue_subscriber = RedisCatalogueSubscriber(catalogue_service)

    app = create_fastapi_application(
        lifespan=None,
        catalogue_service=catalogue_service,
        healthcheck_service=healthcheck_service,
        ces_event_consumer=ces_event_consumer,
    )

    yield app

    # clean up / reset resources here
    # app.test_client().delete("/demo-data", headers={'x-api-key': Config.API_KEY_AUTHORIZED})
    TestClient(app).delete("/demo-data", headers={"x-api-key": settings.api_key_authorized.get_secret_value()})
    redis_catalogue_subscriber.subscriber.stop()


@pytest.fixture()
def client(make_catalog_app):
    return TestClient(make_catalog_app)


@pytest.fixture()
def secured_headers():
    return {"x-api-key": settings.api_key_authorized.get_secret_value()}


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(order=True, eq=True, frozen=True)
class Provider:
    provider_did: str = field(metadata=config(field_name="@id"))


@dataclass_json
@dataclass(order=True, eq=True, frozen=True)
class CatalogItem:
    provider: str
    services: List[str]


@dataclass_json
@dataclass(order=True, eq=True, frozen=True)
class Location:
    country_name: str
    location_did: str
    provider_designation: str
    provider_did: str
    state: str
    urban_area: str


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(order=True, eq=True, frozen=True)
class ComplianceReference:
    compliance_ref_did: str = field(metadata=config(field_name="id"))
    compliance_reference_manager: str = field(
        metadata=config(field_name="aster-conformity:hasComplianceReferenceManager")
    )
    reference_url: str = field(metadata=config(field_name="aster-conformity:hasReferenceUrl"))
    title: str = field(metadata=config(field_name="aster-conformity:hasComplianceReferenceTitle"))
    type: str = field(metadata=config(field_name="aster-conformity:referenceType"))
    valid_from_date: str = field(metadata=config(field_name="aster-conformity:cRValidUntil"))
    version: str = field(metadata=config(field_name="aster-conformity:hasVersion"))


@dataclass_json
@dataclass(order=True, eq=True)
class DataFilters:
    available_layers: List[str]
    available_locations: List[Location]
    available_providers: List[Provider]
    available_services: List[str]
    available_compliance_references: List[ComplianceReference]

    def sort(self):
        self.available_layers.sort()
        self.available_locations.sort()
        self.available_providers.sort()
        self.available_services.sort()
        self.available_compliance_references.sort()


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def anyio_backend():
    return "asyncio"
