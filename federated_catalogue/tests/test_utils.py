# -*- coding: utf-8 -*-
from federated_catalogue import utils


def test_transform_did_into_url():
    did = "did:web:example:service_offering/123456/data.json"

    result = utils.transform_did_into_url(did)

    assert result == "https://example/service_offering/123456/data.json"


def test_transform_did_into_url_should_keep_url_when_is_not_did():
    url = "https://example.com/123456/data.json"

    result = utils.transform_did_into_url(url)

    assert result == "https://example.com/123456/data.json"


def test_extract_type_from_jsonld_document_without_type_is_none():
    jsonld_document = {
        "@context": {"name": "http://schema.org/name", "description": "http://schema.org/description"},
        "@id": "http://example.org/about",
        "name": "Jane Doe",
        "description": "I am Jane Doe",
    }

    assert utils.get_type_from_jsonld(jsonld_document) is None


def test_extract_type_from_jsonld_document_with_vc_type_is_vc():
    jsonld_document = {
        "@context": {"name": "http://schema.org/name", "description": "http://schema.org/description"},
        "@id": "http://example.org/about",
        "type": "VerifiableCredential",
        "name": "Jane Doe",
        "description": "I am Jane Doe",
    }

    assert utils.get_type_from_jsonld(jsonld_document) is "VerifiableCredential"


def test_extract_type_from_jsonld_document_with_array_of_type_is_array_with_same_type():
    jsonld_document = {
        "@context": {"name": "http://schema.org/name", "description": "http://schema.org/description"},
        "@id": "http://example.org/about",
        "@type": ["VerifiableCredential", "myType"],
        "name": "Jane Doe",
        "description": "I am Jane Doe",
    }

    result = utils.get_type_from_jsonld(jsonld_document)

    assert isinstance(result, list) is True
    assert result.__contains__("VerifiableCredential") is True
    assert result.__contains__("myType") is True


def test_extract_id_from_jsonld_document_without_id_is_none():
    jsonld_document = {
        "@context": {"name": "http://schema.org/name", "description": "http://schema.org/description"},
        "name": "Jane Doe",
        "description": "I am Jane Doe",
    }

    assert utils.get_id_from_jsonld(jsonld_document) is None


def test_extract_id_from_jsonld_document_with_at_id():
    jsonld_document = {
        "@context": {"name": "http://schema.org/name", "description": "http://schema.org/description"},
        "@id": "http://example.org/about",
        "type": "VerifiableCredential",
        "name": "Jane Doe",
        "description": "I am Jane Doe",
    }

    assert utils.get_id_from_jsonld(jsonld_document) is "http://example.org/about"


def test_extract_id_from_jsonld_document_with_id():
    jsonld_document = {
        "@context": {"name": "http://schema.org/name", "description": "http://schema.org/description"},
        "id": "http://example.org/about",
        "type": "VerifiableCredential",
        "name": "Jane Doe",
        "description": "I am Jane Doe",
    }

    assert utils.get_id_from_jsonld(jsonld_document) is "http://example.org/about"
