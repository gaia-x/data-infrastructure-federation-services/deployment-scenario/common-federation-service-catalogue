import json
import os

import pytest
from fakeredis import aioredis
from federated_catalogue.Config import ROOT_DIR
from federated_catalogue.domain.interfaces.graph_repository import GraphRepository
from federated_catalogue.domain.query_filter import QueryFilter
from federated_catalogue.infra.spi.db.redis import RedisCatalogueRepository
from federated_catalogue.services.catalogue import CatalogueService
from federated_catalogue.services.ces_event_consumer import CesEventConsumer


class TestCesEventConsumer:

    # TODO : to pass this test, we need to mock the utils method to retrieve json
    @pytest.mark.anyio
    async def test_consume_ces_event(self):
        # GIVEN
        fake_redis = aioredis.FakeRedis(version=6)
        catalogue_repository = RedisCatalogueRepository(redis_client=fake_redis)
        graph_repository = MockGraphRepository()
        catalogue_service = CatalogueService(catalogue_repository, graph_repository)

        ces_event_consumer = CesEventConsumer(catalogue_service=catalogue_service, redis_client=fake_redis)

        ces_event_fp = os.path.join(ROOT_DIR, "tests", "services", "ces_event.json")

        # WHEN
        with open(ces_event_fp, "r") as file:
            ces_events = json.load(file)
            await ces_event_consumer.consume_ces_events(ces_events)

        # THEN
        await fake_redis.set("TEST_KEY", "test_value")
        print(f"Keys --> {await fake_redis.keys()}")
        # print(f"Keys: {await catalogue_repository.get('https://www.delta-dao.com/.well-known/2210_gx_participant.json')}")


class MockGraphRepository(GraphRepository):
    def list_address_light(self):
        pass

    def count_list_vc_service_offering(self, input_filters: QueryFilter):
        pass

    def delete_provider(self, did: str):
        pass

    def list_criterions(self, can_be_self_assessed: bool):
        pass

    def list_compliance_references(self):
        pass

    def list_compliance_labels(self, level: int):
        pass

    def list_vc_locations(self):
        pass

    def delete_service_from_did(self, did: str):
        pass

    def list_vc_providers(self):
        pass

    def list_vc_service_offering(self, input_filters: QueryFilter, page: int, size: int):
        pass

    def delete_did_document(self, did_document):
        pass

    def post_json_ld_content(self, json_ld, did=""):
        pass
