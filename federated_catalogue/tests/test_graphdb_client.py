# -*- coding: utf-8 -*-
import pytest
from federated_catalogue import graphdb_client


class TestGraphDBClient:
    """Test class for the graphdb_client module"""

    def test_build_filters(self):
        """Ensure query filters are correctly formatted"""
        filters = {
            "provider_did": "did:web:provider",
            "location_did": "did:web:location",
            "located_service_did": "did:web:located-service",
            "service_did": "did:web:service",
            "compliance_ref_did": "did:web:compliance-ref",
            "compliance_label_level": "1",
            "random_filter": "value1",
        }
        r = graphdb_client.build_filters(filters)
        assert "FILTER(?provider = <did:web:provider>) ." in r
        assert "FILTER(?location = <did:web:location>) ." in r
        assert "FILTER(?located_service = <did:web:located-service>) ." in r
        assert "FILTER(?service = <did:web:service>) ." in r
        assert "FILTER(?compliance_reference = <did:web:compliance-ref>) ." in r
        assert "FILTER(?compliance_label_level" not in r
        assert "FILTER(?random_filter = 'value1') ." in r

    def test_clean_tabs(self):
        """Ensure tabs, spaces and quotes are correctly formatted"""
        value = "''blahblah'',"
        r = graphdb_client.clean_tabs(value)
        assert "blahblah" == r

    @pytest.mark.parametrize(
        "result,field",
        [
            ({"keyword": {"value": "''blahblah'',"}}, "keyword"),
            ({"layer": {"value": "''blahblah'',"}}, "layer"),
            ({"service_type": {"value": "''blahblah'',"}}, "service_type"),
        ],
    )
    def test_values_are_tab_cleaned(self, result, field):
        """Ensure get_value_if_exist function works well with tabs, spaces and quotes"""
        r = graphdb_client.get_value_if_exist(result, field)
        assert "blahblah" == r

    def test_not_existing_returns_empty(self):
        """Ensure get_value_if_exist function works well with empty values"""
        result = {"not keyword": {"value": "''blahblah'',"}}
        r = graphdb_client.get_value_if_exist(result, "keyword")
        assert "" == r
