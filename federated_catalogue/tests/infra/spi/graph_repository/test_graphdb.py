from federated_catalogue.infra.spi.graph_repository import graphdb
from federated_catalogue.infra.spi.graph_repository.graphdb import (
    __is_a_verifiable_credential__,
)


def test_build_pagination():
    assert graphdb.build_pagination(page=0, size=10) == "limit 10 offset 0"
    assert graphdb.build_pagination(page=1, size=10) == "limit 10 offset 10"
    assert graphdb.build_pagination(page=2, size=10) == "limit 10 offset 20"


def test_method_is_a_verifiable_credential():
    assert __is_a_verifiable_credential__("VerifiableCredential") is True
    assert __is_a_verifiable_credential__(["VerifiableCredential"]) is True
    assert __is_a_verifiable_credential__(["ServiceOffering"]) is False
    assert __is_a_verifiable_credential__("ServiceOffering") is False
    assert __is_a_verifiable_credential__(["ServiceOffering", "VerifiableCredential"]) is True
