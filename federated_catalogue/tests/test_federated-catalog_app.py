# -*- coding: utf-8 -*-
import http
import json

import pytest
from federated_catalogue.Config import ROOT_DIR
from federated_catalogue.tests.conftest import (
    ComplianceReference,
    DataFilters,
    Location,
    Provider,
)


@pytest.mark.integration
# pylint: disable=no-member
class TestApp:
    """Test main Flask app module"""

    def test_healthcheck(self, client):
        """Test /healthcheck endpoint (static)"""
        response = client.get("/healthcheck")
        assert response.status_code == 200
        assert response.status_code == http.HTTPStatus.OK
        assert "application/json" in response.headers.values()
        assert '{"catalogue repository (Redis)":true}' in response.text

    def test_get_service_types(self, client):
        """Test /api/service_types endpoint (static)"""
        response = client.get("/api/service_types")
        assert response.status_code == 200
        assert [
            "Authentication",
            "Backup",
            "Bare Metal",
            "Collaboration",
            "Compute",
            "Container",
            "Data Analytics",
            "Data Base",
            "Digital Signature",
            "ID management",
            "IoT",
            "Logs",
            "Machine Learning",
            "Networking",
            "Storage",
            "Translation",
            "Virtual Machine",
            "VPN",
            "VPS",
            "Webhosting",
            "Data Product",
        ] == json.loads(response.text)

    def test_get_layers(self, client):
        """Test /api/layers endpoint (static)"""
        response = client.get("/api/layers")
        assert response.status_code == 200
        assert ["IAAS", "PAAS", "SAAS"] == json.loads(response.text)

    # @pytest.mark.parametrize(
    #     "endpoint, dataset",
    #     [
    #         ("/api/filters-data", f"{ROOT_DIR}/tests/data/api-get-filters-data.json"),
    #     ],
    # )
    # def test_get_data_filter(self, client, endpoint, dataset):
    #     response = client.get(endpoint)
    #     assert response.status_code == 200
    #     # TODO re-enable check of content
    #     # with open(dataset, "r", encoding="utf-8") as f:
    #     #     expected = DataFilters.schema().loads(f.read())
    #     #     given = DataFilters.schema().loads(response.text)
    #     #     assert expected.sort() == given.sort()

    @pytest.mark.parametrize(
        "endpoint, dataset",
        [
            ("/api/locations", f"{ROOT_DIR}/tests/data/api-get-locations.json"),
        ],
    )
    def test_get_locations(self, client, endpoint, dataset):
        response = client.get(endpoint)
        assert response.status_code == 200
        with open(dataset, "r", encoding="utf-8") as f:
            expected = Location.schema().loads(f.read(), many=True)
            given = Location.schema().loads(response.text, many=True)
            assert expected.sort() == given.sort()

    @pytest.mark.parametrize(
        "endpoint, dataset",
        [
            ("/api/providers", f"{ROOT_DIR}/tests/data/api-get-providers.json"),
        ],
    )
    def test_get_providers(self, client, endpoint, dataset):
        response = client.get(endpoint)
        assert response.status_code == 200
        with open(dataset, "r", encoding="utf-8") as f:
            expected = Provider.schema().loads(f.read(), many=True)
            given = Provider.schema().loads(response.text, many=True)
            assert expected.sort() == given.sort()

    # def test_get_catalog(self, client):
    #     """Test /api/catalog endpoint"""
    #     response = client.get("/api/catalog")
    #     assert response.status_code == 200
    #     result_file = "federated_catalogue/tests/data/api-get-catalog.json"
    #     with open(result_file, "r", encoding="utf-8") as f:
    #         expected = CatalogItem.schema().loads(f.read(), many=True)
    #         given = CatalogItem.schema().loads(response.text, many=True)
    #         assert expected.sort() == given.sort()

    # def test_get_services(self, client):
    #     """Test /api/services endpoint"""
    #     response = client.get("/api/services")
    #     assert response.status_code == 200
    #     result_file = f"{ROOT_DIR}/tests/data/api-get-services.json"
    #     with open(result_file, "r", encoding="utf-8") as f:
    #         assert json.loads(f.read()).sort() == json.loads(response.text).sort()

    # def test_get_compliance_references(self, client):
    #     """Test /api/compliance_references endpoint"""
    #     response = client.get("/api/compliance_references")
    #     assert response.status_code == 200
    #     result_file = f"{ROOT_DIR}/tests/data/api-get-compliance-references.json"
    #     with open(result_file, "r", encoding="utf-8") as f:
    #         expected = ComplianceReference.schema().loads(f.read(), many=True)
    #         given = ComplianceReference.schema().loads(response.text, many=True)
    #         assert expected.sort() == given.sort()

    def test_get_compliance_labels(self, client):
        """ "Test /api/compliance_labels endpoint to return labels. Not implemented - returns an empty set"""
        response = client.get("/api/compliance_labels")
        assert response.status_code == 200
        assert [] == json.loads(response.text)

    @pytest.mark.parametrize("layer, expected", [("PAAS", 0), ("IAAS", 19)])
    def test_post_query(self, layer, expected, client, secured_headers):
        """ "Test /query endpoint to query services"""
        filters = {"layers": [layer]}
        response = client.post("/query", headers=secured_headers, json=filters)

        assert response.status_code == 200
        # TODO : re-enable after query refacto
        # assert expected == len(json.loads(response.text))

    def test_delete_demo_data(self, client, secured_headers):
        """Test /demo-data endpoint to delete Graph DB"""
        response = client.delete("/demo-data", headers=secured_headers)
        assert response.status_code == 200
