# -*- coding: utf-8 -*-
from federated_catalogue import Config
from federated_catalogue.Config import Settings, get_settings

settings: Settings = get_settings()


class TestConfig:
    """Test class for the Config module"""

    def test_service_types(self):
        """Ensure service types data references are not modified"""
        assert Config.SERVICE_TYPES == [
            "Authentication",
            "Backup",
            "Bare Metal",
            "Collaboration",
            "Compute",
            "Container",
            "Data Analytics",
            "Data Base",
            "Digital Signature",
            "ID management",
            "IoT",
            "Logs",
            "Machine Learning",
            "Networking",
            "Storage",
            "Translation",
            "Virtual Machine",
            "VPN",
            "VPS",
            "Webhosting",
            "Data Product",
        ]

    def test_layers(self):
        """Ensure layers data references are not modified"""
        assert Config.LAYERS == ["IAAS", "PAAS", "SAAS"]

    def test_federation_repository(self):
        """Ensure federation repository is not modified"""
        assert Config.FEDERATION_REPOSITORY == "abc-federation"

    def test_api_key_authorized(self):
        """Ensure API key is correctly loaded from env file"""
        assert settings.api_key_authorized.get_secret_value() == "this-is-a-secure-key"

    def test_api_port_exposed(self):
        """Ensure API port is correctly loaded from env file"""
        assert settings.api_port_exposed == 5001

    def test_redis_host_exposed(self):
        """Ensure Redis Host is correctly loaded from env file"""
        assert settings.redis_host == "redis://redis:6379/0"

    def test_graph_db_url(self):
        """Ensure Graph DB URL is correctly loaded from env file"""
        assert settings.graphdb_url == "http://graphdb:7200"
