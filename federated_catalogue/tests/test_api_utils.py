# -*- coding: utf-8 -*-
import pytest

# https://flask.palletsprojects.com/en/2.2.x/testing/
# https://py-pkgs.org/05-testing.html


@pytest.mark.integration
class TestAPIUtils:
    """Test class for the api_key_required module"""

    def test_api_key_is_required(self, client):
        """Ensure api key is required on specific endpoints"""
        secured_endpoints = {
            "/test-api-key": "get",
            "/demo-data": "delete",
        }
        for endpoint, method in secured_endpoints.items():
            if method == "get":
                response = client.get(endpoint)
            if method == "delete":
                response = client.delete(endpoint)
            if method == "post":
                response = client.post(endpoint)
            assert response.status_code == 403
            assert "Could not validate API KEY" in response.text

    def test_api_key_invalid(self, client):
        """Ensure invalid api key returns 403"""
        response = client.get("/test-api-key", headers={"x-api-key": "dummy"})
        assert response.status_code == 403
        assert "Could not validate API KEY" in response.text

    def test_api_key_valid(self, client):
        """Ensure api key return 200"""
        response = client.get("/test-api-key", headers={"x-api-key": "this-is-a-secure-key"})
        assert response.status_code == 200
        assert "Coucou tout le monde" in response.text
