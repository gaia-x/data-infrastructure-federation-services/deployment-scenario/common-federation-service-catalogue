# -*- coding: utf-8 -*-

import os

from federated_catalogue.Config import ROOT_DIR
from federated_catalogue.services.compliance_criterion_service import (
    ComplianceCriterionService,
)

criterion_service = ComplianceCriterionService(
    os.path.join(ROOT_DIR, "tests", "compliance_criterion", "mock", "criterions_test.json")
)


def test_return_all_list_of_compliance_criterion():
    result_criterions = criterion_service.list_criterions()

    assert len(result_criterions) == 3


def test_return_only_self_assessed_compliance_criterion():
    result_criterions = criterion_service.list_criterions(True)

    assert len(result_criterions) == 2


def test_return_only_not_self_assessed_compliance_criterion():
    result_criterions = criterion_service.list_criterions(False)

    assert len(result_criterions) == 1
