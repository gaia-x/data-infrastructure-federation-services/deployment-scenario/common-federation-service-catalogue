# -*- coding: utf-8 -*-
# pylint: disable=line-too-long
provider_by_did = {
    "did:web:aws.provider.gaia-x.community:participant:7d1507284a5757cac6b62708a4ef00bfc5d695256489cb704f12b4b9e6255df2/data.json": "AWS",
    "did:web:3ds-outscale.provider.gaia-x.community:participant:3f7965bcfc864665fa0e9aeb3a72020a75cac0e2ac91df85f47da2fb97048e58/data.json": "3DS-OUTSCALE",
    "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/data.json": "ARUBA",
    "did:web:docaposte.provider.gaia-x.community:participant:44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5be831bb07d4f260f/data.json": "DOCAPOSTE",
    "did:web:gigas.provider.gaia-x.community:participant:10f73b0bf81e10616bd3ac78c0f77c34c9a6022353ee0077b6e09e87d33ffb9e/data.json": "GIGAS",
    "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json": "OVHCLOUD",
    "did:web:leaseweb.provider.gaia-x.community:participant:e16635100ff43f2569d94867cbc6e9f270aa7031ed5df31cdac4bde9595ef3c7/data.json": "LEASEWEB",
}

mapping = {
    "3DS-OUTSCALE": {"S-01": 2, "S-02": 3},
    "ARUBA": {"S-01": 3, "S-02": 3, "S-03": 3, "S-04": 3, "S-05": 3, "S-06": 3},
    "AWS": {
        "S-01": 2,
        "S-02": 2,
        "S-03": 2,
        "S-04": 2,
        "S-05": 2,
        "S-06": 2,
        "S-07": 2,
        "S-08": 2,
        "S-09": 2,
        "S-10": 2,
        "S-11": 2,
        "S-12": 2,
        "S-13": 2,
        "S-14": 2,
        "S-15": 2,
        "S-16": 2,
        "S-17": 2,
        "S-18": 2,
        "S-19": 2,
        "S-20": 2,
        "S-21": 2,
        "S-22": 2,
        "S-23": 2,
        "S-24": 2,
        "S-25": 2,
        "S-26": 2,
        "S-27": 2,
        "S-28": 2,
        "S-29": 2,
        "S-30": 2,
        "S-31": 2,
        "S-32": 2,
        "S-33": 2,
        "S-34": 2,
        "S-35": 2,
        "S-36": 2,
        "S-37": 2,
        "S-38": 2,
        "S-39": 2,
        "S-40": 2,
        "S-41": 2,
        "S-42": 2,
        "S-43": 2,
        "S-44": 2,
        "S-45": 2,
        "S-46": 2,
        "S-47": 2,
        "S-48": 2,
        "S-49": 2,
        "S-50": 2,
        "S-51": 2,
        "S-52": 2,
    },
    "CORETECH": {
        "S-01": 1,
        "S-02": 1,
        "S-03": 1,
        "S-04": 1,
        "S-05": 1,
        "S-06": 1,
        "S-07": 1,
        "S-08": 1,
        "S-09": 1,
        "S-10": 1,
    },
    "DOCAPOSTE": {
        "S-01": 1,
        "S-02": 1,
        "S-03": 1,
        "S-04": 1,
        "S-05": 1,
        "S-06": 1,
        "S-07": 1,
        "S-08": 1,
        "S-09": 1,
        "S-10": 1,
        "S-11": 1,
        "S-12": 1,
    },
    "LEASEWEB": {"S-01": 2, "S-02": 2, "S-03": 2, "S-04": 2, "S-05": 2, "S-06": 2, "S-07": 2},
    "GIGAS": {"S-01": 3, "S-02": 2, "S-03": 2, "S-04": 2, "S-05": 2},
    "OVHCLOUD": {
        "S-01": 1,
        "S-02": 1,
        "S-03": 1,
        "S-04": 1,
        "S-05": 1,
        "S-06": 1,
        "S-07": 1,
        "S-08": 1,
        "S-09": 2,
        "S-10": 1,
        "S-11": 1,
        "S-12": 1,
        "S-13": 1,
        "S-14": 2,
        "S-15": 1,
        "S-16": 1,
        "S-17": 2,
        "S-18": 1,
        "S-19": 1,
        "S-20": 1,
        "S-21": 1,
        "S-22": 1,
        "S-23": 1,
        "S-24": 1,
        "S-25": 2,
        "S-26": 2,
        "S-27": 2,
        "S-28": 1,
        "S-29": 1,
        "S-30": 1,
        "S-31": 1,
        "S-32": 1,
        "S-14-France": 3,
        "S-14-United Kingdom": 3,
        "S-14-Belgium": 3,
        "S-14-Poland": 3,
        "S-27-France": 3,
        "S-27-United Kingdom": 3,
        "S-27-Belgium": 3,
        "S-27-Poland": 3,
        "S-27-Germany": 3,
        "S-26-France": 3,
        "S-26-United Kingdom": 3,
        "S-26-Belgium": 3,
        "S-26-Poland": 3,
        "S-26-Germany": 3,
        "S-14-Germany": 3,
    },
}

ue_countries = ["France", "United Kingdom", "Belgium", "Poland", "Germany"]


def get_label_for(provider_did, service_internal_id, country_name):
    provider = get_provider_for(provider_did)

    if provider is None:
        return 0
    elif provider in mapping and service_internal_id in mapping[provider]:
        if provider == "OVHCLOUD" and service_internal_id in ["S-14", "S-26", "S-27"] and country_name in ue_countries:
            return mapping[provider][f"{service_internal_id}-{country_name}"]
        return mapping[provider][service_internal_id]
    else:
        return 0


def get_provider_for(provider_did):
    if provider_did in provider_by_did:
        return provider_by_did[provider_did]

    return None
