[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.poetry]
name = "federated-catalogue"
version = "0.2.0"
description = "federated-catalogue - Gaia-X federated self-decription object catalogue"
authors = ["None"]
license = "Apache-2.0"
readme = "README.md"
packages = [{include = "federated_catalogue"}]

[tool.poetry.dependencies]
python = "^3.10"
rdflib = "^7.0.0"
fastapi = "^0.98.0"
uvicorn = "^0.22.0"
requests = "^2.30.0"
python-dotenv = "^1.0.0"
dataclasses-json = "^0.6.4"
redis = "^5.0.1"
httpx = "^0.24.1"
asgi-correlation-id = "^4.2.0"
structlog = "^23.1.0"
prometheus-fastapi-instrumentator = "^6.0.0"
async-timeout = "^4.0.3"

[tool.poetry.group.dev.dependencies]
types-requests = "^2.30.0.0"
pylint = "^2.17.4"
types-flask-cors = "^3.0.10.5"
pytest = "^7.3.1"
pytest-env = "^0.8.1"
isort = "^5.12.0"
black = "^23.3.0"
ruff = "^0.0.267"
coverage = "^7.2.7"
pytest-cov = "^4.1.0"
rich = "^13.4.2"
# locust = "^2.15.1"
fakeredis = "^2.18.1"
pytest-asyncio = "^0.21.0"
anyio = "^3.6.2"

[tool.pytest.ini_options]
minversion = "6.0"
addopts = "-rp --no-header --ignore=tests/locust"

env = [
"API_KEY_AUTHORIZED=this-is-a-secure-key",
"API_PORT_EXPOSED=5001",
"REDIS_HOST=redis://redis:6379/0",
"GRAPHDB_URL=http://graphdb:7200",
"REDIS_PUB_SUB_HOST=redis://redis:6379/1",
"PARTICIPANT_NAME=abc-federation",
"PARENT_DOMAIN=dev.gaiax.ovh",
"LOG_LEVEL=DEBUG",
"ENVIRONMENT=TEST"
]

markers = [
    "integration: marks tests as requiring external docker containers (integration)",
]

testpaths = [
    "federated-catalogue/tests",
]

[tool.isort]
profile = "black"
known_third_party = ["flask", "swagger-ui-py", "rdflib", "requests", "python-dotenv", "flask-cors", "dataclasses-json", "linkml", "redis", "inject"]
src_paths = ["catalogue", "tests"]
no_lines_before="LOCALFOLDER"
use_parentheses=true
include_trailing_comma=true
color_output=true

[tool.black]
line-length = 120
target-version = ['py310']
include = '(federated_catalogue|tests)\/.*(\.py)'
include_trailing_comma = false
exclude='''
(
  /(
      \.eggs         # exclude a few common directories in the
    | \.eggs-info
    | \.git          # root of the project
    | \.tox
    | \.venv
    | \.sha1
    | \.direnv/python_venv
    | \.direnv/.sha1
    | python_venv
    | build
    | dist
    | site
    | site.tox
  )/
)
'''

[tool.ruff]
line-length = 120
src = ["catalogue"]
exclude = [".bzr", ".direnv", ".eggs", ".git", ".git-rewrite", ".hg", ".mypy_cache", ".nox", ".pants.d", ".pytype", ".ruff_cache", ".svn", ".tox", ".venv", "__pypackages__", "_build", "buck-out", "build", "dist", "node_modules", "venv", "tests"]

select = [
    "E",  # pycodestyle
    "F",  # pyflakes
    "W",  # pycodestyle
    "B",  # bugbear
    "I",  # isort
    "S",  # flake8-bandit
    "RUF", # ruff
]

fixable = [
    "E",  # pycodestyle
    "F",  # pyflakes
    "W",  # pycodestyle
    "B",  # bugbear
    "I",  # isort
    "S",  # flake8-bandit
    "RUF", # ruff
]

ignore = [
    "RUF008",
    "RUF009",
    "S104"
]

[tool.ruff.pydocstyle]
convention = "google"

[tool.pylint.'BASIC']
argument-naming-style="snake_case"
attr-naming-style="snake_case"
function-naming-style="snake_case"
const-naming-style="UPPER_CASE"
class-naming-style="PascalCase"
variable-naming-style="snake_case"


[tool.pylint.'FORMAT']
max-line-length = 120
ignore-long-lines='^\s*(# )?<?https?://\S+>?$'

[tool.pylint.'MASTER']
disable = '''
    missing-module-docstring,
    missing-class-docstring,
    invalid-name,
    no-value-for-parameter,
    protected-access,
    too-few-public-methods,
    logging-fstring-interpolation
'''

[tool.pylint.'LOGGING']
logging-format-style = 'new'
logging-modules = 'logging'
