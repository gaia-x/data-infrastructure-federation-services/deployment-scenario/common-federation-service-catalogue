# Federated Catalogue

The catalogue is able to stock VC and Credentials and to search it via API.

This component is used for the Aster-X demonstration ecosystem. You can find more information on this
page https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11

## If you want to know more about Federated Catalogue functionalities

- [Read this documentation](./documentation/README.md)

## If you want to contribute on this repository

- [Read the contributing rules](./CONTRIBUTING.md)
- [Read the developer guide](./documentation/developer-guide.md)

## If you want to know how the catalogue was used for the Gaia-X Summit November 2022

- [Read the demo description guide](./documentation/demo-description.md)

## Swagger endpoints

| Environment | API Documentation                                                    |
|-------------|----------------------------------------------------------------------|
| DEV         | https://federated-catalogue-api.abc-federation.dev.gaiax.ovh/api/doc |
| DEMO        | https://federated-catalogue-api.aster-x.demo23.gxfs.fr/api/doc       |
