# J'ai un missing criterion, quel scheme me permet de l'obtenir ?
import requests

compliance_certification_scheme_dids = []

third_scheme_dids = [
    # THIRD
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/0c8f60212eb57bb4d64e28831c36acd0c691bcfe52525c85780ffe65e1a1bf30/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/10f01a488503c2563e357ee83807eb78328d396881a8f42b90e5709d016e3fde/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/10f2f5644412e89dc678e1a291d70e0ccf8b1a0608be7b72983811d727b52030/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/24ed9ef4e71eb9745a4cc0a5ef1b20715bed6b21f65c5aced62284dc15fe6ea6/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/258569de2ba98a59f943385aa4f41789540b8140a56b57c8ae5caaf66d03398c/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/2ac534803b83d6451caef296e2696aedfea439106fe8f97ece1be1021436a5c3/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/2ec78d3d3cafd518e707e602845e50b698b903092dae25fdbcfe27535da34036/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/30168763171f73a6a9844c2983eb75a1ba608d9b6e6068a9c48b89864563d70c/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/3240d30ae7205824f065b48e70b4652322d0062b14c6f54752545cf6d10d85df/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/328be80d80f3fb35fdd90a8c51cf1ee8bebbe24ad02055924f254575ce6a5d10/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/3508c4dc2f88e61e4cd693039dc9f6524e80e15f2d5afa125dc5f7c588b06256/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/387a52783c823f0711288555d2b8a0a932914fd65eef89e1b7faa5fd66240665/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/3987050ce1b7566e98b3f3cd07728c47f0e3aa8315eb9ded2f8420ea88b084ef/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/3e01f1e8a2b0b93efee1f9df2d9d163a00c814cffb282c99dacf29bab4e449bc/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/42378a84e567fd3e4f95edcdb37ef72f2a884f2de56a4b945cd2d24ab2714d63/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/4e7926f658ab77f5499e11bd9c0eacada11101aa1ea9ace57f11a1a33d95d5f5/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/553453a18ecd6c3ae360d8729ce846fb50943ec2195ba51afae615e875ea7d69/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/55a110d03705226f86f4ccfa2c7659e10cc3db8bfa5f18947498ee831ec97a73/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/574e45221077616575ed0a94d3fdce693bd02474a2a15ffd9c69afe0d4d1405d/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/5ee86b4ce8dcdeb43a611a60c4224628e172a0a212b05aac7d159493aff86d78/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/68f70a8a46706216fa6fe3b8b49fd014dca0c43fae5c9290e42bf65e4618eefc/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/69317dbc9f83181701c3e328346989100b233b276f73617e2cfe0f709fbe9e72/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/6a74512304ba74a8496ff3053d90af26d758e5564f440d81b188543a23f27c4e/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/6c085b526f63b6a0a720115abf3c126a7b144c9296b0d5c9a6c253c1798bcdca/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/7116ba094f14edd03ab80bd1098ff8398f5f0ab73dd79f49c8b7cedf5dc2591c/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/73223cee5129b62ac086aa5e1cb0b4a093551708f2cb987576ee0553fbe2ba07/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/799f34803f5bec84db3d958bcdef639fbc211f60d7205b05120bb49fa4bd7f05/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/7a7bfb898c1c6ba8f9c2124fa8107d2512668dfd0fcc54000ab6bd9907884593/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/7c43f45013b284eb63f04454a528fae9d55f6fe25f0ca392f0452d5888f80958/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/7e244f9cc0bd1f4119b793854fb86274aab91a6fb742947b5f85e26d3fc44761/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/7f17eda24852b9e1fccb673d861ae5ae495937aed3f9ea85806834c5f37dbf68/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/88897c23006643cb7ac79d3cc58537b6c920bd31ade8299b78dd4a0a5a10f2f8/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/9081ff96a42b6dd9eef29559cc2d8379b1d0f4eb6d2a39b19d9bd6665bd1f6c1/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/927aa1d6a40043a057e20be99ffa1cc8b12cda2b420bba993db7256833c7a02d/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/951a31edbb128425214cc2fb06336cf42ec8148148a7c5b2e107153f3f5d5b28/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/97374a5d3de620b61331ef705890e3635193afee721d2f74f6b024365c4efda7/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/98b68d76c462eb88b8341b8e08a915ee30f96569c264c1ff80ec245fab7a9d11/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/9b7e2e54bde4da671e4f606d35f7736d5b4d053dcfdadd6bb209cab104f1c994/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/a003cd0e08eb73f03c08043c2f19e3664dd03cdc7e8a18c2c3101c20d5c19bcc/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/a63cb078043dff8c62f736e1f806f040d66397d4c1c5967acdeef04f270b0dfc/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/b226dae5ec25c8e0dbd6a35fe59f01033bba0ed92ca6033e4aa82c656b0fcdd4/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/b66930fdf7bbea6f1ea00e919f522d67f736b1448bd1f1774263d7b0a2a648a3/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/bca07123dc9987b960a07d4ba396aac943729289f1bc58a0bddfa1a8374bb36e/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/c19a1d11a5aa182044e3f6e140d796809677e59f28f19e7de16cd97954cfdbf5/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/c3754afae7b54d884caa43535a388914ee9a5378261275eb064cf1304c33f083/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/cbad269ff192da6828ead391b8f8d0f7c3dec8175ad447724af1d622d809f580/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/d2adb5340cd33464aff90dedc15bc1f55d5a6043667b950f6690c92d27f77e36/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/d6132871f518d0866883f7484444e3f0dfcd00eb43579f5543274872a38af7d7/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/d7602f0649f6b0ab756c67708a3f0abb3c8a3c9fbc2d1c90fb1212b25e7e886a/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/e04c270ebf8d8d27b50956a69841d6dbe4d4cd89f44c5a293ebb66e64004e97e/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/e573eb34b78654101069e15b93c2fa3fcd40f29b7b28eb537690b1f7f73f6009/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/ebe5272b7152f8f813dbabb14880dd6ea4bb82db52466218c3165d26af4abb7f/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/edea0399192d09dfb460076910f4a4f078f5688e6d40e7063ee058f330d625a2/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/ef2c598024de9df0bd390984b992c734a3e2464418034ff0abb264e184194c72/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/third-party-compliance-certification-scheme/fb82f8107193de5cc0c872bcc2766abb5f128faed150ec2ee51d1f336c66f739/data.json",
    # SIMPLE
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/0ab85fcf83ad5389451d5add074a3f39a80aa72b420f7f71edf82e95954ce7d4/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/24ed9ef4e71eb9745a4cc0a5ef1b20715bed6b21f65c5aced62284dc15fe6ea6/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/2ec78d3d3cafd518e707e602845e50b698b903092dae25fdbcfe27535da34036/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/30168763171f73a6a9844c2983eb75a1ba608d9b6e6068a9c48b89864563d70c/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/5ee86b4ce8dcdeb43a611a60c4224628e172a0a212b05aac7d159493aff86d78/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/68f70a8a46706216fa6fe3b8b49fd014dca0c43fae5c9290e42bf65e4618eefc/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/7116ba094f14edd03ab80bd1098ff8398f5f0ab73dd79f49c8b7cedf5dc2591c/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/73223cee5129b62ac086aa5e1cb0b4a093551708f2cb987576ee0553fbe2ba07/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/b226dae5ec25c8e0dbd6a35fe59f01033bba0ed92ca6033e4aa82c656b0fcdd4/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/bca07123dc9987b960a07d4ba396aac943729289f1bc58a0bddfa1a8374bb36e/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/c93e0e0a42808e86a1e7fea130fce02fe4b3a93b1ec99d2e07383e71dce785e4/data.json",
    "did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-certification-scheme/d73d457d686ea02f7a6a06d8469bc9af35ff4d47042baaefb28cbf5c42cc9480/data.json",
]


def transform_did_into_url(did: str):
    if did.startswith("did:web"):
        return did.replace(":", "/").replace("did/web/", "https://")
    else:
        return did


scheme_map = {}
for scheme_did in third_scheme_dids:
    scheme: dict = requests.get(transform_did_into_url(scheme_did)).json()
    scheme_map[scheme_did] = scheme

compliance_certification_scheme_map = {}
for scheme_did in compliance_certification_scheme_dids:
    scheme: dict = requests.get(transform_did_into_url(scheme_did)).json()
    compliance_certification_scheme_map[scheme_did] = scheme

print("SCHEMES LOADED")

# missing_criterion = 'did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-criterion/3db6d8373f30d8205f0ec696b39983f438082bded9873801f451c29de20b4389/data.json'
# no one

# missing_criterion = 'did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-criterion/1c5d1d2b7ea821c63d35947ec741b9ff89e4635b232901991264a2f16d9882c7/data.json',
# no one

# missing_criterion = 'did:web:aster-x.demo23.gxfs.fr:participant:d30a153a1080f608ca5ab38877d3f8076c6d4d465dd6cf9110accd6c6dfcd36d/compliance-criterion/7b493ba0feecbc7ce6321fa34135d7ad73034740a8f92ad5c977a9750cb8582b/data.json'
# no one


for key, value in scheme_map.items():
    for criteria in value["aster-conformity:grantsComplianceCriteria"]:
        if criteria["id"] == missing_criterion:
            print(f"Criterion granted with {key}")
